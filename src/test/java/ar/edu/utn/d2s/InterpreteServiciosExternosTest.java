package ar.edu.utn.d2s;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ar.edu.utn.d2s.pdis.Direccion;
import ar.edu.utn.d2s.pdis.Servicio;
import org.junit.Before;
import org.junit.Test;
import org.uqbar.geodds.Point;

import ar.edu.utn.d2s.pdis.CGP;
import ar.edu.utn.d2s.pdis.SucursalBanco;
import ar.edu.utn.d2s.serviciosExternos.InterpreteServiciosExternos;
import ar.edu.utn.d2s.serviciosExternos.CGP.ConsultorCGPsMock;
import ar.edu.utn.d2s.serviciosExternos.banco.ConsultorBancosMock;

public class InterpreteServiciosExternosTest {

	private InterpreteServiciosExternos interpreteServExt;
	private ConsultorBancosMock consultorBancos;
	private ConsultorCGPsMock consultorCGPs;
	private List<SucursalBanco> listaBancosEncontrados;
	private List<CGP> listaCGPsEncontrados;
	private SucursalBanco bancoPrueba;
	private List<Servicio> listaServiciosBanco = new ArrayList<>();
	private CGP CGPPrueba;
	
	@Before
	public void setUp() {
		
		consultorBancos = new ConsultorBancosMock();
		consultorCGPs = new ConsultorCGPsMock();
		interpreteServExt = new InterpreteServiciosExternos();
	
		listaBancosEncontrados = interpreteServExt.interpretarBancos(consultorBancos.consultarBancos("ICBC","transacciones"));
		listaCGPsEncontrados = interpreteServExt.interpretarCGPs(consultorCGPs.getListaCentrosDTO());
	
		
	}
	
	
	@Test
	public void testInterpretaBanco() {
		
		
		assertTrue(listaCGPsEncontrados.size()==2);
		
		
	}
	
	
	@Test
	public void testInterpretaCGP() {
		

		assertTrue(listaCGPsEncontrados.size()==2);
	}
	
	
}
