package ar.edu.utn.d2s;

import static org.junit.Assert.*;

import java.util.Calendar;

import ar.edu.utn.d2s.pdis.Direccion;
import org.junit.Before;
import org.junit.Test;
import org.uqbar.geodds.Point;

import ar.edu.utn.d2s.pdis.SucursalBanco;

public class SucursalBancoTest {

	private SucursalBanco galicia99;
	private SucursalBanco hsbc36;
	private SucursalBanco santander23;
	private Point puntoReferencia;
	private Calendar horaBancaria;
	private Calendar horaNoBancaria;
	
	@Before
	public void setUp() {

		Direccion direccionGalicia99 = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(-34.604589, -58.423584)); //Sarmiento entre Gascon y Acu�a de Figeroa, CABA
		galicia99 = new SucursalBanco("Banco Galicia", null, direccionGalicia99, null);

		Direccion direccionHsbc36 = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(-34.605896, -58.395941)); //Sarmiento entre Junin y Ayacucho, CABA
		hsbc36 = new SucursalBanco("HSBC", null, direccionHsbc36, null);

		Direccion direccionSantander23 = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(-34.605955, -58.414328)); //Sarmiento entre Gallo y Sanchez de Bustamente, CABA
		santander23 = new SucursalBanco("Santander Río", null, direccionSantander23, null);

		puntoReferencia = new Point(-34.604735, -58.421991);	//Sarmiento entre Medrano y Acu�a de Figueroa
		
		horaBancaria = Calendar.getInstance();
		horaBancaria.set(Calendar.MILLISECOND, 0);
		horaBancaria.set(Calendar.SECOND, 0);
		horaBancaria.set(Calendar.MINUTE, 10);
		horaBancaria.set(Calendar.HOUR_OF_DAY, 10);
		horaBancaria.set(Calendar.DAY_OF_MONTH, 19);
		horaBancaria.set(Calendar.MONTH, 04);
		horaBancaria.set(Calendar.YEAR, 2016);
		
		horaNoBancaria = Calendar.getInstance();
		horaNoBancaria.set(Calendar.MILLISECOND, 0);
		horaNoBancaria.set(Calendar.SECOND, 0);
		horaNoBancaria.set(Calendar.MINUTE, 00);
		horaNoBancaria.set(Calendar.HOUR, 16);
		horaNoBancaria.set(Calendar.DAY_OF_MONTH, 18);
		horaNoBancaria.set(Calendar.MONTH, 04);
		horaNoBancaria.set(Calendar.YEAR, 2016);
		
	}
	
	/*------------ Tests Punto 1 ------------ */
	@Test
	public void testPuntoCercaEstaCerca() {
		
		assertTrue(galicia99.estaCerca(puntoReferencia));
	}
	
	@Test
	public void testPuntoLejosNoEstaCerca() {
		
		assertFalse(hsbc36.estaCerca(puntoReferencia));
	}
	
	@Test
	public void testPuntoLimiteNoEstaCerca() {
		
		assertFalse(santander23.estaCerca(puntoReferencia));
	}
	
	/*---------- Fin Tests Punto 1 ---------- */
	
	
	/*------------ Tests Punto 2 ------------ */
	@Test
	public void testEstaDisponibleEnHoraBancaria() {
		
		
		assertTrue(galicia99.estaDisponible(horaBancaria,""));
		
	}
	
	@Test
	public void testNoEstaDisponibleFueraDeHoraBancaria() {
		
		assertFalse(galicia99.estaDisponible(horaNoBancaria, ""));
	}
	
	/*---------- Fin Tests Punto 2 ---------- */
}
