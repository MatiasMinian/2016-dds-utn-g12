package ar.edu.utn.d2s;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;

import ar.edu.utn.d2s.pdis.Direccion;
import org.junit.Before;
import org.junit.Test;
import org.uqbar.geodds.Point;

import ar.edu.utn.d2s.pdis.Colectivo;
import ar.edu.utn.d2s.pdis.Parada;

public class ColectivoTest {

	private Colectivo colectivo24;
	private Colectivo colectivo109;
	private Colectivo colectivo71;
	private Parada paradaCerca;
	private Parada paradaLejos;
	private Parada paradaLimite;
	private List<Parada> unaParadaCercaLista;
	private List<Parada> ningunaParadaCercaLista;
	private List<Parada> unaParadaLimiteLista;
	private Point puntoReferencia;
	
	@Before
	public void setUp() {
		
		colectivo24 = new Colectivo("Línea 24", null, null, null, 24);
		colectivo109 = new Colectivo("Línea 109", null, null, null, 109);
		colectivo71 = new Colectivo("Línea 71", null, null, null, 71);

		Direccion direccionParadaCerca = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(-34.604610, -58.422960)); // Sarmiento esq. Acu�a de Figueroa
		paradaCerca = new Parada(direccionParadaCerca);

		Direccion direccionParadaLejos = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(-34.597816, -58.420942)); // Av. C�rdoba entre Acu�a de Figueroa y Medrano
		paradaLejos = new Parada(direccionParadaLejos);

		Direccion direccionParadaLimite = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(-34.604560, -58.423567)); // Sarmiento entre Acu�a de Figueroa y Gascon
		paradaLimite = new Parada(direccionParadaLimite);
		
		puntoReferencia = new Point(-34.604735, -58.421991);	// Sarmiento entre Medrano y Acu�a de Figueroa
		
		unaParadaCercaLista = new ArrayList<Parada>();
		unaParadaCercaLista.add(paradaCerca);
		unaParadaCercaLista.add(paradaLejos);
		ningunaParadaCercaLista = new ArrayList<Parada>();
		ningunaParadaCercaLista.add(paradaLejos);
		ningunaParadaCercaLista.add(paradaLejos);
		unaParadaLimiteLista = new ArrayList<Parada>();
		unaParadaLimiteLista.add(paradaLimite);
		
		colectivo24.setParadas(unaParadaCercaLista);
		colectivo109.setParadas(ningunaParadaCercaLista);
		colectivo71.setParadas(unaParadaLimiteLista);
	}
	
	/*------------ Tests Punto 1 ------------ */
	@Test
	public void testParadaCercaEstaCerca() {
		
		assertTrue(colectivo24.estaCerca(puntoReferencia));
	}
	
	@Test
	public void testParadaLejosNoEstaCerca() {
		
		assertFalse(colectivo109.estaCerca(puntoReferencia));
	}
	
	@Test
	public void testParadaLimiteNoEstaCerca() {
		
		assertFalse(colectivo71.estaCerca(puntoReferencia));
	}
	
	/*---------- Fin Tests Punto 1 ---------- */
	
	
	/*------------ Tests Punto 2 ------------ */
	@Test
	public void testSiempreEstaDisponible() {
		
		Calendar fecha = Calendar.getInstance();
		fecha.set(Calendar.MILLISECOND, 0);
		fecha.set(Calendar.SECOND, 0);
		fecha.set(Calendar.MINUTE, 30);
		fecha.set(Calendar.HOUR, 10);
		fecha.set(Calendar.DAY_OF_MONTH, 18);
		fecha.set(Calendar.MONTH, 04);
		fecha.set(Calendar.YEAR, 2016);
		
		assertTrue(colectivo24.estaDisponible(null, ""));
		assertTrue(colectivo24.estaDisponible(Calendar.getInstance(), ""));
		assertTrue(colectivo24.estaDisponible(fecha, ""));
	}
	
	/*---------- Fin Tests Punto 2 ---------- */
}
