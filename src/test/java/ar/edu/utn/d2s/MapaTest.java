package ar.edu.utn.d2s;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.cache.CacheManager;
import ar.edu.utn.d2s.pdis.*;
import org.junit.Before;
import org.junit.Test;

import ar.edu.utn.d2s.serviciosExternos.InterpreteServiciosExternos;
import ar.edu.utn.d2s.serviciosExternos.CGP.ConsultorCGPsMock;
import ar.edu.utn.d2s.serviciosExternos.banco.ConsultorBancosMock;
import junit.framework.AssertionFailedError;

//Tests punto 3 (Búsqueda de puntos)

public class MapaTest {

	private Mapa mapa;
	private Colectivo linea19;
	private Colectivo linea24;
	private List<PalabraClave> listaPalabrasClave;
	private Rubro hamburgueseria;
	private LocalComercial mcDonalds;
	private SucursalBanco bancoFrances;
	private SucursalBanco itau;
	private Comuna Comuna1;
	private List<Servicio> listaServicios;
	private Servicio Turismo;
	private CGP comuna1;
	private List<PuntoDeInteres> puntosDeInteresLista = new ArrayList<PuntoDeInteres>();
	private ResultadoBusqueda resultadoBusqueda;
	private ConsultorCGPsMock ccgpmock;
	private ConsultorBancosMock cbmock;
	private InterpreteServiciosExternos ise;


	@Before
	public void setUp() {

		mapa = Mapa.getInstance();

		mapa.inicializar();

		linea19 = new Colectivo("Linea 19", null, null, null, 19);
		linea24 = new Colectivo("Linea 24", null, null, null, 24);

		mapa.altaDePunto(linea19);

		puntosDeInteresLista.add(linea24);

		listaPalabrasClave = new ArrayList<>();
		listaPalabrasClave.add(new PalabraClave(null, "hamburguesa"));
		listaPalabrasClave.add(new PalabraClave(null, "tomate"));
		listaPalabrasClave.add(new PalabraClave(null, "lechuga"));

		hamburgueseria = new Rubro(null, "hamburgueseria" ,321);

		mcDonalds = new LocalComercial("Mcdonalds", "", null, null, hamburgueseria, listaPalabrasClave);

		mapa.altaDePunto(mcDonalds);

		bancoFrances = new SucursalBanco("frances", null, null, null);

		mapa.altaDePunto(bancoFrances);

		Comuna1 = new Comuna(1,null);
		listaServicios = new ArrayList<Servicio>();
		Turismo = new Servicio(null,"turismo");
		listaServicios.add(Turismo);
		comuna1 = new CGP("Comuna 1",null,null,Comuna1,listaServicios);

		mapa.altaDePunto(comuna1);

		itau = new SucursalBanco("itau", null, null, null);

		resultadoBusqueda = new ResultadoBusqueda();
		
		/*
		ccgpmock = new ConsultorCGPsMock();
		cbmock = new ConsultorBancosMock();
		
		mapa.setConsultorCGPs(ccgpmock);
		mapa.setConsultorBancos(cbmock);
		
		ise = new InterpreteServiciosExternos();
		
		mapa.setInterprete(ise);
		*/
		

	}

	//Tests genéricos para todos
	@Test(expected = Exception.class)
	public void testNoMatcheaPorIngresarTextoVacio() throws Exception{

           mapa.buscarPuntoSegun("");
           
    }

	@Test
	public void testNoMatcheaPorNoEncontrarPunto() throws Exception{

		resultadoBusqueda=mapa.buscarPuntoSegun("galeria jardin");
		assertTrue(resultadoBusqueda.getResultado().size()==0);

	}

	//Fin tests genéricos


	//Test banco
	
	@Test
	public void testMatcheaPorComienzoNombreBanco() throws Exception{

		resultadoBusqueda = mapa.buscarPuntoSegun("fran");
		assertTrue(resultadoBusqueda.getResultado().contains(bancoFrances));

	}
	

	//Fin test banco

	//Test colectivo

	@Test
	public void testMatcheaPorLineaColectivo() throws Exception{

		resultadoBusqueda=mapa.buscarPuntoSegun("19");
		assertTrue(resultadoBusqueda.getResultado().contains(linea19));
	}

	//Fin test colectivo


	//Tests local comercial

	@Test
	public void testMatcheaPorComienzoNombreLocal() throws Exception{

		resultadoBusqueda=mapa.buscarPuntoSegun("Mc");
		assertTrue(resultadoBusqueda.getResultado().contains(mcDonalds));

	}
	
	@Test
	public void testMatcheaPorNombreRubro() throws Exception{

		resultadoBusqueda=mapa.buscarPuntoSegun("hamburgueseria");
		assertTrue(resultadoBusqueda.getResultado().contains(mcDonalds));


	}

	@Test
	public void testMatcheaPorPalabrasClave() throws Exception{

		resultadoBusqueda=mapa.buscarPuntoSegun("hamburguesa");
		assertTrue(resultadoBusqueda.getResultado().contains(mcDonalds));

	}
	

	//Fin tests local comercial

	//Tests CGPS

	@Test
	public void testMatcheaPorNumeroComuna() throws Exception{

		resultadoBusqueda=mapa.buscarPuntoSegun("1");
		assertTrue(resultadoBusqueda.getResultado().contains(comuna1));

	}

	@Test
	public void testMatcheaPorComienzoNombreServicio() throws Exception{

		resultadoBusqueda=mapa.buscarPuntoSegun("turis");
		assertTrue(resultadoBusqueda.getResultado().contains(comuna1));
	}

	//Fin tests CGP

	/* ---------------------- Tests Entrega 2 ---------------------- */
	//@Test

	
	@Test
	public void testAltaPuntoDeInteres() {

		assertTrue(mapa.getPuntosDeInteres().contains(linea19));
	}


	@Test
	public void testBajaPuntoDeInteres() throws Exception {

        mapa.bajaDePunto(0);
		
		assertTrue(!mapa.getPuntosDeInteres().contains(linea19));

	}

	@Test(expected = Exception.class)
	public void testBajaPuntoDeInteresNoExistente() throws Exception{

		mapa.bajaDePunto(1);

	}	

	@Test
	public void testEncuentraEnServicioExtSiNoEstaNiEnRepoNiEnCache() throws Exception {

	
		resultadoBusqueda = mapa.buscarPuntoSegun("Flores");
		
		assertTrue(resultadoBusqueda.getResultado().size()==1);
	}


	
	@Test
	public void testGuardaEnCacheSiFueEncontradoEnServExterno() throws Exception {

		
		mapa.buscarPuntoSegun("Flores");
		
		
		assertTrue(CacheManager.getInstance().matchSearch("Flores").size()==1);
		
	}



	@Test
	public void testEncuentraPuntoDeInteresPorId() throws IllegalStateException{

		mcDonalds.setId(5);
		
		assertTrue(mapa.obtenerPunto(5).getNombre().equals("Mcdonalds"));
	}


	@Test
	public void testEncuentraEnRepo() throws Exception {

		mapa.altaDePunto(itau);
        
		resultadoBusqueda = mapa.buscarPuntoSegun("ita");
		
        assertTrue(resultadoBusqueda.getResultado().contains(itau));
		
		
	}

		
	
	/* -------------------- Fin Tests Entrega 2 -------------------- */

}
