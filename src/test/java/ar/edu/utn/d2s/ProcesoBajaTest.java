package ar.edu.utn.d2s;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.core.config.AwaitCompletionReliabilityStrategy;
import org.junit.Before;
import org.junit.Test;

import ar.edu.utn.d2s.pdis.Colectivo;
import ar.edu.utn.d2s.pdis.PuntoDeInteres;
import ar.edu.utn.d2s.pdis.SucursalBanco;
import ar.edu.utn.d2s.procesos.AlFinalizarProceso;
import ar.edu.utn.d2s.procesos.BajaPDIS;
import ar.edu.utn.d2s.servicioGobierno.ServicioGobiernoMock;
import ar.edu.utn.d2s.serviciosExternos.InterpreteServiciosExternos;
import ar.edu.utn.d2s.serviciosExternos.CGP.ConsultorCGPsMock;
import ar.edu.utn.d2s.serviciosExternos.banco.ConsultorBancosMock;

public class ProcesoBajaTest {

	private Mapa mapa;
	private ServicioGobiernoMock servicioGobierno;
    private BajaPDIS procesoBaja;
    private SucursalBanco frances;
    private Colectivo linea19;
    private AlFinalizarProceso alFinalizar;
	
	@Before
	public void setUp() {
		
		linea19 = new Colectivo(null, null, null, null, 19);
		
		frances = new SucursalBanco("frances",null, null, null);
		
		mapa = Mapa.getInstance();
		
		mapa.altaDePunto(frances);
		mapa.altaDePunto(linea19);
		
	    servicioGobierno = new ServicioGobiernoMock();
	    servicioGobierno.inicializar();
		
	    procesoBaja = BajaPDIS.getInstance();
	    
	    procesoBaja.setServicioBajasGobierno(servicioGobierno);
	    
	    alFinalizar = new AlFinalizarProceso();
		
		
	}		
	
	
	@Test
	public void testDaDeBajaPuntosEnMapa(){
		
		HashMap<String,String> listaDeBajas = servicioGobierno.listaBajaPDIS();
		
		if(listaDeBajas.size() < 1){
			alFinalizar.correr(false);
			return;
		}

		Iterator<String> iteradorPuntos = listaDeBajas.keySet().iterator();

		while(iteradorPuntos.hasNext()){

			String valorBusqueda = iteradorPuntos.next();

			for(PuntoDeInteres unPunto : Mapa.getInstance().getPuntosDeInteres()){

				if(unPunto.verificaReferencia(valorBusqueda)){

					Mapa.getInstance().getPuntosDeInteres().remove(unPunto);
					
					break;

				}

			}

		}
		
		alFinalizar.correr(true);
		
		
		assertTrue(mapa.getPuntosDeInteres().size()==0);
				
	}
	
}

