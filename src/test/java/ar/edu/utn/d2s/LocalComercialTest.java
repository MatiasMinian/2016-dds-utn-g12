package ar.edu.utn.d2s;

import static org.junit.Assert.*;

import java.util.*;

import ar.edu.utn.d2s.pdis.*;
import org.junit.Before;
import org.junit.Test;
import org.uqbar.geodds.Point;

public class LocalComercialTest {

	private LocalComercial libreriaLocal;
	private Rubro libreriaRubro;
	private Point libreriaUbicacion;
	
	private Point puntoCercaLibreria;
	private Point puntoLejosLibreria;
	
	private HorarioAtencion horariosAtencionLibreria;
	private Map<Integer, Long> diasHabiles;
	private Calendar horaLibreriaAbierta;
	private Calendar horaLibreriaCerrada;
	private Calendar horaLibreriaAlmuerza;
	
	@Before
	public void setUp() {
		
		libreriaRubro = new Rubro(null, "Libreria", 0.6);		// 6 cuadras = 0.6 km
		Direccion libreriaUbicacion = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(-34.594883, -58.400391));	// Av. Santa Fe entre Larrea y Azcu�naga
		libreriaLocal = new LocalComercial("", "", libreriaUbicacion, null, libreriaRubro, null);
		
		puntoCercaLibreria = new Point(-34.594574, -58.396472);	// Arenales entre Junin y Ayacucho
		puntoLejosLibreria = new Point(-34.594110, -58.391052);	// Juncal entre Rodriguez Pe�a y Montevideo

		Calendar desde = Calendar.getInstance();
		desde.set(Calendar.MILLISECOND, 0);
		desde.set(Calendar.SECOND, 0);
		desde.set(Calendar.MINUTE, 0);
		desde.set(Calendar.HOUR_OF_DAY, 10);

		Calendar hasta = Calendar.getInstance();
		hasta.set(Calendar.MILLISECOND, 0);
		hasta.set(Calendar.SECOND, 0);
		hasta.set(Calendar.MINUTE, 0);
		hasta.set(Calendar.HOUR_OF_DAY, 12);

		List<Horario> horariosLibreria = new ArrayList<>();
		horariosLibreria.add(new Horario(desde, hasta, 1));
		horariosLibreria.add(new Horario(desde, hasta, 2));
		horariosLibreria.add(new Horario(desde, hasta, 3));
		horariosLibreria.add(new Horario(desde, hasta, 4));
		horariosLibreria.add(new Horario(desde, hasta, 5));

		horariosAtencionLibreria = new HorarioAtencion(horariosLibreria);

        Calendar desde2 = Calendar.getInstance();
        desde2.set(Calendar.MILLISECOND, 0);
        desde2.set(Calendar.SECOND, 0);
        desde2.set(Calendar.MINUTE, 0);
        desde2.set(Calendar.HOUR_OF_DAY, 15);
        
        Calendar hasta2 = Calendar.getInstance();
        hasta2.set(Calendar.MILLISECOND, 0);
        hasta2.set(Calendar.SECOND, 0);
        hasta2.set(Calendar.MINUTE, 0);
        hasta2.set(Calendar.HOUR_OF_DAY, 20);

		List<Horario> horariosLibreriaLocal = new ArrayList<>();
		horariosLibreria.add(new Horario(desde, hasta, 1));
		horariosLibreria.add(new Horario(desde, hasta, 2));
		horariosLibreria.add(new Horario(desde, hasta, 3));
		horariosLibreria.add(new Horario(desde, hasta, 4));
		horariosLibreria.add(new Horario(desde, hasta, 5));
		
		libreriaLocal.setHorarioAtencion(horariosAtencionLibreria);
		
		horaLibreriaAbierta = Calendar.getInstance();
		horaLibreriaAbierta.set(Calendar.MILLISECOND, 0);
		horaLibreriaAbierta.set(Calendar.SECOND, 0);
		horaLibreriaAbierta.set(Calendar.MINUTE, 0);
		horaLibreriaAbierta.set(Calendar.HOUR_OF_DAY, 11);
		horaLibreriaAbierta.set(Calendar.DAY_OF_MONTH, 18);
		horaLibreriaAbierta.set(Calendar.MONTH, 04);
		horaLibreriaAbierta.set(Calendar.YEAR, 2016);
		
		horaLibreriaCerrada = Calendar.getInstance();
		horaLibreriaCerrada.set(Calendar.MILLISECOND, 0);
		horaLibreriaCerrada.set(Calendar.SECOND, 0);
		horaLibreriaCerrada.set(Calendar.MINUTE, 05);
		horaLibreriaCerrada.set(Calendar.HOUR_OF_DAY, 13);
		horaLibreriaCerrada.set(Calendar.DAY_OF_MONTH, 17);
		horaLibreriaCerrada.set(Calendar.MONTH, 04);
		horaLibreriaCerrada.set(Calendar.YEAR, 2016);
		
		horaLibreriaAlmuerza = Calendar.getInstance();
		horaLibreriaAlmuerza.set(Calendar.MILLISECOND, 0);
		horaLibreriaAlmuerza.set(Calendar.SECOND, 0);
		horaLibreriaAlmuerza.set(Calendar.MINUTE, 30);
		horaLibreriaAlmuerza.set(Calendar.HOUR_OF_DAY, 13);
		horaLibreriaAlmuerza.set(Calendar.DAY_OF_MONTH, 18);
		horaLibreriaAlmuerza.set(Calendar.MONTH, 04);
		horaLibreriaAlmuerza.set(Calendar.YEAR, 2016);
		
	}
	
	/*------------ Tests Punto 1 ------------ */
	@Test
	public void testPuntoCercaEstaCerca() {
		
		assertTrue(libreriaLocal.estaCerca(puntoCercaLibreria));
	}
	
	@Test
	public void testPuntoLejosNoEstaCerca() {
		
		assertFalse(libreriaLocal.estaCerca(puntoLejosLibreria));
	}
	
	/*---------- Fin Tests Punto 1 ---------- */
	
	
	/*------------ Tests Punto 2 ------------ */
	@Test
	public void testEstaAbierto() {
		
		assertTrue(libreriaLocal.estaDisponible(horaLibreriaAbierta));
	}
	
	@Test
	public void testEstaCerrado() {
		
		assertFalse(libreriaLocal.estaDisponible(horaLibreriaCerrada));
	}
	
	@Test
	public void testEstaCerradoAlMediodiaEnDiaHabil() {
		
		assertFalse(libreriaLocal.estaDisponible(horaLibreriaAlmuerza));
	}
	
	/*---------- Fin Tests Punto 2 ---------- */
}
