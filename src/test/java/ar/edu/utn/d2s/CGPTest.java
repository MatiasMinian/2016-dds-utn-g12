package ar.edu.utn.d2s;

import static org.junit.Assert.*;

import java.util.*;

import ar.edu.utn.d2s.pdis.*;
import org.junit.Before;
import org.junit.Test;
import org.uqbar.geodds.Point;
import org.uqbar.geodds.Polygon;

public class CGPTest {

	private CGP comuna11;
	//private CGP comuna1;
	private Poligono polygonComuna11;
	private Polygon polygonComuna1;
	private Point puntoComuna1;
	private Point puntoComuna11;
	
	private Servicio rentas;
	private Servicio nuevoDni;
	private HorarioAtencion horarioAtencionRentas;
	private HorarioAtencion horarioAtencionNuevoDni;
	private Map<Integer, Long> diasHabiles;
	private Calendar horaQueAtiendeRentas;
	
	@Before
	public void setUp() {
				
		polygonComuna11 = new Poligono();
		polygonComuna11.add(new Point(-34.604735, -58.421991));
		polygonComuna11.add(new Point(-34.606531, -58.512906));
		polygonComuna11.add(new Point(-34.627060, -58.491239));
		polygonComuna11.add(new Point(-34.618070, -58.476127));
		polygonComuna1 = new Polygon();
		polygonComuna1.add(new Point(-34.599598, -58.392943));
		polygonComuna1.add(new Point(-34.598399, -58.369576));
		polygonComuna1.add(new Point(-34.621677, -58.368423));
		polygonComuna1.add(new Point(-34.622681, -58.391188));
		
		comuna11 = new CGP("Comuna 11", "", null, new Comuna(11, polygonComuna11), null);
		//comuna1 = new CGP("Comuna 1", null, null, null, new Comuna(1, polygonComuna1), null);
		
		puntoComuna11 = new Point(-34.611552, -58.496599);
		puntoComuna1 = new Point(-34.608661, -58.375444);

		Calendar desde = Calendar.getInstance();
		desde.set(Calendar.MILLISECOND, 0);
		desde.set(Calendar.SECOND, 0);
		desde.set(Calendar.MINUTE, 0);
		desde.set(Calendar.HOUR_OF_DAY, 9);

		Calendar hasta = Calendar.getInstance();
		hasta.set(Calendar.MILLISECOND, 0);
		hasta.set(Calendar.SECOND, 0);
		hasta.set(Calendar.MINUTE, 0);
		hasta.set(Calendar.HOUR_OF_DAY, 18);

		List<Horario> horariosRentas = new ArrayList<>();
		horariosRentas.add(new Horario(desde, hasta, 2));
		horariosRentas.add(new Horario(desde, hasta, 3));
		horariosRentas.add(new Horario(desde, hasta, 4));
		horariosRentas.add(new Horario(desde, hasta, 5));
		horariosRentas.add(new Horario(desde, hasta, 6));

		horarioAtencionRentas = new HorarioAtencion(horariosRentas);

		rentas = new Servicio(horarioAtencionRentas, "Rentas");


		Calendar desde2 = Calendar.getInstance();
        desde2.set(Calendar.MILLISECOND, 0);
        desde2.set(Calendar.SECOND, 0);
        desde2.set(Calendar.MINUTE, 0);
        desde2.set(Calendar.HOUR_OF_DAY, 9);
        
        Calendar hasta2 = Calendar.getInstance();
        hasta2.set(Calendar.MILLISECOND, 0);
        hasta2.set(Calendar.SECOND, 0);
        hasta2.set(Calendar.MINUTE, 0);
        hasta2.set(Calendar.HOUR_OF_DAY, 12);

		List<Horario> horariosNuevoDNI = new ArrayList<>();
		horariosNuevoDNI.add(new Horario(desde2, hasta2, 2));
		horariosNuevoDNI.add(new Horario(desde2, hasta2, 3));
		horariosNuevoDNI.add(new Horario(desde2, hasta2, 4));
		horariosNuevoDNI.add(new Horario(desde2, hasta2, 5));
		horariosNuevoDNI.add(new Horario(desde2, hasta2, 6));

		horarioAtencionNuevoDni = new HorarioAtencion(horariosNuevoDNI);

		nuevoDni = new Servicio(horarioAtencionNuevoDni, "Nuevo DNI");
		

		horaQueAtiendeRentas = Calendar.getInstance();
		horaQueAtiendeRentas.set(Calendar.MILLISECOND, 0);
		horaQueAtiendeRentas.set(Calendar.SECOND, 0);
		horaQueAtiendeRentas.set(Calendar.MINUTE, 0);
		horaQueAtiendeRentas.set(Calendar.HOUR_OF_DAY, 15);
		horaQueAtiendeRentas.set(Calendar.DAY_OF_WEEK, 2);
		
		comuna11.agregarServicio(rentas);
		comuna11.agregarServicio(nuevoDni);
	}
	
	/*------------ Tests Punto 1 ------------ */
	@Test
	public void testPuntoCercaEstaCerca() {
		
		assertTrue(comuna11.estaCerca(puntoComuna11));
	}
	
	@Test
	public void testPuntoLejosNoEstaCerca() {
		
		assertFalse(comuna11.estaCerca(puntoComuna1));
	}
	
	/*---------- Fin Tests Punto 1 ---------- */
	
	
	/*------------ Tests Punto 2 ------------ */
	@Test
	public void testServicioXEstaDisponible() {
		
		assertTrue(comuna11.estaDisponible(horaQueAtiendeRentas, "Rentas"));
	}
	
	@Test
	public void testHayServicioDisponible() {
		
		assertTrue(comuna11.estaDisponible(horaQueAtiendeRentas));
	}
	
	@Test
	public void testServicioXNoEstaDisponible() {
		
		assertFalse(comuna11.estaDisponible(horaQueAtiendeRentas, "Nuevo DNI"));
	}
	
	/*---------- Fin Tests Punto 2 ---------- */
}
