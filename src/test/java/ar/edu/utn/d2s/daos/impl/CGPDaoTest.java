package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.model.pdis.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

public class CGPDaoTest {

    private CGPEntity cgpBallester;

    @Before
    public void setUp() throws Exception {

        PointEntity punto1CgpBallester = new PointEntity();
        punto1CgpBallester.setX(12.4);
        punto1CgpBallester.setY(149.3);

        PointEntity punto2CgpBallester = new PointEntity();
        punto2CgpBallester.setX(-123.8);
        punto2CgpBallester.setY(-450);

        ComunaEntity comunaBallester = new ComunaEntity();
        comunaBallester.setNumero(4);
        comunaBallester.agregarPunto(punto1CgpBallester);
        comunaBallester.agregarPunto(punto2CgpBallester);

        DireccionEntity direccionCgpBallester = new DireccionEntity();
        direccionCgpBallester.setNumero(1234);
        direccionCgpBallester.setBarrio("Ballester");
        direccionCgpBallester.setCallePrincipal("Laprida");
        direccionCgpBallester.setCodigoPostal(1650);
        direccionCgpBallester.setCoordX(874);
        direccionCgpBallester.setCoordY(340.1);
        direccionCgpBallester.setDepartamento("A");
        direccionCgpBallester.setEntreCalle1("Guemes");
        direccionCgpBallester.setEntreCalle2("Juarez");
        direccionCgpBallester.setLocalidad("San Martin");
        direccionCgpBallester.setPais("Argentina");
        direccionCgpBallester.setPiso(5);
        direccionCgpBallester.setProvincia("Buenos Aires");
        direccionCgpBallester.setUnidad(8);


        Calendar horaComienzoLunesCgpBallester = Calendar.getInstance();
        horaComienzoLunesCgpBallester.set(Calendar.HOUR_OF_DAY, 10);
        horaComienzoLunesCgpBallester.set(Calendar.MINUTE, 0);
        horaComienzoLunesCgpBallester.set(Calendar.SECOND, 0);

        Calendar horaFinLunesCgpBallester = Calendar.getInstance();
        horaFinLunesCgpBallester.set(Calendar.HOUR_OF_DAY, 18);
        horaFinLunesCgpBallester.set(Calendar.MINUTE, 0);
        horaFinLunesCgpBallester.set(Calendar.SECOND, 0);

        HorarioEntity horarioLunesCgpBallester = new HorarioEntity();
        horarioLunesCgpBallester.setDia(2);
        horarioLunesCgpBallester.setHoraComienzo(horaComienzoLunesCgpBallester);
        horarioLunesCgpBallester.setHoraFin(horaFinLunesCgpBallester);


        Calendar horaComienzoMiercolesCgpBallester = Calendar.getInstance();
        horaComienzoMiercolesCgpBallester.set(Calendar.HOUR_OF_DAY, 12);
        horaComienzoMiercolesCgpBallester.set(Calendar.MINUTE, 0);
        horaComienzoMiercolesCgpBallester.set(Calendar.SECOND, 0);

        Calendar horaFinMiercolesCgpBallester = Calendar.getInstance();
        horaFinMiercolesCgpBallester.set(Calendar.HOUR_OF_DAY, 20);
        horaFinMiercolesCgpBallester.set(Calendar.MINUTE, 0);
        horaFinMiercolesCgpBallester.set(Calendar.SECOND, 0);

        HorarioEntity horarioMiercolesCgpBallester = new HorarioEntity();
        horarioMiercolesCgpBallester.setDia(4);
        horarioMiercolesCgpBallester.setHoraComienzo(horaComienzoMiercolesCgpBallester);
        horarioMiercolesCgpBallester.setHoraFin(horaFinMiercolesCgpBallester);

        HorarioDeAtencionEntity multasHorarioAtencionCgpBallester = new HorarioDeAtencionEntity();
        multasHorarioAtencionCgpBallester.agregarHorario(horarioLunesCgpBallester);
        multasHorarioAtencionCgpBallester.agregarHorario(horarioMiercolesCgpBallester);

        ServicioEntity multasCgpBallester = new ServicioEntity();
        multasCgpBallester.setNombre("Multas");
        multasCgpBallester.setHorarioDeAtencion(multasHorarioAtencionCgpBallester);

        cgpBallester = new CGPEntity();
        cgpBallester.setNombre("Centro Ballester");
        cgpBallester.setIcono("/cgp_icono.jpg");
        cgpBallester.setRangoCercania(0.5);  // Dejarlo siempre igual
        cgpBallester.setComuna(comunaBallester);
        cgpBallester.setDireccion(direccionCgpBallester);
        cgpBallester.agregarServicio(multasCgpBallester);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void saveTest() throws Exception {
        CGPDao cgpDao = new CGPDao();
        cgpDao.save(cgpBallester);
    }

    @Test
    public void updateTest() throws Exception {
    }

    @Test
    public void deleteTest() throws Exception {

    }

    @Test
    public void findByIdTest() throws Exception {

    }

}