package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.mappers.HorarioDeAtencionMapper;
import ar.edu.utn.d2s.daos.model.pdis.DireccionEntity;
import ar.edu.utn.d2s.daos.model.pdis.ServicioEntity;
import ar.edu.utn.d2s.daos.model.pdis.SucursalBancoEntity;
import ar.edu.utn.d2s.pdis.SucursalBanco;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SucursalBancoDaoTest {

    SucursalBancoEntity sucursalHSBC;

    @Before
    public void setUp() throws Exception {

        DireccionEntity direccionSucursalHSBC = new DireccionEntity();
        direccionSucursalHSBC.setNumero(1049);
        direccionSucursalHSBC.setBarrio("Vicente Lopez");
        direccionSucursalHSBC.setCallePrincipal("Carlos Melo");
        direccionSucursalHSBC.setCodigoPostal(2346);
        direccionSucursalHSBC.setCoordX(-34.527079);
        direccionSucursalHSBC.setCoordY(-58.475222);
        direccionSucursalHSBC.setDepartamento(null);
        direccionSucursalHSBC.setEntreCalle1("Gaspar Campos");
        direccionSucursalHSBC.setEntreCalle2("Dr. Eduardo Madero");
        direccionSucursalHSBC.setLocalidad("Vicente Lopez");
        direccionSucursalHSBC.setPais("Argentina");
        direccionSucursalHSBC.setPiso(null);
        direccionSucursalHSBC.setProvincia("Buenos Aires");
        direccionSucursalHSBC.setUnidad(null);

        ServicioEntity servicioAtencionAlCliente = new ServicioEntity();
        servicioAtencionAlCliente.setNombre("Atencion al cliente");

        ServicioEntity servicioCreditos = new ServicioEntity();
        servicioCreditos.setNombre("Créditos");

        sucursalHSBC = new SucursalBancoEntity();
        sucursalHSBC.setNombre("HSBC Vicente Lopez");
        sucursalHSBC.setIcono("/sucursal_banco_icono.jpg");
        sucursalHSBC.setDireccion(direccionSucursalHSBC);
        sucursalHSBC.setRangoCercania(0.5); // Dejarlo siempre igual
        sucursalHSBC.setHorarioDeAtencion(new HorarioDeAtencionMapper().mapToEntity(new SucursalBanco().getHorarioAtencion()));
        sucursalHSBC.agregarServicio(servicioAtencionAlCliente);
        sucursalHSBC.agregarServicio(servicioCreditos);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void save() throws Exception {
        SucursalBancoDao sucursalBancoDao = new SucursalBancoDao();
        sucursalBancoDao.save(sucursalHSBC);

    }

    @Test
    public void update() throws Exception {

    }

    @Test
    public void delete() throws Exception {

    }

    @Test
    public void findById() throws Exception {

    }

}