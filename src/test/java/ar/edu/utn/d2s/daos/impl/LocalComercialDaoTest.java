package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.model.pdis.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

public class LocalComercialDaoTest {

    private LocalComercialEntity localKiosko;

    @Before
    public void setUp() throws Exception {

        DireccionEntity direccionLocalKiosko = new DireccionEntity();
        direccionLocalKiosko.setNumero(5366);
        direccionLocalKiosko.setBarrio("Villa Urquiza");
        direccionLocalKiosko.setCallePrincipal("Cullen");
        direccionLocalKiosko.setCodigoPostal(6780);
        direccionLocalKiosko.setCoordX(-34.574479);
        direccionLocalKiosko.setCoordY(-58.490787);
        direccionLocalKiosko.setDepartamento(null);
        direccionLocalKiosko.setEntreCalle1("Andonaegui");
        direccionLocalKiosko.setEntreCalle2("Bucarelli");
        direccionLocalKiosko.setLocalidad("Capital Federal");
        direccionLocalKiosko.setPais("Argentina");
        direccionLocalKiosko.setPiso(null);
        direccionLocalKiosko.setProvincia("Buenos Aires");
        direccionLocalKiosko.setUnidad(null);

        RubroEntity rubroAlmacen = new RubroEntity();
        rubroAlmacen.setNombre("Almacen");
        rubroAlmacen.setRangoCercania(0.2);

        PalabraClaveEntity palabraClavePirulo = new PalabraClaveEntity();
        palabraClavePirulo.setPalabra("Pirulo");
        PalabraClaveEntity palabraClaveKiosko = new PalabraClaveEntity();
        palabraClaveKiosko.setPalabra("Kiosco");
        PalabraClaveEntity palabraClaveGolosinas = new PalabraClaveEntity();
        palabraClaveGolosinas.setPalabra("Golosinas");

        Calendar horaComienzoLunesKioskoPirulo = Calendar.getInstance();
        horaComienzoLunesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 2);
        horaComienzoLunesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinLunesKioskoPirulo = Calendar.getInstance();
        horaFinLunesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 2);
        horaFinLunesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioLunesKioskoPirulo = new HorarioEntity();
        horarioLunesKioskoPirulo.setDia(2);
        horarioLunesKioskoPirulo.setHoraComienzo(horaComienzoLunesKioskoPirulo);
        horarioLunesKioskoPirulo.setHoraFin(horaFinLunesKioskoPirulo);


        Calendar horaComienzoMartesKioskoPirulo = Calendar.getInstance();
        horaComienzoMartesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 3);
        horaComienzoMartesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinMartesKioskoPirulo = Calendar.getInstance();
        horaFinMartesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 3);
        horaFinMartesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioMartesKioskoPirulo = new HorarioEntity();
        horarioMartesKioskoPirulo.setDia(3);
        horarioMartesKioskoPirulo.setHoraComienzo(horaComienzoMartesKioskoPirulo);
        horarioMartesKioskoPirulo.setHoraFin(horaFinMartesKioskoPirulo);


        Calendar horaComienzoMiercolesKioskoPirulo = Calendar.getInstance();
        horaComienzoMiercolesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 4);
        horaComienzoMiercolesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinMiercolesKioskoPirulo = Calendar.getInstance();
        horaFinMiercolesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 4);
        horaFinMiercolesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioMiercolesKioskoPirulo = new HorarioEntity();
        horarioMiercolesKioskoPirulo.setDia(4);
        horarioMiercolesKioskoPirulo.setHoraComienzo(horaComienzoMiercolesKioskoPirulo);
        horarioMiercolesKioskoPirulo.setHoraFin(horaFinMiercolesKioskoPirulo);


        Calendar horaComienzoJuevesKioskoPirulo = Calendar.getInstance();
        horaComienzoJuevesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 5);
        horaComienzoJuevesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinJuevesKioskoPirulo = Calendar.getInstance();
        horaFinJuevesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 5);
        horaFinJuevesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioJuevesKioskoPirulo = new HorarioEntity();
        horarioJuevesKioskoPirulo.setDia(5);
        horarioJuevesKioskoPirulo.setHoraComienzo(horaComienzoJuevesKioskoPirulo);
        horarioJuevesKioskoPirulo.setHoraFin(horaFinJuevesKioskoPirulo);


        Calendar horaComienzoViernesKioskoPirulo = Calendar.getInstance();
        horaComienzoViernesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 6);
        horaComienzoViernesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinViernesKioskoPirulo = Calendar.getInstance();
        horaFinViernesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 6);
        horaFinViernesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioViernesKioskoPirulo = new HorarioEntity();
        horarioViernesKioskoPirulo.setDia(6);
        horarioViernesKioskoPirulo.setHoraComienzo(horaComienzoViernesKioskoPirulo);
        horarioViernesKioskoPirulo.setHoraFin(horaFinViernesKioskoPirulo);


        HorarioDeAtencionEntity horarioAtencionKioskoPirulo = new HorarioDeAtencionEntity();
        horarioAtencionKioskoPirulo.agregarHorario(horarioLunesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioMartesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioMiercolesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioJuevesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioViernesKioskoPirulo);


        localKiosko = new LocalComercialEntity();
        localKiosko.setNombre("Kiosko Pirulo");
        localKiosko.setIcono("/local_comercial_icono.jpg");
        localKiosko.setRangoCercania(0d);
        localKiosko.setDireccion(direccionLocalKiosko);
        localKiosko.setRubro(rubroAlmacen);
        localKiosko.agregarPalabraClave(palabraClavePirulo);
        localKiosko.agregarPalabraClave(palabraClaveKiosko);
        localKiosko.agregarPalabraClave(palabraClaveGolosinas);
        localKiosko.setHorarioDeAtencion(horarioAtencionKioskoPirulo);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void saveTest() throws Exception {
        LocalComercialDao localComercialDao = new LocalComercialDao();
        localComercialDao.save(localKiosko);
    }

    @Test
    public void updateTest() throws Exception {

    }

    @Test
    public void deleteTest() throws Exception {

    }

    @Test
    public void findByIdTest() throws Exception {

    }

}