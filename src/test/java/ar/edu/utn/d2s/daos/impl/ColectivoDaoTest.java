package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.model.pdis.ColectivoEntity;
import ar.edu.utn.d2s.daos.model.pdis.DireccionEntity;
import ar.edu.utn.d2s.daos.model.pdis.ParadaEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ColectivoDaoTest {

    ColectivoEntity colectivo114;
    DireccionEntity direccionColectivo114;

    @Before
    public void setUp() throws Exception {

        direccionColectivo114 = new DireccionEntity();
        direccionColectivo114.setNumero(1401);
        direccionColectivo114.setBarrio("Belgrano");
        direccionColectivo114.setCallePrincipal("Arribeños");
        direccionColectivo114.setCodigoPostal(1490);
        direccionColectivo114.setCoordX(-34.564289);
        direccionColectivo114.setCoordY(-58.445050);
        direccionColectivo114.setDepartamento(null);
        direccionColectivo114.setEntreCalle1("Zabala");
        direccionColectivo114.setEntreCalle2("Virrey Loreto");
        direccionColectivo114.setLocalidad("Capital Federal");
        direccionColectivo114.setPais("Argentina");
        direccionColectivo114.setPiso(null);
        direccionColectivo114.setProvincia("Buenos Aires");
        direccionColectivo114.setUnidad(null);

        DireccionEntity direccionColectivo114Parada1;
        direccionColectivo114Parada1 = new DireccionEntity();
        direccionColectivo114Parada1.setNumero(1730);
        direccionColectivo114Parada1.setBarrio("Belgrano");
        direccionColectivo114Parada1.setCallePrincipal("José Hernández");
        direccionColectivo114Parada1.setCodigoPostal(1490);
        direccionColectivo114Parada1.setCoordX(-34.562257);
        direccionColectivo114Parada1.setCoordY(-58.447283);
        direccionColectivo114Parada1.setDepartamento(null);
        direccionColectivo114Parada1.setEntreCalle1("11 de Septiembre");
        direccionColectivo114Parada1.setEntreCalle2("Arribeños");
        direccionColectivo114Parada1.setLocalidad("Capital Federal");
        direccionColectivo114Parada1.setPais("Argentina");
        direccionColectivo114Parada1.setPiso(null);
        direccionColectivo114Parada1.setProvincia("Buenos Aires");
        direccionColectivo114Parada1.setUnidad(null);

        DireccionEntity direccionColectivo114Parada2;
        direccionColectivo114Parada2 = new DireccionEntity();
        direccionColectivo114Parada2.setNumero(2390);
        direccionColectivo114Parada2.setBarrio("Belgrano");
        direccionColectivo114Parada2.setCallePrincipal("José Hernández");
        direccionColectivo114Parada2.setCodigoPostal(1490);
        direccionColectivo114Parada2.setCoordX(-34.565476);
        direccionColectivo114Parada2.setCoordY(-58.452874);
        direccionColectivo114Parada2.setDepartamento(null);
        direccionColectivo114Parada2.setEntreCalle1("Vuelta de Obligado");
        direccionColectivo114Parada2.setEntreCalle2("Av. Cabildo");
        direccionColectivo114Parada2.setLocalidad("Capital Federal");
        direccionColectivo114Parada2.setPais("Argentina");
        direccionColectivo114Parada2.setPiso(null);
        direccionColectivo114Parada2.setProvincia("Buenos Aires");
        direccionColectivo114Parada2.setUnidad(null);

        ParadaEntity parada1Colectivo114 = new ParadaEntity();
        parada1Colectivo114.setDireccion(direccionColectivo114Parada1);

        ParadaEntity parada2Colectivo114 = new ParadaEntity();
        parada2Colectivo114.setDireccion(direccionColectivo114Parada2);

        colectivo114 = new ColectivoEntity();
        colectivo114.setNombre("Colectivos SanMartin srl");
        colectivo114.setIcono("/colectivo_icono.jpg");
        colectivo114.setRangoCercania(0.1);  // Dejarlo siempre igual
        colectivo114.setNroLinea(114);
        colectivo114.setDireccion(direccionColectivo114);
        colectivo114.agregarParada(parada1Colectivo114);
        colectivo114.agregarParada(parada2Colectivo114);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void saveTest() throws Exception {
        ColectivoDao colectivoDao = new ColectivoDao();
        colectivoDao.save(colectivo114);
    }

    @Test
    public void updateTest() throws Exception {

    }

    @Test
    public void deleteTest() throws Exception {

    }

}