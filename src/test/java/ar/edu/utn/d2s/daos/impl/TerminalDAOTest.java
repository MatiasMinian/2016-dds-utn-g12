package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.model.TerminalEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class TerminalDAOTest {

    private TerminalEntity terminalIvan;
    private TerminalEntity terminalFederico;
    private TerminalEntity terminalKevin;

    @Before
    public void setUp() throws Exception {

        terminalIvan = new TerminalEntity("icernigoj", "$2a$10$e0MYzXyjpJS7Pd0RVvHwHe1HlCS4bZJ18JuywdEMLT83E1KDmUhCy", "icernigoj@gmail.com", "$2a$10$e0MYzXyjpJS7Pd0RVvHwHe",
                false, new ArrayList<>(), new ArrayList<>(), "icernigoj");

        terminalFederico = new TerminalEntity("federico", "$2a$10$E3DgchtVry3qlYlzJCsyxeSK0fftK4v0ynetVCuDdxGVl1obL.ln2", "federico@gmail.com", "$2a$10$E3DgchtVry3qlYlzJCsyxe",
                false, new ArrayList<>(), new ArrayList<>(), "federico");

        terminalKevin = new TerminalEntity("kevin", "$2a$10$h.dl5J86rGH7I8bD9bZeZeci0pDt0.VwFTGujlnEaZXPf/q7vM5wO", "kevin@gmail.com", "$2a$10$h.dl5J86rGH7I8bD9bZeZe",
                false, new ArrayList<>(), new ArrayList<>(), "kevin");



    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void saveTest() throws Exception {
        TerminalDAO terminalDAO = new TerminalDAO();
        terminalDAO.save(terminalIvan);
        terminalDAO.save(terminalFederico);
        terminalDAO.save(terminalKevin);

    }

    @Test
    public void updateTest() throws Exception {

    }

    @Test
    public void deleteTest() throws Exception {

    }

    @Test
    public void findByIdTest() throws Exception {

    }

}