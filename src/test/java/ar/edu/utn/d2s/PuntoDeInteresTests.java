package ar.edu.utn.d2s;

import org.uqbar.geodds.Point;

import ar.edu.utn.d2s.pdis.CGP;
import ar.edu.utn.d2s.pdis.Colectivo;
import ar.edu.utn.d2s.pdis.LocalComercial;
import ar.edu.utn.d2s.pdis.SucursalBanco;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PuntoDeInteresTests {

	
	//Armado de los juegos de datos
	private Colectivo linea114;
	private Colectivo linea41;
	private Colectivo linea67;
	private Colectivo linea161;
	private Colectivo linea29;
	private Colectivo linea144;
	private Colectivo linea333;
	private Colectivo linea343;
	private Colectivo linea71;
	private Colectivo linea93;

	private CGP cgp1;
	private CGP cgp2;
	private CGP cgp3;
	private CGP cgp4;
	private CGP cgp5;
	private CGP cgp6;
	private CGP cgp7;
	private CGP cgp8;
	private CGP cgp9;
	private CGP cgp10;
	
	private LocalComercial carwash;
	private LocalComercial mcdonalds;
	private LocalComercial burguerking;
	private LocalComercial pancho46;
	private LocalComercial lavanderia;
	private LocalComercial coto;
	private LocalComercial walmart;
	private LocalComercial dia;
	private LocalComercial kiosko;
	private LocalComercial locutorio;

	private SucursalBanco hsbc;
	private SucursalBanco patagonia;
	private SucursalBanco frances;
	private SucursalBanco galicia;
	private SucursalBanco comafi;
	private SucursalBanco itau;
	private SucursalBanco piano;
	private SucursalBanco supervielle;
	private SucursalBanco icbc;
	private SucursalBanco nacion;
	
	@Before
	public void setUp() throws Exception {
		
		frances = new SucursalBanco("frances",null,null, null);

	}

	@Test
	public void testNombrePuntoDistintoVacio(){

        assertNotEquals("",frances.getNombre());

	}

	@Test
	public void testNombrePuntoMenor60Caracteres(){

		assertTrue(frances.getNombre().length() <= 60);

	}
}