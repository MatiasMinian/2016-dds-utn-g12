package ar.edu.utn.d2s;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import ar.edu.utn.d2s.daos.Dataset;
import ar.edu.utn.d2s.pdis.PalabraClave;
import ar.edu.utn.d2s.pdis.ResultadoBusqueda;

import org.junit.Before;
import org.junit.Test;
import org.uqbar.geodds.Point;

import ar.edu.utn.d2s.pdis.Colectivo;
import ar.edu.utn.d2s.pdis.Direccion;
import ar.edu.utn.d2s.pdis.Horario;
import ar.edu.utn.d2s.pdis.HorarioAtencion;
import ar.edu.utn.d2s.pdis.LocalComercial;
import ar.edu.utn.d2s.pdis.Rubro;
import ar.edu.utn.d2s.procesos.ActualizarLocalesComerciales;
import ar.edu.utn.d2s.procesos.AlFinalizarProceso;
import ar.edu.utn.d2s.usuarios.Usuario;

public class ActualizarLocalesComercialesTest {

	private ActualizarLocalesComerciales actualizador;
	private Mapa mapa;
	private LocalComercial libreriaLocal;
	private Rubro libreriaRubro;
	private List<PalabraClave> palabrasClave;
	private HorarioAtencion horariosAtencionLibreria;
	private AlFinalizarProceso alFinalizarProceso;
	private ResultadoBusqueda resultadoBusqueda;

	@Before
	public void setUp() {

		palabrasClave = new ArrayList<PalabraClave>();
		palabrasClave.add(new PalabraClave(null, "Cacho"));
		palabrasClave.add(new PalabraClave(null, "Libros"));
		palabrasClave.add(new PalabraClave(null, "Biromes"));
		palabrasClave.add(new PalabraClave(null, "Libreria"));

		actualizador = ActualizarLocalesComerciales.getInstance();
		
		mapa = Mapa.getInstance();
		
		Calendar desde = Calendar.getInstance();
		desde.set(Calendar.MILLISECOND, 0);
		desde.set(Calendar.SECOND, 0);
		desde.set(Calendar.MINUTE, 0);
		desde.set(Calendar.HOUR_OF_DAY, 10);

		Calendar hasta = Calendar.getInstance();
		hasta.set(Calendar.MILLISECOND, 0);
		hasta.set(Calendar.SECOND, 0);
		hasta.set(Calendar.MINUTE, 0);
		hasta.set(Calendar.HOUR_OF_DAY, 12);

		List<Horario> horariosLibreria = new ArrayList<>();
		horariosLibreria.add(new Horario(desde, hasta, 1));
		horariosLibreria.add(new Horario(desde, hasta, 2));
		horariosLibreria.add(new Horario(desde, hasta, 3));
		horariosLibreria.add(new Horario(desde, hasta, 4));
		horariosLibreria.add(new Horario(desde, hasta, 5));

		horariosAtencionLibreria = new HorarioAtencion(horariosLibreria);
		
		libreriaRubro = new Rubro(null, "Libreria", 0.6);		// 6 cuadras = 0.6 km

		libreriaLocal = new LocalComercial("Libreria Cacho", null, null, horariosAtencionLibreria, libreriaRubro, palabrasClave);
			
		alFinalizarProceso = new AlFinalizarProceso();
		
				
	}

	@Test
	public void testActualizaLocalComercialNoExistente() throws Exception{

		actualizador.setPath("resources/testLocalComercialNoExistente.txt");

		actualizador.ejecutar(alFinalizarProceso);
		
		assertTrue(mapa.getPuntosDeInteres().stream().anyMatch(punto->punto.getNombre().equals("Lavarap")));
		
	}
	
	@Test
	public void testActualizaLocalComercialExistente() throws Exception {

		mapa.altaDePunto(libreriaLocal);
		
		actualizador.setPath("resources/testLocalComercialExistente.txt");

		actualizador.ejecutar(alFinalizarProceso);
		
		assertTrue(libreriaLocal.getPalabrasClave().stream().anyMatch(palabraClave->palabraClave.getPalabra().equals("Tijera")));
		assertTrue(libreriaLocal.getPalabrasClave().stream().anyMatch(palabraClave->palabraClave.getPalabra().equals("Lapicera")));
		assertTrue(libreriaLocal.getPalabrasClave().stream().anyMatch(palabraClave->palabraClave.getPalabra().equals("Cartuchera")));
		
	}

	
}
