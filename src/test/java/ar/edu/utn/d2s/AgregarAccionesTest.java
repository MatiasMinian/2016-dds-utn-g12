package ar.edu.utn.d2s;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ar.edu.utn.d2s.acciones.Accion;
import ar.edu.utn.d2s.acciones.AccionMock1;
import ar.edu.utn.d2s.acciones.AccionMock2;
import ar.edu.utn.d2s.acciones.ReporteDeBusquedaPorFechaPorTerminal;
import ar.edu.utn.d2s.acciones.ReporteDeBusquedaPorTerminal;
import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.daos.mappers.TerminalMapper;
import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.procesos.AgregarAcciones;
import ar.edu.utn.d2s.procesos.AlFinalizarProceso;
import ar.edu.utn.d2s.usuarios.AdministradorDeUsuarios;
import ar.edu.utn.d2s.usuarios.Terminal;
import ar.edu.utn.d2s.usuarios.Usuario;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AgregarAccionesTest {
	private Usuario terminal1 = new Terminal("pablo_exe@hotmail.com");
	private Usuario terminal2 = new Terminal("minian.matias@gmail.com");
	private List<Usuario> listaUsuarios =  new ArrayList<>();
	private AgregarAcciones agregarAccionesProceso = AgregarAcciones.getInstance();
	private List<Accion> acciones = new ArrayList<>();
	private ReporteDeBusquedaPorFechaPorTerminal rbft;
	private ReporteDeBusquedaPorTerminal rbt;
	private List<Terminal> terminales;
	private TerminalDAO terminalDAO;
	private TerminalMapper terminalMapper;
	private AlFinalizarProceso alFinalizarProceso;

	@Before
	public void setUp() throws Exception {

		rbft = new ReporteDeBusquedaPorFechaPorTerminal();
		rbt = new ReporteDeBusquedaPorTerminal();
		
		acciones.add(rbft);
		acciones.add(rbt);
		agregarAccionesProceso.setAcciones(acciones);
		
		terminalDAO = new TerminalDAO();
		terminalMapper = new TerminalMapper();
		alFinalizarProceso = new AlFinalizarProceso();

	}


	@Test
	public void testEjecutar() {
		
		List<Terminal> terminales = terminalDAO.findAll(TerminalEntity.class).stream().map(terminalEntity -> terminalMapper.mapFromEntity(terminalEntity)).collect(Collectors.toList());

		terminales.forEach(terminal -> terminal.agregarAcciones(acciones));
		alFinalizarProceso.correr(true);
		
		assertTrue(terminales.stream().allMatch(terminal->terminal.getAcciones().size()==2));
		
	}
	

}
