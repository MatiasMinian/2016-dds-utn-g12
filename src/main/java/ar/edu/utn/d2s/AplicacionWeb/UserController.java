package ar.edu.utn.d2s.AplicacionWeb;

import static ar.edu.utn.d2s.AplicacionWeb.AplicacionWeb.userDao;

import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.daos.impl.UsuarioDao;
import ar.edu.utn.d2s.usuarios.Usuario;
import org.mindrot.jbcrypt.BCrypt;

import ar.edu.utn.d2s.AplicacionWebUser.User;

public class UserController {
    // Authenticate the user by hashing the inputted password using the stored salt,
    // then comparing the generated hashed password to the stored hashed password
    public static boolean authenticate(String username, String password) {
        if (username.isEmpty() || password.isEmpty()) {
            return false;
        }
        //User user = userDao.getUserByUsername(username);
        UsuarioDao usuarioDao = new UsuarioDao();
        Usuario usuario = usuarioDao.getUsuarioByName(username);

        if (usuario == null) {
            return false;
        }
        String hashedPassword = BCrypt.hashpw(password, usuario.getSalt());
        return hashedPassword.equals(usuario.getPassword());
    }

    //Cambio de contraseña
    public static void setPassword(String username, String oldPassword, String newPassword) {
        if (authenticate(username, oldPassword)) {
            String newSalt = BCrypt.gensalt();
            String newHashedPassword = BCrypt.hashpw(newSalt, newPassword);

            //TODO: Obtener usuario actual o pasarlo por parametro y setearle neva password
        }
    }
}
