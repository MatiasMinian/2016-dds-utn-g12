package ar.edu.utn.d2s.AplicacionWeb;
import static spark.Spark.after;
import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.delete;
import static spark.Spark.put;
import static spark.Spark.staticFiles;
import static spark.debug.DebugScreen.enableDebugScreen;

import ar.edu.utn.d2s.AplicacionWebUser.UserDao;
import ar.edu.utn.d2s.AplicacionWebUtil.Filters;
import ar.edu.utn.d2s.AplicacionWebUtil.Path;
import ar.edu.utn.d2s.AplicacionWebUtil.ViewUtil;
import ar.edu.utn.d2s.daos.Dataset;

import javax.xml.crypto.Data;

public class AplicacionWeb {

    public static UserDao userDao;

    public static void main(String[] args) {

        // Instantiate your dependencies
        userDao = new UserDao();
        Dataset dataset = new Dataset();
        dataset.crearDataset();

		 // Configure Spark
        port(4567);
        staticFiles.location("/public");
        staticFiles.expireTime(600L);
        enableDebugScreen();

        // Set up before-filters (called before each get/post)
        //before("*",                  Filters.addTrailingSlashes);
        before("*",                  Filters.handleLocaleChange);

        // Set up routes
        get(Path.Web.INDEX,          IndexController.serveIndexPage);
        get(Path.Web.BUSCAR,         BusquedaController.serveIndexPage);
        get(Path.Web.PDIS,         	 BusquedaController.serveSearch);
        get(Path.Web.ACCIONES,       AccionesController.serveAccionesPage);
        put(Path.Web.ACCION,		 AccionesController.putAccion);
        delete(Path.Web.ACCION,		 AccionesController.deleteAccion);
        get(Path.Web.HISTORIAL,      HistorialController.serveHistorialPage);
        get(Path.Web.HISTORIALBUSQUEDA, HistorialController.serveHistorialSearchResults);
        get(Path.Web.LOGIN,          LoginController.serveLoginPage);
        post(Path.Web.LOGIN,         LoginController.handleLoginPost);
        post(Path.Web.LOGOUT,        LoginController.handleLogoutPost);
        get("*",                     ViewUtil.notFound);

        //Set up after-filters (called after each get/post)
        after("*",                   Filters.addGzipHeader);
	}

}
