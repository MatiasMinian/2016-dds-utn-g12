package ar.edu.utn.d2s.AplicacionWeb;

import java.util.HashMap;
import java.util.Map;

import ar.edu.utn.d2s.AplicacionWebUtil.Path;
import ar.edu.utn.d2s.AplicacionWebUtil.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Route;
import static ar.edu.utn.d2s.AplicacionWeb.AplicacionWeb.*;


public class IndexController {
    public static Route serveIndexPage = (Request request, Response response) -> {
        Map<String, Object> model = new HashMap<>();
        model.put("users", userDao.getAllUserNames());
        //model.put("book", bookDao.getRandomBook());
        return ViewUtil.render(request, model, Path.Template.INDEX);
    };
}
