package ar.edu.utn.d2s.AplicacionWeb;

import ar.edu.utn.d2s.acciones.*;
import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.usuarios.Terminal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;

import ar.edu.utn.d2s.AplicacionWebUtil.Path;
import static ar.edu.utn.d2s.AplicacionWebUtil.RequestUtil.*;
import ar.edu.utn.d2s.AplicacionWebUtil.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Route;

public class AccionesController {
	public static Route serveAccionesPage = (Request request, Response response) -> {
		 Map<String, Object> model = new HashMap<>();
		 List<Accion> accionesRepo = new ArrayList<>();
		 String currUsername = getSessionCurrentUser(request);
		 Terminal currentUser = new TerminalDAO().findByName(currUsername);
		 Accion accionAux;
		 Integer accionId;
		 Boolean accionStatus;
		 Map<String,Object> accionHashMapAux;
		 List<Map<String,Object>> accionesMenu = new ArrayList<>();
		 
		 if(currentUser != null) {
			 accionesRepo.add(new ReporteDeBusquedaPorFechaPorTerminal());
			 accionesRepo.add(new ReporteDeBusquedaPorTerminal());
		 }
		 
		 Iterator<Accion> iter = accionesRepo.iterator();
		 while( iter.hasNext() ) {
			 accionAux = iter.next();
			 accionHashMapAux = new HashMap<>();
			 accionHashMapAux.put("accionObj", accionAux);
			 accionId = accionAux.getActionId();
			 accionStatus = hasEqualId(currentUser, accionId);
			 accionHashMapAux.put("enabled", accionStatus);
			 accionesMenu.add(accionHashMapAux);
		 }
		 
		 model.put("accionesMenu", accionesMenu);

		 return ViewUtil.render(request, model, Path.Template.ACCIONES);
	};
	
	private static Boolean hasEqualId(Terminal user, Integer comparedId) {
		return user.getAcciones().stream().anyMatch(accion -> accion.getActionId().equals(comparedId));
	}
	
	public static Route putAccion = (Request request, Response response) -> {
		
		//HashMap<String,Object> reqBody = new ObjectMapper().readValue(request.body(), HashMap.class);
		
		Integer accionId = Integer.parseInt(request.params(":id"));
		String currUsername = getSessionCurrentUser(request);		
		List<Accion> acciones = new ArrayList<>();
        TerminalDAO terminalDAO = new TerminalDAO();
        Optional<Accion> accionOpt;
		acciones.add(new ReporteDeBusquedaPorFechaPorTerminal());
		acciones.add(new ReporteDeBusquedaPorTerminal());

		Terminal currentUser = terminalDAO.findByName(currUsername);
		
		if( !currentUser.getAcciones().stream().anyMatch(accion -> accion.getActionId().equals(accionId)) ) {
			accionOpt = acciones.stream().filter(accion -> accion.getActionId().equals(accionId)).findFirst();
			if(accionOpt.isPresent()) {
				currentUser.agregarAccion(accionOpt.get());
				terminalDAO.update(currentUser);
			}
		}
		
		return "{result: OK}";
	};
	
	public static Route deleteAccion = (Request request, Response response) -> {
		
		Integer accionId = Integer.parseInt(request.params(":id"));
		String currUsername = getSessionCurrentUser(request);		
		TerminalDAO terminalDAO = new TerminalDAO();
		Terminal currentUser = terminalDAO.findByName(currUsername);
		Optional<Accion> accionOpt;
		
		if( currentUser.getAcciones().stream().anyMatch(accion -> accion.getActionId().equals(accionId)) ) {
			accionOpt = currentUser.getAcciones().stream().filter(accion -> accion.getActionId().equals(accionId)).findFirst();
			if(accionOpt.isPresent()) {
				currentUser.getAcciones().remove( accionOpt.get() );
				terminalDAO.update(currentUser);
			}
		}
		return "{result: OK}";
	};
}
