package ar.edu.utn.d2s.AplicacionWeb;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.pdis.ResultadoBusqueda;
import ar.edu.utn.d2s.usuarios.Terminal;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ar.edu.utn.d2s.Mapa;
import ar.edu.utn.d2s.AplicacionWebUtil.Path;
import ar.edu.utn.d2s.AplicacionWebUtil.ViewUtil;
import spark.Request;
import spark.Response;
import spark.Route;

import static ar.edu.utn.d2s.AplicacionWebUtil.RequestUtil.getSessionCurrentUser;

public class BusquedaController {
    public static Route serveIndexPage = (Request request, Response response) -> {
        Map<String, Object> model = new HashMap<>();

        return ViewUtil.render(request, model, Path.Template.BUSCAR);
    };
    
    public static Route serveSearch = (Request request, Response response) -> {
        StringWriter swPuntosDeInteres = new StringWriter();
        String queryTexto = request.queryParams("texto");
        Mapa mapa = Mapa.getInstance();
        String jsonResponse;
        TerminalDAO terminalDAO = new TerminalDAO();
        ResultadoBusqueda resultadoBusqueda = mapa.buscarPuntoSegun(queryTexto);

        String currUsername = getSessionCurrentUser(request);
        Terminal currentUser = terminalDAO.findByName(currUsername);
        currentUser.almacenarResultadosDeBusqueda(resultadoBusqueda);

        currentUser.ejecutarAcciones();

        terminalDAO.update(currentUser);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.writeValue(swPuntosDeInteres, resultadoBusqueda.getResultado());
        jsonResponse = swPuntosDeInteres.toString();
        swPuntosDeInteres.close();

        return jsonResponse;
    };
}
