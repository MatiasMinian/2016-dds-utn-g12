package ar.edu.utn.d2s.AplicacionWeb;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.AplicacionWebUtil.Path;
import ar.edu.utn.d2s.AplicacionWebUtil.ViewUtil;
import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.pdis.ResultadoBusqueda;
import ar.edu.utn.d2s.usuarios.Terminal;
import spark.Request;
import spark.Response;
import spark.Route;

public class HistorialController {
	public static Route serveHistorialPage = (Request request, Response response) -> {
		 Map<String, Object> model = new HashMap<>();
		 Boolean showResultsPanel = false;
		 
		 model.put("showResultsPanel", showResultsPanel);
		 model.put("results", new HashMap<>());
		 
		 return ViewUtil.render(request, model, Path.Template.HISTORIAL);
	};
	
	public static Route serveHistorialSearchResults = (Request request, Response response) -> {
		Map<String, Object> model = new HashMap<>();
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd");
		
		Boolean showResultsPanel = true;
		
		String queryUsername = request.queryParams("username");
		String queryDateFrom = request.queryParams("dateFrom");
		String queryDateTo = request.queryParams("dateTo").contentEquals("") ? formatDate.format(Calendar.getInstance().getTime()) : request.queryParams("dateTo");;
		
		Terminal queryUser = new TerminalDAO().findByName(queryUsername);
		
		model.put("showResultsPanel", showResultsPanel);
		
		if(queryUser == null || queryUser.getClass().getName() != "ar.edu.utn.d2s.usuarios.Terminal")		
			model.put("results", new HashMap<>());
		else {
			model.put("results", queryUser.getResultadoBusquedas().stream().filter( r -> isInGivenRange(r, queryDateFrom, queryDateTo) ).collect(Collectors.toList()));
			model.put("username", queryUser.getUsername());
		}
		
		return ViewUtil.render(request, model, Path.Template.HISTORIAL);
	};
	
	private static Boolean isInGivenRange(ResultadoBusqueda res, String dateFrom, String dateTo) {
		
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		
		String resDateStr = formatDate.format(res.getFecha().getTime());
		
		return resDateStr.compareTo(dateFrom) >= 0 && resDateStr.compareTo(dateTo) <= 0;
	};
}
