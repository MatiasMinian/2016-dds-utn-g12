package ar.edu.utn.d2s.serviciosExternos.banco;

import java.util.List;

public class BancoExternoDTO {
	private String banco;
	private double x;
	private double y;
	private String sucursal;
	private String gerente;
	private List<String> servicios;

	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getGerente() {
		return gerente;
	}
	public void setGerente(String gerente) {
		this.gerente = gerente;
	}
	public List<String> getServicios() {
		return servicios;
	}
	public void setServicios(List<String> servicios) {
		this.servicios = servicios;
	}


	public boolean verificaReferencia(String nombre, String servicio) {

		return (this.banco.equalsIgnoreCase(nombre) && this.servicios.stream().anyMatch(s -> s.equalsIgnoreCase(servicio)));

		// todasLasOpciones.stream().anyMatch(p -> p.equalsIgnoreCase(this.banco)) && this.servicios.stream().anyMatch(s -> todasLasOpciones.contains(s));
	}


}
