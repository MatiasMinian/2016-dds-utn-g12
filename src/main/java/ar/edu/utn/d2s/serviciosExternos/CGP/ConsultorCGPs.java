package ar.edu.utn.d2s.serviciosExternos.CGP;

import java.util.List;

public interface ConsultorCGPs {
	
	public List<CentroDTO> consultarCGPs(String dato);

}
