package ar.edu.utn.d2s.serviciosExternos.banco;

import com.google.gson.Gson;
import java.util.*;
import java.util.stream.Collectors;

public class ConsultorBancosMock implements ConsultorBancos {

    private Set<BancoExternoDTO> bancos = new HashSet<>();

    public ConsultorBancosMock() {
        BancoExternoDTO bancoExternoDTO = new BancoExternoDTO();
        bancoExternoDTO.setBanco("Banco de la plaza");
        bancoExternoDTO.setGerente("Javier Loeschbor");
        bancoExternoDTO.setSucursal("Avellaneda");
        bancoExternoDTO.setX(-35.933822);
        bancoExternoDTO.setY(72.348353);
        bancoExternoDTO.setServicios(new ArrayList<>(Arrays.asList("cobro cheques","depositos","extracciones","transferencias","creditos")) );

        this.bancos.add(bancoExternoDTO);

        bancoExternoDTO = new BancoExternoDTO();
        bancoExternoDTO.setBanco("ICBC");
        bancoExternoDTO.setGerente("Fabian Fantaguzzi");
        bancoExternoDTO.setSucursal("Caballito");
        bancoExternoDTO.setX(-30.4455546);
        bancoExternoDTO.setY(70.4668457);
        bancoExternoDTO.setServicios(new ArrayList<>(Arrays.asList("depositos","extracciones","transferencias","seguros")));

        this.bancos.add(bancoExternoDTO);
    };

    @Override
    public String consultarBancos(String nombre, String servicio) {

        Gson gson = new Gson();
        Set<BancoExternoDTO> bancosMatcheados = this.bancos.stream().filter(bancoExternoDTO -> bancoExternoDTO.verificaReferencia(nombre,servicio)).collect(Collectors.toSet());
        return gson.toJson(bancosMatcheados);
    }


}
