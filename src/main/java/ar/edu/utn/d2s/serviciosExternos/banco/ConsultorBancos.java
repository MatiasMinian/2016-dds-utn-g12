package ar.edu.utn.d2s.serviciosExternos.banco;

import java.util.List;

public interface ConsultorBancos {

	public String consultarBancos(String nombre, String servicio);

}
