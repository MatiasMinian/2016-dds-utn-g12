package ar.edu.utn.d2s.serviciosExternos;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.uqbar.geodds.Point;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ar.edu.utn.d2s.pdis.CGP;
import ar.edu.utn.d2s.pdis.Comuna;
import ar.edu.utn.d2s.pdis.Direccion;
import ar.edu.utn.d2s.pdis.Servicio;
import ar.edu.utn.d2s.pdis.SucursalBanco;
import ar.edu.utn.d2s.serviciosExternos.CGP.CentroDTO;
import ar.edu.utn.d2s.serviciosExternos.CGP.ServicioDTO;
import ar.edu.utn.d2s.serviciosExternos.banco.BancoExternoDTO;

public class InterpreteServiciosExternos {

	private List<SucursalBanco> listaSucursalBancos;
	private List<CGP> listaCGPs;
	
	public List<SucursalBanco> interpretarBancos(String bancosJson) {

		listaSucursalBancos = new ArrayList<>();
		
		final Gson gson = new Gson();
		final Type tipoListaBancos = new TypeToken<List<BancoExternoDTO>>(){}.getType();
		final List<BancoExternoDTO> bancosDTO = gson.fromJson(bancosJson, tipoListaBancos);
		bancosDTO.forEach(banco -> instanciarUnaSucursalBanco(banco));
		return listaSucursalBancos; 
	}
	
	/* ---- Getters & Setters ---- */
	
	public List<SucursalBanco> getListaSucursalBancos() {
		return listaSucursalBancos;
	}

	public void setListaSucursalBancos(List<SucursalBanco> listaSucursalBancos) {
		this.listaSucursalBancos = listaSucursalBancos;
	}

	public List<CGP> getListaCGPs() {
		return listaCGPs;
	}

	public void setListaCGPs(List<CGP> listaCGPs) {
		this.listaCGPs = listaCGPs;
	}
	
	/* --------------------------- */
	
	/* --------- Métodos --------- */

	public void instanciarUnaSucursalBanco(BancoExternoDTO bancoExterno) {
		

		Direccion direccionBanco = new Direccion("", "", "", 0, 0, "", 0, 0, "", "", "", "", new Point(bancoExterno.getX(), bancoExterno.getY()));
		SucursalBanco banco = new SucursalBanco(bancoExterno.getBanco(), "", direccionBanco, bancoExterno.getServicios().stream().map(s -> new Servicio(null, s)).collect(Collectors.toList()));
		listaSucursalBancos.add(banco);
	}
	
	public List<CGP> interpretarCGPs (List<CentroDTO> listaCentrosDTO){
		
		listaCGPs = new ArrayList<CGP>();
		
		listaCentrosDTO.forEach(centroDTO -> this.instanciarCGP(centroDTO));
		
		return listaCGPs;
		
	}
	
	public void instanciarCGP(CentroDTO unCentroDTO){
		
		CGP cgp = new CGP(unCentroDTO.getNombreDirector(),"",this.instanciarDireccion(unCentroDTO),this.instanciarComuna(unCentroDTO),this.interpretarServicios(unCentroDTO.getServicios()));
		listaCGPs.add(cgp);
		
	}
	
	public List<Servicio> interpretarServicios(List<ServicioDTO> serviciosDTO){
		
		List<Servicio> listaServicios = new ArrayList<Servicio>();
		
		for(ServicioDTO unServicioDTO : serviciosDTO){
			
			Servicio servicio = new Servicio(null,unServicioDTO.getNombreServicio());
			listaServicios.add(servicio);
			
		}
		
		return listaServicios;
		
	}
	
	public Comuna instanciarComuna(CentroDTO unCentro){
		
		Comuna comuna = new Comuna(unCentro.getNumeroComuna(),null);
		
		return comuna;
	}
	
    public Direccion instanciarDireccion(CentroDTO unCentro){
		
		Direccion direccion = new Direccion(unCentro.getDomicilio(),null,null,0,0,null,0,0,null,null,null,null, null);
		
		return direccion;
	}
    
    /* --------------------------- */	
}
