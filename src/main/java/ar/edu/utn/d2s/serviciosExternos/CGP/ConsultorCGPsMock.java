package ar.edu.utn.d2s.serviciosExternos.CGP;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ConsultorCGPsMock implements ConsultorCGPs{

	private List<CentroDTO> listaCentrosDTO = new ArrayList<>();

        //TODO AGREGAR LOGICA PARA QUE NO DEVUELVA SIEMPRE LO MISMO
	public ConsultorCGPsMock() {

        //Creo dos centrosDTO distintos para tests
        //Creo centroDTO1 comuna nro 5 con dos servicios(Infracciones y Audiencias) y sus rangos de horario de atenci�n
        List<RangoServicioDTO> listaRangosInfracciones = new ArrayList<RangoServicioDTO>();

        RangoServicioDTO rangoInfraccionesLunes = new RangoServicioDTO(1, 8, 0, 14, 0);
        RangoServicioDTO rangoInfraccionesMartes = new RangoServicioDTO(2, 8, 0, 14, 0);
        RangoServicioDTO rangoInfraccionesMiercoles = new RangoServicioDTO(3, 8, 0, 14, 0);
        RangoServicioDTO rangoInfraccionesJueves = new RangoServicioDTO(4, 8, 0, 14, 0);
        RangoServicioDTO rangoInfraccionesViernes = new RangoServicioDTO(5, 8, 0, 14, 0);

        listaRangosInfracciones.add(rangoInfraccionesLunes);
        listaRangosInfracciones.add(rangoInfraccionesMartes);
        listaRangosInfracciones.add(rangoInfraccionesMiercoles);
        listaRangosInfracciones.add(rangoInfraccionesJueves);
        listaRangosInfracciones.add(rangoInfraccionesViernes);

        ServicioDTO infracciones = new ServicioDTO("Infracciones", listaRangosInfracciones);

        List<RangoServicioDTO> listaRangosAudiencias = new ArrayList<RangoServicioDTO>();

        RangoServicioDTO rangoAudienciasMartes = new RangoServicioDTO(2, 9, 0, 13, 0);
        RangoServicioDTO rangoAudienciasJueves = new RangoServicioDTO(4, 9, 0, 13, 0);

        listaRangosAudiencias.add(rangoAudienciasMartes);
        listaRangosAudiencias.add(rangoAudienciasJueves);

        ServicioDTO audiencias = new ServicioDTO("Audiencias", listaRangosAudiencias);

        List<ServicioDTO> listaServiciosComuna5 = new ArrayList<ServicioDTO>();
        listaServiciosComuna5.add(infracciones);
        listaServiciosComuna5.add(audiencias);

        CentroDTO centroDTO1 = new CentroDTO(5,"Almagro, Boedo" , "Carlos", "Carlos Calvo 3307", "4931-6699 / 4932-5471", listaServiciosComuna5);


        //Creo centroDTO2 comuna nro 7 con dos servicios(Asesoramiento jur�dico y patrocinio, y Rentas) y sus rangos de horario de atenci�n

        List<RangoServicioDTO> listaRangosAJP = new ArrayList<RangoServicioDTO>();

        RangoServicioDTO rangoAJPJueves = new RangoServicioDTO(4, 9, 0, 13, 0);

        listaRangosAJP.add(rangoAJPJueves);

        ServicioDTO asesoramientoJuridicoYPatrocinio = new ServicioDTO("Asesoramiento Jur�dico y Patrocinio", listaRangosAJP);

        List<RangoServicioDTO> listaRangosRentas = new ArrayList<RangoServicioDTO>();

        RangoServicioDTO rangoRentasLunes = new RangoServicioDTO(1, 8, 30, 18, 0);
        RangoServicioDTO rangoRentasMartes = new RangoServicioDTO(2, 8, 30, 18, 0);
        RangoServicioDTO rangoRentasMiercoles = new RangoServicioDTO(3, 8, 30, 18, 0);
        RangoServicioDTO rangoRentasJueves = new RangoServicioDTO(4, 8, 30, 18, 0);
        RangoServicioDTO rangoRentasViernes = new RangoServicioDTO(5, 8, 30, 18, 0);

        listaRangosRentas.add(rangoRentasLunes);
        listaRangosRentas.add(rangoRentasMartes);
        listaRangosRentas.add(rangoRentasMiercoles);
        listaRangosRentas.add(rangoRentasJueves);
        listaRangosRentas.add(rangoRentasViernes);

        ServicioDTO rentas = new ServicioDTO("Rentas", listaRangosRentas);

        List<ServicioDTO> listaServiciosComuna7 = new ArrayList<ServicioDTO>();
        listaServiciosComuna7.add(rentas);
        listaServiciosComuna7.add(asesoramientoJuridicoYPatrocinio);

        CentroDTO centroDTO2 = new CentroDTO(7,"Flores, Parque Chacabuco" , "Fernando", "Av. Rivadavia 7202", "4637-2355/4145/6902/4613-1530", listaServiciosComuna7);

        listaCentrosDTO.add(centroDTO1);
        listaCentrosDTO.add(centroDTO2);

    }

	public List<CentroDTO> getListaCentrosDTO() {
		return listaCentrosDTO;
	}

	public void setListaCentrosDTO(List<CentroDTO> listaCentrosDTO) {
		this.listaCentrosDTO = listaCentrosDTO;
	}



	public List<CentroDTO> consultarCGPs(String dato){

		return listaCentrosDTO.stream().filter(centroDTO -> centroDTO.getZonasIncluidas().contains(dato) || centroDTO.getDomicilio().startsWith(dato)).collect(Collectors.toList());

	}

}
