package ar.edu.utn.d2s.serviciosExternos.CGP;

public class RangoServicioDTO {
	
	private int numeroDia;
	private int horarioDesde;
	private int minutoDesde;
	private int horarioHasta;
	private int minutoHasta;
	
	public RangoServicioDTO(int numeroDia, int horarioDesde, int minutoDesde, int horarioHasta, int minutoHasta) {
		this.numeroDia = numeroDia;
		this.horarioDesde = horarioDesde;
		this.minutoDesde = minutoDesde;
		this.horarioHasta = horarioHasta;
		this.minutoHasta = minutoHasta;
	}
	
	/* ---- Getters & Setters ---- */

	public int getNumeroDia() {
		return numeroDia;
	}

	public void setNumeroDia(int numeroDia) {
		this.numeroDia = numeroDia;
	}

	public int getHorarioDesde() {
		return horarioDesde;
	}

	public void setHorarioDesde(int horarioDesde) {
		this.horarioDesde = horarioDesde;
	}

	public int getMinutoDesde() {
		return minutoDesde;
	}

	public void setMinutoDesde(int minutoDesde) {
		this.minutoDesde = minutoDesde;
	}

	public int getHorarioHasta() {
		return horarioHasta;
	}

	public void setHorarioHasta(int horarioHasta) {
		this.horarioHasta = horarioHasta;
	}

	public int getMinutoHasta() {
		return minutoHasta;
	}

	public void setMinutoHasta(int minutoHasta) {
		this.minutoHasta = minutoHasta;
	}
	
	/* --------------------------- */
	
	/* --------- Métodos --------- */
	
	
	
	/* --------------------------- */
}
