package ar.edu.utn.d2s.serviciosExternos.CGP;

import java.util.List;

public class ServicioDTO {
	
	private String nombreServicio;
	private List<RangoServicioDTO> rangos;
	
	public ServicioDTO(String nombreServicio, List<RangoServicioDTO> rangos) {
		this.nombreServicio = nombreServicio;
		this.rangos = rangos;
	}
	
	/* ---- Getters & Setters ---- */

	public String getNombreServicio() {
		return nombreServicio;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public List<RangoServicioDTO> getRangos() {
		return rangos;
	}

	public void setRangos(List<RangoServicioDTO> rangos) {
		this.rangos = rangos;
	}
	
	/* --------------------------- */
	
	/* --------- Métodos --------- */
	
	
	
	/* --------------------------- */
}
