package ar.edu.utn.d2s.AplicacionWebUser;

public class User {
    String username;
    String salt;
    String hashedPassword;
    
	public User(String username, String salt, String hashedPassword) {
		super();
		this.username = username;
		this.salt = salt;
		this.hashedPassword = hashedPassword;
	}
	public String getUsername() {
		return username;
	}
	
	public String getSalt() {
		return salt;
	}
	public String getHashedPassword() {
		return hashedPassword;
	}
    
    
}
