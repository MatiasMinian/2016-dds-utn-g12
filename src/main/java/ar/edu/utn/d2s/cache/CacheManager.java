package ar.edu.utn.d2s.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import ar.edu.utn.d2s.pdis.PuntoDeInteres;

public class CacheManager {

	public static final int GENERAL_CACHE_SIZE = 1000;

	private static CacheManager instance = null;
	private Cache<String, Set<PuntoDeInteres>> cache = new Cache<>(GENERAL_CACHE_SIZE);

	public static CacheManager getInstance() {
		if (instance == null) {
			instance = new CacheManager();
		}
		return instance;
	}

	public Set<PuntoDeInteres> matchSearch(String search) {
		return cache.get(search);
	}

	public void addSearch(String search, Set<PuntoDeInteres> points) {
		cache.put(search, points);
	}
}
