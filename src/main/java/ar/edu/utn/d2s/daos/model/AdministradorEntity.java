package ar.edu.utn.d2s.daos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ADMINISTRADOR")
@PrimaryKeyJoinColumn(name="id")
public class AdministradorEntity extends UsuarioEntity {

    @Column(name = "limite_tiempo_busqueda", nullable = false)
    private long limiteTiempoBusqueda;

    public long getLimiteTiempoBusqueda() {
        return limiteTiempoBusqueda;
    }

    public void setLimiteTiempoBusqueda(long limiteTiempoBusqueda) {
        this.limiteTiempoBusqueda = limiteTiempoBusqueda;
    }
}
