package ar.edu.utn.d2s.daos;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

public interface GenericDao <T, PK extends Serializable> {

	public void save(T entity);

	public void update(T entity);

	public void delete(T entity);

	public T findById(Class<T> clazz, PK id);

	public T findOne(String query, Object[] params);

	public List<T> findMany(String query, Object[] params);

	public List<T> findAll(Class<T> clazz);
}