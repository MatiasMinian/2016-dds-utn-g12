package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.ServicioEntity;
import ar.edu.utn.d2s.pdis.Servicio;

public class ServicioMapper implements Mapper<ServicioEntity, Servicio> {

    HorarioDeAtencionMapper horarioDeAtencionMapper = new HorarioDeAtencionMapper();

    @Override
    public Servicio mapFromEntity(ServicioEntity entity) {
        Servicio servicio = new Servicio();
        servicio.setId(entity.getId());
        servicio.setNombre(entity.getNombre());
        servicio.setHorarioAtencion(horarioDeAtencionMapper.mapFromEntity(entity.getHorarioDeAtencion()));
        return servicio;
    }

    @Override
    public ServicioEntity mapToEntity(Servicio servicio) {
        ServicioEntity entity = new ServicioEntity();
        entity.setId(servicio.getId());
        entity.setNombre(servicio.getNombre());
        entity.setHorarioDeAtencion(horarioDeAtencionMapper.mapToEntity(servicio.getHorarioAtencion()));
        return entity;
    }
}
