package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.RubroEntity;
import ar.edu.utn.d2s.pdis.Rubro;

public class RubroMapper implements Mapper<RubroEntity, Rubro> {

    @Override
    public Rubro mapFromEntity(RubroEntity entity) {
        return new Rubro(entity.getId(), entity.getNombre(), entity.getRangoCercania());
    }

    @Override
    public RubroEntity mapToEntity(Rubro rubro) {
        return new RubroEntity(rubro.getId(), rubro.getNombre(), rubro.getRangoDeCercania());
    }
}
