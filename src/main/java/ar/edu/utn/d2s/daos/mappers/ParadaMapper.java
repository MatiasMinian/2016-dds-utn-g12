package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.ParadaEntity;
import ar.edu.utn.d2s.pdis.Parada;

public class ParadaMapper implements Mapper<ParadaEntity, Parada> {

    DireccionMapper direccionMapper = new DireccionMapper();

    @Override
    public Parada mapFromEntity(ParadaEntity entity) {
        Parada parada = new Parada();
        parada.setId(entity.getId());
        parada.setDireccion(direccionMapper.mapFromEntity(entity.getDireccion()));
        return parada;
    }

    @Override
    public ParadaEntity mapToEntity(Parada parada) {
        ParadaEntity entity = new ParadaEntity();
        entity.setId(parada.getId());
        entity.setDireccion(direccionMapper.mapToEntity(parada.getDireccion()));
        return entity;
    }
}
