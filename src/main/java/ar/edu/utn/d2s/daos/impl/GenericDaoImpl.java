package ar.edu.utn.d2s.daos.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import ar.edu.utn.d2s.daos.GenericDao;

public abstract class GenericDaoImpl<T, PK extends Serializable> implements GenericDao<T, PK> {

    //@PersistenceContext(unitName = "HibernatePersistence", type = PersistenceContextType.EXTENDED)
    protected EntityManager entityManager = null;

    //@PersistenceUnit(unitName = "HibernatePersistence")

    protected static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("HibernatePersistence");

    protected void createEntityManager() {
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
    }

    protected void destroyEntityManager() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void save(T entity) {
        createEntityManager();
        entityManager.persist(entity);
        destroyEntityManager();
    }

    @Override
    public void update(T entity) {
        createEntityManager();
        entityManager.merge(entity);
        destroyEntityManager();
    }

    @Override
    public void delete(T entity) {
        createEntityManager();
        entityManager.remove(entity);
        destroyEntityManager();
    }

    @Override
    public T findById(Class<T> clazz, PK id) {
        createEntityManager();
        T entity = entityManager.find(clazz, id);
        destroyEntityManager();
        return entity;
    }

    @Override
    public T findOne(String stringQuery, Object[] params) {
        createEntityManager();
        Query query = entityManager.createQuery(stringQuery);
        for (int i = 1; i <= params.length; i++) {
            query.setParameter(i, params[i-1]);
        }
        T entity = null;
        try {
            entity = (T) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        destroyEntityManager();
        return entity;
    }

    @Override
    public List<T> findMany(String stringQuery, Object[] params) {
        createEntityManager();
        Query query = entityManager.createQuery(stringQuery);
        for (int i = 1; i <= params.length; i++) {
            query.setParameter(i, params[i-1]);
        }
        List<T> entities = (List<T>) query.getResultList();
        destroyEntityManager();
        return entities;
    }

    @Override
    public List<T> findAll(Class clazz) {
        createEntityManager();
        Query query = entityManager.createQuery("SELECT entity FROM " + clazz.getSimpleName() + " entity WHERE entity.id is not null");
        List<T> entities = (List<T>) query.getResultList();
        destroyEntityManager();
        return entities;
    }
}
