package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PALABRA_CLAVE")
public class PalabraClaveEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "palabra", nullable = false, unique = true)
    private String palabra;

    @ManyToMany(mappedBy = "palabrasClave")
    private List<LocalComercialEntity> localComercialEntities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public List<LocalComercialEntity> getLocalComercialEntities() {
        return localComercialEntities;
    }

    public void setLocalComercialEntities(List<LocalComercialEntity> localComercialEntities) {
        this.localComercialEntities = localComercialEntities;
    }

    public void addLocalComercial (LocalComercialEntity localComercialEntity){
        localComercialEntities.add(localComercialEntity);
    }
}
