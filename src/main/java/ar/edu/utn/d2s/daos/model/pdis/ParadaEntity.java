package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;

@Entity
@Table(name = "PARADA")
public class ParadaEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "colectivo_id", nullable = false)
    private ColectivoEntity colectivo;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "direccion_id"/*, nullable = false*/)
    private DireccionEntity direccion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ColectivoEntity getColectivo() {
        return colectivo;
    }

    public void setColectivo(ColectivoEntity colectivo) {
        this.colectivo = colectivo;
    }

    public DireccionEntity getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionEntity direccion) {
        this.direccion = direccion;
    }
}
