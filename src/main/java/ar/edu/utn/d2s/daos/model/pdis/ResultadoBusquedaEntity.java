package ar.edu.utn.d2s.daos.model.pdis;

import ar.edu.utn.d2s.daos.model.TerminalEntity;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "RESULTADO_BUSQUEDA")
public class ResultadoBusquedaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "texto", nullable = false)
    private String texto;

    @Column(name = "cantidad_resultados", nullable = false)
    private int cantidadDeResultados;

    @Column(name = "tiempo_busqueda",nullable = false)
    private long tiempoBusqueda;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha",nullable = false)
    private Calendar fecha;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "terminal_id ", nullable = false)
    private TerminalEntity terminal;

    public ResultadoBusquedaEntity() {};

    public ResultadoBusquedaEntity(String texto, int cantidadDeResultados, long tiempoBusqueda, Calendar fecha) {
        this.id = id;
        this.texto = texto;
        this.cantidadDeResultados = cantidadDeResultados;
        this.tiempoBusqueda = tiempoBusqueda;
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getCantidadDeResultados() {
        return cantidadDeResultados;
    }

    public void setCantidadDeResultados(int cantidadDeResultados) {
        this.cantidadDeResultados = cantidadDeResultados;
    }

    public long getTiempoBusqueda() {
        return tiempoBusqueda;
    }

    public void setTiempoBusqueda(long tiempoBusqueda) {
        this.tiempoBusqueda = tiempoBusqueda;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public TerminalEntity getTerminal() {
        return terminal;
    }

    public void setTerminal(TerminalEntity terminal) {
        this.terminal = terminal;
    }
}
