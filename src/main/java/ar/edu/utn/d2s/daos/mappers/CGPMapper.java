package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.CGPEntity;
import ar.edu.utn.d2s.pdis.CGP;
import ar.edu.utn.d2s.pdis.Servicio;
import org.uqbar.geodds.Point;

import java.util.stream.Collectors;

public class CGPMapper implements Mapper<CGPEntity, CGP> {

    DireccionMapper direccionMapper = new DireccionMapper();
    ComunaMapper comunaMapper = new ComunaMapper();
    ServicioMapper servicioMapper = new ServicioMapper();

    @Override
    public CGP mapFromEntity(CGPEntity entity) {
        CGP cgp = new CGP();
        cgp.setId(entity.getId());
        cgp.setNombre(entity.getNombre());
        cgp.setIcono(entity.getIcono());
        cgp.setDireccion(direccionMapper.mapFromEntity(entity.getDireccion()));
        cgp.setComuna(comunaMapper.mapFromEntity(entity.getComuna()));
        cgp.setServicios(entity.getServicios().stream().map(servicioEntity -> servicioMapper.mapFromEntity(servicioEntity))
                                                        .collect(Collectors.toList()));
        return cgp;
    }

    @Override
    public CGPEntity mapToEntity(CGP cgp) {
        CGPEntity entity = new CGPEntity();
        entity.setId(cgp.getId());
        entity.setNombre(cgp.getNombre());
        entity.setIcono(cgp.getIcono());
        entity.setRangoCercania(cgp.getRangoDeCercania());
        entity.setDireccion(direccionMapper.mapToEntity(cgp.getDireccion()));
        entity.setComuna(comunaMapper.mapToEntity(cgp.getComuna()));
        //entity.setServicios(cgp.getServicios()); // TODO
        return entity;
    }
}
