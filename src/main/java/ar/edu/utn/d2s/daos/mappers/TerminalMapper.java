package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.usuarios.Terminal;
import java.util.stream.Collectors;

public class TerminalMapper implements Mapper<TerminalEntity,Terminal> {

    private AccionMapper accionMapper = new AccionMapper();
    private ResultadoBusquedaMapper resultadoBusquedaMapper = new ResultadoBusquedaMapper();

    @Override
    public Terminal mapFromEntity(TerminalEntity entity) {
        Terminal terminal = new Terminal(entity.getMail());
        terminal.setId(entity.getId());
        terminal.setPassword(entity.getPassword());
        terminal.setUsername(entity.getUsername());
        terminal.setAcciones(entity.getAcciones().stream().map(accionEntity -> accionMapper.mapFromEntity(accionEntity)).collect(Collectors.toList()));
        terminal.setAlmacenamientoHabilitado(entity.isGuardarResultadosBusqueda());
        terminal.setNombre(entity.getNombre());
        terminal.setResultadoBusquedas(entity.getResultadoBusquedas().stream().map(resultadoBusquedaEntity -> resultadoBusquedaMapper.mapFromEntity(resultadoBusquedaEntity)).collect(Collectors.toList()));
        terminal.setSalt(entity.getSalt());
        return terminal;
    }

    @Override
    public TerminalEntity mapToEntity(Terminal model) {
        TerminalEntity entity = new TerminalEntity();

        entity.setNombre(model.getNombre());
        entity.setId(model.getId());
        model.getAcciones().forEach(accion -> entity.agregarAccion(accionMapper.mapToEntity(accion)));
        entity.setGuardarResultadosBusqueda(model.isAlmacenamientoHabilitado());
        entity.setResultadoBusquedas(model.getResultadoBusquedas().stream().map(resultadoBusqueda -> resultadoBusquedaMapper.mapToEntity(resultadoBusqueda)).collect(Collectors.toList()));
        entity.getResultadoBusquedas().forEach(resultadoBusquedaEntity -> resultadoBusquedaEntity.setTerminal(entity));
        entity.setMail(model.getMail());
        entity.setPassword(model.getPassword());
        entity.setUsername(model.getUsername());
        entity.setSalt(model.getSalt());

        return entity;
    }
}
