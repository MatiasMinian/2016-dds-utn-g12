package ar.edu.utn.d2s.daos.impl;

import javax.persistence.Query;

import ar.edu.utn.d2s.daos.mappers.TerminalMapper;
import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.usuarios.Terminal;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TerminalDAO extends GenericDaoImpl<TerminalEntity, Long> {

	private TerminalMapper terminalMapper = new TerminalMapper();

	public Terminal findByName(String username) {
		
		//createEntityManager();
		//Query query = entityManager.createQuery("SELECT term FROM TerminalEntity term WHERE term.username = :username").setParameter("username", username);
        String query = "SELECT term FROM TerminalEntity term WHERE term.username = ?1";

        Object[] params = {username};
        TerminalEntity terminalEntity = findOne(query, params);
        if (terminalEntity == null) {
            return null;
        }

		return terminalMapper.mapFromEntity(terminalEntity);
		
		
	}

   public List<Terminal> findAll(){
       List<Terminal> terminales = findAll(TerminalEntity.class).stream().map(terminalEntity -> terminalMapper.mapFromEntity(terminalEntity)).collect(Collectors.toList());
       return terminales;
   }

   public void save (Terminal terminal){
       save(terminalMapper.mapToEntity(terminal));
   }

   public void update(Terminal terminal) {
       update(terminalMapper.mapToEntity(terminal));
   }
}
