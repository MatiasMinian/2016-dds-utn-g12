package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.pdis.ResultadoBusqueda;
import ar.edu.utn.d2s.daos.model.pdis.ResultadoBusquedaEntity;

public class ResultadoBusquedaMapper implements Mapper<ResultadoBusquedaEntity, ResultadoBusqueda> {

    @Override
    public ResultadoBusqueda mapFromEntity(ResultadoBusquedaEntity entity) {
        ResultadoBusqueda resultadoBusqueda = new ResultadoBusqueda();
        resultadoBusqueda.setId(entity.getId());
        resultadoBusqueda.setTexto(entity.getTexto());
        resultadoBusqueda.setCantidadDeResultados(entity.getCantidadDeResultados());
        resultadoBusqueda.setTiempoBusqueda(entity.getTiempoBusqueda());
        resultadoBusqueda.setFecha(entity.getFecha());
        return resultadoBusqueda;
    }

    @Override
    public ResultadoBusquedaEntity mapToEntity(ResultadoBusqueda resultadoBusqueda) {
        ResultadoBusquedaEntity entity = new ResultadoBusquedaEntity();
        entity.setId(resultadoBusqueda.getId());
        entity.setTexto(resultadoBusqueda.getTexto());
        entity.setCantidadDeResultados(resultadoBusqueda.getCantidadDeResultados());
        entity.setTiempoBusqueda(resultadoBusqueda.getTiempoBusqueda());
        entity.setFecha(resultadoBusqueda.getFecha());
        return entity;
    }
}
