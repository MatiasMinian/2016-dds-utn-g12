package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.mappers.UsuarioMapper;
import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.daos.model.UsuarioEntity;
import ar.edu.utn.d2s.usuarios.Usuario;

import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

public class UsuarioDao extends GenericDaoImpl<UsuarioEntity, Long> {

    private UsuarioMapper usuarioMapper = new UsuarioMapper();

    public Usuario getUsuarioByName (String username){
        //createEntityManager();

        //Query query = entityManager.createQuery("SELECT user FROM UsuarioEntity user WHERE username = :username").setParameter("username", username);
        String query = "SELECT user FROM UsuarioEntity user WHERE username = ?1";
        Object[] params = {username};

        UsuarioEntity usuarioEntity = findOne(query, params);
        if (usuarioEntity == null) {
            return null;
        }



        return usuarioMapper.mapFromEntity(usuarioEntity);

       //UsuarioEntity usuarioEntity = findAll(UsuarioEntity.class).stream().filter(usuario -> usuario.getUsername().equals(name)).findFirst().orElse(null);
        //return usuarioMapper.mapFromEntity(usuarioEntity);
    }

    public Iterable<String> getNombresUsuarios (){
        return findAll(UsuarioEntity.class).stream().map(usuarioEntity -> usuarioEntity.getUsername()).collect(Collectors.toList());
    }

}
