package ar.edu.utn.d2s.daos.model.pdis;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "HORARIO_DE_ATENCION")
public class HorarioDeAtencionEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "horarioDeAtencion")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<HorarioEntity> horarios = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<HorarioEntity> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<HorarioEntity> horarios) {
        this.horarios = horarios;
    }

    public void agregarHorario(HorarioEntity horario) {
        horario.setHorarioDeAtencion(this);
        horarios.add(horario);
    }
}
