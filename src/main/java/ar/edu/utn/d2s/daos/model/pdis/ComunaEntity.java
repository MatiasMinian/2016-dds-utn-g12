package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "COMUNA")
public class ComunaEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "numero", nullable = false)
    private int numero;

    @OneToOne(mappedBy = "comuna")
    private CGPEntity cgp;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "comuna", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PointEntity> puntos = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public CGPEntity getCgp() {
        return cgp;
    }

    public void setCgp(CGPEntity cgp) {
        this.cgp = cgp;
    }

    public List<PointEntity> getPuntos() {
        return puntos;
    }

    public void setPuntos(List<PointEntity> puntos) {
        this.puntos = puntos;
    }

    public void agregarPunto(PointEntity punto) {
        punto.setComuna(this);
        puntos.add(punto);
    }
}
