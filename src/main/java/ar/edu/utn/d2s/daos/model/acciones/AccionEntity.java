package ar.edu.utn.d2s.daos.model.acciones;

import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.usuarios.Terminal;

import javax.persistence.*;

@Entity
@Table(name = "ACCION")
public class AccionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_accion", nullable =false)
    private Integer id_Accion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "terminal_id ", nullable = false)
    private TerminalEntity terminal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TerminalEntity getTerminal() {
        return terminal;
    }

    public void setTerminal(TerminalEntity terminal) {
        this.terminal = terminal;
    }

    public Integer getId_Accion() {
        return id_Accion;
    }

    public void setId_Accion(Integer id_Accion) {
        this.id_Accion = id_Accion;
    }
}
