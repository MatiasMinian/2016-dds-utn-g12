package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.UsuarioEntity;
import ar.edu.utn.d2s.usuarios.Usuario;

public class UsuarioMapper implements Mapper<UsuarioEntity,Usuario> {

    @Override
    public Usuario mapFromEntity(UsuarioEntity entity) {

        Usuario usuario = new Usuario(entity.getMail());
        usuario.setSalt(entity.getSalt());
        usuario.setId(entity.getId());
        usuario.setUsername(entity.getUsername());
        usuario.setPassword(entity.getPassword());

        return usuario;
    }

    @Override
    public UsuarioEntity mapToEntity(Usuario model) {

        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setMail(model.getMail());
        usuario.setPassword(model.getPassword());
        usuario.setSalt(model.getSalt());
        usuario.setUsername(model.getUsername());
        usuario.setId(model.getId());

        return usuario;
    }
}
