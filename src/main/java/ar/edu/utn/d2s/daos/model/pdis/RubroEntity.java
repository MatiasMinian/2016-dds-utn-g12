package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "RUBRO")
public class RubroEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "rango_cercania", nullable = false)
    private double rangoCercania;

    public RubroEntity() {};

    public RubroEntity(Long id, String nombre, double rangoCercania) {
        this.id = id;
        this.nombre = nombre;
        this.rangoCercania = rangoCercania;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getRangoCercania() {
        return rangoCercania;
    }

    public void setRangoCercania(double rangoCercania) {
        this.rangoCercania = rangoCercania;
    }
}
