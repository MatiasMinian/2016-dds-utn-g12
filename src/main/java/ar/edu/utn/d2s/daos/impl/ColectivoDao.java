package ar.edu.utn.d2s.daos.impl;

import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.daos.mappers.ColectivoMapper;
import ar.edu.utn.d2s.daos.model.pdis.ColectivoEntity;
import ar.edu.utn.d2s.pdis.Colectivo;

public class ColectivoDao extends GenericDaoImpl<ColectivoEntity, Long> {
	private ColectivoMapper colectivoMapper = new ColectivoMapper();

	public List<Colectivo> findAll() {
		return super.findAll(ColectivoEntity.class).stream().map(colectivoEntity -> colectivoMapper.mapFromEntity(colectivoEntity) ).collect(Collectors.toList());
	}
}
