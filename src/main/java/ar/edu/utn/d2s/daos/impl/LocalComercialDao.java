package ar.edu.utn.d2s.daos.impl;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.daos.mappers.LocalComercialMapper;
import ar.edu.utn.d2s.daos.model.pdis.LocalComercialEntity;
import ar.edu.utn.d2s.pdis.LocalComercial;

public class LocalComercialDao extends GenericDaoImpl<LocalComercialEntity, Long> {

	private LocalComercialMapper localComercialMapper = new LocalComercialMapper();

	public List<LocalComercial> findAll() {
		return super.findAll(LocalComercialEntity.class).stream().map(localComercialEntity -> localComercialMapper.mapFromEntity(localComercialEntity)).collect(Collectors.toList());
	}
}
