package ar.edu.utn.d2s.daos.model.pdis;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "LOCAL_COMERCIAL")
@PrimaryKeyJoinColumn(name="id")
public class LocalComercialEntity extends PuntoDeInteresEntity {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "horario_atencion_id", nullable = false)
    private HorarioDeAtencionEntity horarioDeAtencion;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rubro_id", nullable = false)
    private RubroEntity rubro;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "LOCAL_PALABRA", joinColumns = @JoinColumn(name = "local_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "palabra_clave_id", nullable = false))
    @Fetch(value = FetchMode.SUBSELECT)
    private Set<PalabraClaveEntity> palabrasClave = new HashSet<>();

    public HorarioDeAtencionEntity getHorarioDeAtencion() {
        return horarioDeAtencion;
    }

    public void setHorarioDeAtencion(HorarioDeAtencionEntity horarioDeAtencion) {
        this.horarioDeAtencion = horarioDeAtencion;
    }

    public RubroEntity getRubro() {
        return rubro;
    }

    public void setRubro(RubroEntity rubro) {
        this.rubro = rubro;
    }

    public Set<PalabraClaveEntity> getPalabrasClave() {
        return palabrasClave;
    }

    public void setPalabrasClave(Set<PalabraClaveEntity> palabrasClave) {
        this.palabrasClave = palabrasClave;
    }

    public void agregarPalabraClave(PalabraClaveEntity palabraClave) {
        palabraClave.addLocalComercial(this);
        palabrasClave.add(palabraClave);
    }
}
