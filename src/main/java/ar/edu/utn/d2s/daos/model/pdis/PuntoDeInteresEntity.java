package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;

@Entity
@Table(name = "PUNTO_DE_INTERES")
@Inheritance(strategy = InheritanceType.JOINED)
public class PuntoDeInteresEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre", nullable = false)
	private String nombre;
	@Column(name = "icono")
	private String icono;
	@Column(name = "rango_cercania", nullable = false)
	private Double rangoCercania;
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "direccion_id",nullable = false)
	private DireccionEntity direccion;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public Double getRangoCercania() {
		return rangoCercania;
	}

	public void setRangoCercania(Double rangoCercania) {
		this.rangoCercania = rangoCercania;
	}

	public DireccionEntity getDireccion() {
		return direccion;
	}

	public void setDireccion(DireccionEntity direccion) {
		this.direccion = direccion;
	}
}
