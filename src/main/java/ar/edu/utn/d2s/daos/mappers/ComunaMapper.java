package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.ComunaEntity;
import ar.edu.utn.d2s.daos.model.pdis.PointEntity;
import ar.edu.utn.d2s.pdis.Comuna;
import ar.edu.utn.d2s.pdis.Poligono;
import org.uqbar.geodds.Point;
import org.uqbar.geodds.Polygon;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ComunaMapper implements Mapper<ComunaEntity, Comuna> {

    private PointMapper pointMapper = new PointMapper();

    @Override
    public Comuna mapFromEntity(ComunaEntity entity) {
        Comuna comuna = new Comuna();
        comuna.setId(entity.getId());
        comuna.setNumero(entity.getNumero());
        List<Point> points = entity.getPuntos().stream().map(pointEntity -> pointMapper.mapFromEntity(pointEntity)).collect(Collectors.toList());
        comuna.setArea(new Poligono(points));

        return comuna;
    }

    @Override
    public ComunaEntity mapToEntity(Comuna comuna) {
        ComunaEntity entity = new ComunaEntity();
        entity.setId(comuna.getId());
        entity.setNumero(comuna.getNumero());

        List<PointEntity> points = new ArrayList<>();
        comuna.getArea().getSurface().forEach(point -> points.add(pointMapper.mapToEntity(point)));
        entity.setPuntos(points);

        return entity;
    }
}
