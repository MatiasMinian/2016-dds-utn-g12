package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.DireccionEntity;
import ar.edu.utn.d2s.pdis.Direccion;
import org.uqbar.geodds.Point;

public class DireccionMapper implements Mapper<DireccionEntity, Direccion> {

    @Override
    public Direccion mapFromEntity(DireccionEntity entity) {
        Direccion direccion = new Direccion();
        direccion.setId(entity.getId());
        direccion.setCallePrincipal(entity.getCallePrincipal());
        direccion.setEntreCalle1(entity.getEntreCalle1());
        direccion.setEntreCalle2(entity.getEntreCalle2());
        direccion.setNumero(entity.getNumero());
        direccion.setPiso(entity.getPiso());
        direccion.setDepartamento(entity.getDepartamento());
        direccion.setUnidad(entity.getUnidad());
        direccion.setCodigoPostal(entity.getCodigoPostal());
        direccion.setLocalidad(entity.getLocalidad());
        direccion.setBarrio(entity.getBarrio());
        direccion.setProvincia(entity.getProvincia());
        direccion.setPais(entity.getPais());
        direccion.setPunto(new Point(entity.getCoordX(), entity.getCoordY()));
        return direccion;
    }

    @Override
    public DireccionEntity mapToEntity(Direccion direccion) {
        DireccionEntity entity = new DireccionEntity();
        entity.setId(direccion.getId());
        entity.setCallePrincipal(direccion.getCallePrincipal());
        entity.setEntreCalle1(direccion.getEntreCalle1());
        entity.setEntreCalle2(direccion.getEntreCalle2());
        entity.setNumero(direccion.getNumero());
        entity.setPiso(direccion.getPiso());
        entity.setDepartamento(direccion.getDepartamento());
        entity.setUnidad(direccion.getUnidad());
        entity.setCodigoPostal(direccion.getCodigoPostal());
        entity.setLocalidad(direccion.getLocalidad());
        entity.setBarrio(direccion.getBarrio());
        entity.setProvincia(direccion.getProvincia());
        entity.setPais(direccion.getPais());
        entity.setCoordX(direccion.getPunto().latitude());
        entity.setCoordY(direccion.getPunto().longitude());
        return entity;
    }
}
