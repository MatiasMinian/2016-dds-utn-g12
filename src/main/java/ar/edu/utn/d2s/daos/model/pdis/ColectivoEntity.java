package ar.edu.utn.d2s.daos.model.pdis;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "COLECTIVO")
@PrimaryKeyJoinColumn(name="id")
public class ColectivoEntity extends PuntoDeInteresEntity {

    @Column(name = "nro_linea", nullable = false)
    private int nroLinea;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "colectivo")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ParadaEntity> paradas = new ArrayList<>();

    public int getNroLinea() {
        return nroLinea;
    }

    public void setNroLinea(int nroLinea) {
        this.nroLinea = nroLinea;
    }

    public List<ParadaEntity> getParadas() {
        return paradas;
    }

    public void setParadas(List<ParadaEntity> paradas) {
        this.paradas = paradas;
    }

    public void agregarParada(ParadaEntity parada) {
        parada.setColectivo(this);
        paradas.add(parada);
    }
}
