package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.AdministradorEntity;
import ar.edu.utn.d2s.usuarios.Administrador;

import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;

public class AdministradorMapper implements Mapper<AdministradorEntity,Administrador> {

    @Override
    public Administrador mapFromEntity(AdministradorEntity entity) {
        Administrador administrador = new Administrador(entity.getMail());
        administrador.setPassword(entity.getPassword());
        administrador.setUsername(entity.getUsername());
        administrador.setId(entity.getId());
        administrador.setTiempoLimiteBusqueda(entity.getLimiteTiempoBusqueda());
        administrador.setSalt(entity.getSalt());
        return administrador;
    }

    @Override
    public AdministradorEntity mapToEntity(Administrador model) {
        AdministradorEntity administrador = new AdministradorEntity();
        administrador.setPassword(model.getPassword());
        administrador.setUsername(model.getUsername());
        administrador.setId(model.getId());
        administrador.setLimiteTiempoBusqueda(model.getTiempoLimiteBusqueda());
        administrador.setMail(model.getMail());
        administrador.setSalt(model.getSalt());
        return administrador;
    }
}
