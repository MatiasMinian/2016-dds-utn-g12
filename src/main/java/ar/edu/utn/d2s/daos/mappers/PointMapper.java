package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.PointEntity;
import org.uqbar.geodds.Point;

public class PointMapper implements Mapper<PointEntity, Point> {

    @Override
    public Point mapFromEntity(PointEntity entity) {
        return new Point(entity.getX(), entity.getY());
    }

    @Override
    public PointEntity mapToEntity(Point point) {
        PointEntity entity = new PointEntity();
        entity.setX(point.latitude());
        entity.setY(point.longitude());
        return entity;
    }
}
