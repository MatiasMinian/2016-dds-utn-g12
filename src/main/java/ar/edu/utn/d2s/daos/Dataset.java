package ar.edu.utn.d2s.daos;

import ar.edu.utn.d2s.daos.impl.CGPDao;
import ar.edu.utn.d2s.daos.impl.ColectivoDao;
import ar.edu.utn.d2s.daos.impl.LocalComercialDao;
import ar.edu.utn.d2s.daos.impl.SucursalBancoDao;
import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.daos.mappers.HorarioDeAtencionMapper;
import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.daos.model.pdis.CGPEntity;
import ar.edu.utn.d2s.daos.model.pdis.ColectivoEntity;
import ar.edu.utn.d2s.daos.model.pdis.ComunaEntity;
import ar.edu.utn.d2s.daos.model.pdis.DireccionEntity;
import ar.edu.utn.d2s.daos.model.pdis.HorarioDeAtencionEntity;
import ar.edu.utn.d2s.daos.model.pdis.HorarioEntity;
import ar.edu.utn.d2s.daos.model.pdis.LocalComercialEntity;
import ar.edu.utn.d2s.daos.model.pdis.PalabraClaveEntity;
import ar.edu.utn.d2s.daos.model.pdis.ParadaEntity;
import ar.edu.utn.d2s.daos.model.pdis.PointEntity;
import ar.edu.utn.d2s.daos.model.pdis.RubroEntity;
import ar.edu.utn.d2s.daos.model.pdis.ServicioEntity;
import ar.edu.utn.d2s.daos.model.pdis.SucursalBancoEntity;
import ar.edu.utn.d2s.pdis.SucursalBanco;

import java.util.ArrayList;
import java.util.Calendar;

public class Dataset {

    TerminalDAO terminalDAO = new TerminalDAO();
    CGPDao cgpDAO = new CGPDao();
    LocalComercialDao localDAO = new LocalComercialDao();
    ColectivoDao colectivoDAO = new ColectivoDao();
    SucursalBancoDao bancoDAO = new SucursalBancoDao();
    CGPEntity cgpBallester;
    CGPEntity cgpRecoleta;
    CGPEntity cgpCaballito;
    ColectivoEntity colectivo114;
    ColectivoEntity colectivo19;
    ColectivoEntity colectivo152;
    LocalComercialEntity localKiosko;
    LocalComercialEntity burgerKing;
    LocalComercialEntity unicenter;
    SucursalBancoEntity bancoHSBC;
    SucursalBancoEntity bancoFrances;
    SucursalBancoEntity bancoItau;
    
    
    public void crearDataset() {

        /*CREACION DE TERMINALES*/

        TerminalEntity terminalIvan = new TerminalEntity("icernigoj", "$2a$10$e0MYzXyjpJS7Pd0RVvHwHe1HlCS4bZJ18JuywdEMLT83E1KDmUhCy", "icernigoj@gmail.com", "$2a$10$e0MYzXyjpJS7Pd0RVvHwHe",
                false, new ArrayList<>(), new ArrayList<>(), "icernigoj");

        TerminalEntity terminalFederico = new TerminalEntity("federico", "$2a$10$E3DgchtVry3qlYlzJCsyxeSK0fftK4v0ynetVCuDdxGVl1obL.ln2", "federico@gmail.com", "$2a$10$E3DgchtVry3qlYlzJCsyxe",
                false, new ArrayList<>(), new ArrayList<>(), "federico");

        TerminalEntity terminalKevin = new TerminalEntity("kevin", "$2a$10$h.dl5J86rGH7I8bD9bZeZeci0pDt0.VwFTGujlnEaZXPf/q7vM5wO", "kevin@gmail.com", "$2a$10$h.dl5J86rGH7I8bD9bZeZe",
                true, new ArrayList<>(), new ArrayList<>(), "kevin");

        terminalDAO.save(terminalIvan);
        terminalDAO.save(terminalFederico);
        terminalDAO.save(terminalKevin);

        

        /*CREACION DE CGPs*/
        
        PointEntity punto1CgpBallester = new PointEntity();
        punto1CgpBallester.setX(12.4);
        punto1CgpBallester.setY(149.3);

        PointEntity punto2CgpBallester = new PointEntity();
        punto2CgpBallester.setX(-123.8);
        punto2CgpBallester.setY(-450);

        ComunaEntity comunaBallester = new ComunaEntity();
        comunaBallester.setNumero(4);
        comunaBallester.agregarPunto(punto1CgpBallester);
        comunaBallester.agregarPunto(punto2CgpBallester);

        DireccionEntity direccionCgpBallester = new DireccionEntity();
        direccionCgpBallester.setNumero(1234);
        direccionCgpBallester.setBarrio("Ballester");
        direccionCgpBallester.setCallePrincipal("Laprida");
        direccionCgpBallester.setCodigoPostal(1650);
        direccionCgpBallester.setCoordX(874);
        direccionCgpBallester.setCoordY(340.1);
        direccionCgpBallester.setDepartamento("A");
        direccionCgpBallester.setEntreCalle1("Guemes");
        direccionCgpBallester.setEntreCalle2("Juarez");
        direccionCgpBallester.setLocalidad("San Martin");
        direccionCgpBallester.setPais("Argentina");
        direccionCgpBallester.setPiso(5);
        direccionCgpBallester.setProvincia("Buenos Aires");
        direccionCgpBallester.setUnidad(8);


        Calendar horaComienzoLunesCgpBallester = Calendar.getInstance();
        horaComienzoLunesCgpBallester.set(Calendar.HOUR_OF_DAY, 10);
        horaComienzoLunesCgpBallester.set(Calendar.MINUTE, 0);
        horaComienzoLunesCgpBallester.set(Calendar.SECOND, 0);

        Calendar horaFinLunesCgpBallester = Calendar.getInstance();
        horaFinLunesCgpBallester.set(Calendar.HOUR_OF_DAY, 18);
        horaFinLunesCgpBallester.set(Calendar.MINUTE, 0);
        horaFinLunesCgpBallester.set(Calendar.SECOND, 0);

        HorarioEntity horarioLunesCgpBallester = new HorarioEntity();
        horarioLunesCgpBallester.setDia(2);
        horarioLunesCgpBallester.setHoraComienzo(horaComienzoLunesCgpBallester);
        horarioLunesCgpBallester.setHoraFin(horaFinLunesCgpBallester);


        Calendar horaComienzoMiercolesCgpBallester = Calendar.getInstance();
        horaComienzoMiercolesCgpBallester.set(Calendar.HOUR_OF_DAY, 12);
        horaComienzoMiercolesCgpBallester.set(Calendar.MINUTE, 0);
        horaComienzoMiercolesCgpBallester.set(Calendar.SECOND, 0);

        Calendar horaFinMiercolesCgpBallester = Calendar.getInstance();
        horaFinMiercolesCgpBallester.set(Calendar.HOUR_OF_DAY, 20);
        horaFinMiercolesCgpBallester.set(Calendar.MINUTE, 0);
        horaFinMiercolesCgpBallester.set(Calendar.SECOND, 0);

        HorarioEntity horarioMiercolesCgpBallester = new HorarioEntity();
        horarioMiercolesCgpBallester.setDia(4);
        horarioMiercolesCgpBallester.setHoraComienzo(horaComienzoMiercolesCgpBallester);
        horarioMiercolesCgpBallester.setHoraFin(horaFinMiercolesCgpBallester);

        HorarioDeAtencionEntity multasHorarioAtencionCgpBallester = new HorarioDeAtencionEntity();
        multasHorarioAtencionCgpBallester.agregarHorario(horarioLunesCgpBallester);
        multasHorarioAtencionCgpBallester.agregarHorario(horarioMiercolesCgpBallester);

        ServicioEntity multasCgpBallester = new ServicioEntity();
        multasCgpBallester.setNombre("Multas");
        multasCgpBallester.setHorarioDeAtencion(multasHorarioAtencionCgpBallester);

        cgpBallester = new CGPEntity();
        cgpBallester.setNombre("Centro Ballester");
        cgpBallester.setIcono("/cgp_icono.jpg");
        cgpBallester.setRangoCercania(0.5);  // Dejarlo siempre igual
        cgpBallester.setComuna(comunaBallester);
        cgpBallester.setDireccion(direccionCgpBallester);
        cgpBallester.agregarServicio(multasCgpBallester);
        
        
        
        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------

        
        
        PointEntity punto1CgpRecoleta = new PointEntity();
        punto1CgpRecoleta.setX(25.5);
        punto1CgpRecoleta.setY(150);
        
        PointEntity punto2CgpRecoleta = new PointEntity();
        punto2CgpRecoleta.setX(-45.6);
        punto2CgpRecoleta.setY(-155);

        ComunaEntity comunaRecoleta = new ComunaEntity();
        comunaRecoleta.setNumero(2);
        comunaRecoleta.agregarPunto(punto1CgpRecoleta);
        comunaRecoleta.agregarPunto(punto2CgpRecoleta);

        DireccionEntity direccionCgpRecoleta = new DireccionEntity();
        direccionCgpRecoleta.setNumero(1022);
        direccionCgpRecoleta.setBarrio("Recoleta");
        direccionCgpRecoleta.setCallePrincipal("Uriburu");
        direccionCgpRecoleta.setCodigoPostal(1128);
        direccionCgpRecoleta.setCoordX(580);
        direccionCgpRecoleta.setCoordY(110.3);
        direccionCgpRecoleta.setDepartamento("");
        direccionCgpRecoleta.setEntreCalle1("Santa fe");
        direccionCgpRecoleta.setEntreCalle2("Alvear");
        direccionCgpRecoleta.setLocalidad("Barrio norte");
        direccionCgpRecoleta.setPais("Argentina");
        direccionCgpRecoleta.setPiso(0);
        direccionCgpRecoleta.setProvincia("CABA");
        direccionCgpRecoleta.setUnidad(0);


        Calendar horaComienzoMiercolesCgpRecoleta = Calendar.getInstance();
        horaComienzoMiercolesCgpRecoleta.set(Calendar.HOUR_OF_DAY, 12);
        horaComienzoMiercolesCgpRecoleta.set(Calendar.MINUTE, 0);
        horaComienzoMiercolesCgpRecoleta.set(Calendar.SECOND, 0);

        Calendar horaFinMiercolesCgpRecoleta = Calendar.getInstance();
        horaFinMiercolesCgpRecoleta.set(Calendar.HOUR_OF_DAY, 20);
        horaFinMiercolesCgpRecoleta.set(Calendar.MINUTE, 0);
        horaFinMiercolesCgpRecoleta.set(Calendar.SECOND, 0);

        HorarioEntity horarioMiercolesCgpRecoleta = new HorarioEntity();
        horarioMiercolesCgpRecoleta.setDia(4);
        horarioMiercolesCgpRecoleta.setHoraComienzo(horaComienzoMiercolesCgpRecoleta);
        horarioMiercolesCgpRecoleta.setHoraFin(horaFinMiercolesCgpRecoleta);

        HorarioDeAtencionEntity direccionDeLaMujerHorarioAtencionCgpRecoleta = new HorarioDeAtencionEntity();
        direccionDeLaMujerHorarioAtencionCgpRecoleta.agregarHorario(horarioMiercolesCgpRecoleta);

        ServicioEntity direccionDeLaMujer = new ServicioEntity();
        direccionDeLaMujer.setNombre("direccion de la mujer");
        direccionDeLaMujer.setHorarioDeAtencion(direccionDeLaMujerHorarioAtencionCgpRecoleta);
        
        Calendar horaComienzoLunesCgpRecoleta = Calendar.getInstance();
        horaComienzoLunesCgpRecoleta.set(Calendar.HOUR_OF_DAY, 8);
        horaComienzoLunesCgpRecoleta.set(Calendar.MINUTE, 30);
        horaComienzoLunesCgpRecoleta.set(Calendar.SECOND, 0);

        Calendar horaFinLunesCgpRecoleta = Calendar.getInstance();
        horaFinLunesCgpRecoleta.set(Calendar.HOUR_OF_DAY, 18);
        horaFinLunesCgpRecoleta.set(Calendar.MINUTE, 0);
        horaFinLunesCgpRecoleta.set(Calendar.SECOND, 0);

        HorarioEntity horarioLunesCgpRecoleta = new HorarioEntity();
        horarioLunesCgpRecoleta.setDia(2);
        horarioLunesCgpRecoleta.setHoraComienzo(horaComienzoLunesCgpRecoleta);
        horarioLunesCgpRecoleta.setHoraFin(horaFinLunesCgpRecoleta);


        Calendar horaComienzoMartesCgpRecoleta = Calendar.getInstance();
        horaComienzoMartesCgpRecoleta.set(Calendar.HOUR_OF_DAY, 9);
        horaComienzoMartesCgpRecoleta.set(Calendar.MINUTE, 30);
        horaComienzoMartesCgpRecoleta.set(Calendar.SECOND, 0);

        Calendar horaFinMartesCgpRecoleta = Calendar.getInstance();
        horaFinMartesCgpRecoleta.set(Calendar.HOUR_OF_DAY, 17);
        horaFinMartesCgpRecoleta.set(Calendar.MINUTE, 30);
        horaFinMartesCgpRecoleta.set(Calendar.SECOND, 0);

        HorarioEntity horarioMartesCgpRecoleta = new HorarioEntity();
        horarioMartesCgpRecoleta.setDia(3);
        horarioMartesCgpRecoleta.setHoraComienzo(horaComienzoMartesCgpRecoleta);
        horarioMartesCgpRecoleta.setHoraFin(horaFinMartesCgpRecoleta);

        HorarioDeAtencionEntity ecoBiciHorarioAtencion = new HorarioDeAtencionEntity();
        ecoBiciHorarioAtencion.agregarHorario(horarioLunesCgpRecoleta);
        ecoBiciHorarioAtencion.agregarHorario(horarioMartesCgpRecoleta);

        ServicioEntity ecoBici = new ServicioEntity();
        ecoBici.setNombre("EcoBici");
        ecoBici.setHorarioDeAtencion(ecoBiciHorarioAtencion);

        cgpRecoleta = new CGPEntity();
        cgpRecoleta.setNombre("Centro Recoleta");
        cgpRecoleta.setIcono("/cgp_icono.jpg");
        cgpRecoleta.setRangoCercania(0.5);  // Dejarlo siempre igual
        cgpRecoleta.setComuna(comunaRecoleta);
        cgpRecoleta.setDireccion(direccionCgpRecoleta);
        cgpRecoleta.agregarServicio(ecoBici);
        cgpRecoleta.agregarServicio(direccionDeLaMujer);
        
        
        
        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------
        
                

        PointEntity punto1CgpCaballito = new PointEntity();
        punto1CgpCaballito.setX(55.6);
        punto1CgpCaballito.setY(325.7);
        
        PointEntity punto2CgpCaballito = new PointEntity();
        punto2CgpCaballito.setX(-55.6);
        punto2CgpCaballito.setY(-325.7);

        ComunaEntity comunaCaballito = new ComunaEntity();
        comunaCaballito.setNumero(6);
        comunaCaballito.agregarPunto(punto1CgpCaballito);
        comunaCaballito.agregarPunto(punto2CgpCaballito);

        DireccionEntity direccionCgpCaballito = new DireccionEntity();
        direccionCgpCaballito.setNumero(277);
        direccionCgpCaballito.setBarrio("Caballito");
        direccionCgpCaballito.setCallePrincipal("Patricias Argentinas");
        direccionCgpCaballito.setCodigoPostal(1406);
        direccionCgpCaballito.setCoordX(525);
        direccionCgpCaballito.setCoordY(150.3);
        direccionCgpCaballito.setDepartamento("");
        direccionCgpCaballito.setEntreCalle1("Franklin");
        direccionCgpCaballito.setEntreCalle2("Cangallo");
        direccionCgpCaballito.setLocalidad("Caballito");
        direccionCgpCaballito.setPais("Argentina");
        direccionCgpCaballito.setPiso(0);
        direccionCgpCaballito.setProvincia("CABA");
        direccionCgpCaballito.setUnidad(0);

        Calendar horaComienzoLunesCgpCaballito = Calendar.getInstance();
        horaComienzoLunesCgpCaballito.set(Calendar.HOUR_OF_DAY, 8);
        horaComienzoLunesCgpCaballito.set(Calendar.MINUTE, 30);
        horaComienzoLunesCgpCaballito.set(Calendar.SECOND, 0);

        Calendar horaFinLunesCgpCaballito = Calendar.getInstance();
        horaFinLunesCgpCaballito.set(Calendar.HOUR_OF_DAY, 18);
        horaFinLunesCgpCaballito.set(Calendar.MINUTE, 0);
        horaFinLunesCgpCaballito.set(Calendar.SECOND, 0);

        HorarioEntity horarioLunesCgpCaballito = new HorarioEntity();
        horarioLunesCgpCaballito.setDia(2);
        horarioLunesCgpCaballito.setHoraComienzo(horaComienzoLunesCgpCaballito);
        horarioLunesCgpCaballito.setHoraFin(horaFinLunesCgpCaballito);
        
        HorarioDeAtencionEntity mesaInformesHorarioAtencion = new HorarioDeAtencionEntity();
        mesaInformesHorarioAtencion.agregarHorario(horarioLunesCgpCaballito);
        
        ServicioEntity mesaDeInformes = new ServicioEntity();
        mesaDeInformes.setNombre("Mesa de informes");
        mesaDeInformes.setHorarioDeAtencion(mesaInformesHorarioAtencion);
               
        Calendar horaComienzoJuevesCgpCaballito= Calendar.getInstance();
        horaComienzoJuevesCgpCaballito.set(Calendar.HOUR_OF_DAY, 10);
        horaComienzoJuevesCgpCaballito.set(Calendar.MINUTE, 30);
        horaComienzoJuevesCgpCaballito.set(Calendar.SECOND, 0);

        Calendar horaFinJuevesCgpCaballito = Calendar.getInstance();
        horaFinJuevesCgpCaballito.set(Calendar.HOUR_OF_DAY, 18);
        horaFinJuevesCgpCaballito.set(Calendar.MINUTE, 30);
        horaFinJuevesCgpCaballito.set(Calendar.SECOND, 0);

        HorarioEntity horarioJuevesCgpCaballito = new HorarioEntity();
        horarioJuevesCgpCaballito .setDia(5);
        horarioJuevesCgpCaballito .setHoraComienzo(horaComienzoJuevesCgpCaballito);
        horarioJuevesCgpCaballito .setHoraFin(horaFinJuevesCgpCaballito);


        Calendar horaComienzoViernesCgpCaballito = Calendar.getInstance();
        horaComienzoViernesCgpCaballito.set(Calendar.HOUR_OF_DAY, 9);
        horaComienzoViernesCgpCaballito.set(Calendar.MINUTE, 0);
        horaComienzoViernesCgpCaballito.set(Calendar.SECOND, 0);

        Calendar horaFinViernesCgpCaballito = Calendar.getInstance();
        horaFinViernesCgpCaballito.set(Calendar.HOUR_OF_DAY, 17);
        horaFinViernesCgpCaballito.set(Calendar.MINUTE, 0);
        horaFinViernesCgpCaballito.set(Calendar.SECOND, 0);

        HorarioEntity horarioViernesCgpCaballito = new HorarioEntity();
        horarioViernesCgpCaballito.setDia(6);
        horarioViernesCgpCaballito.setHoraComienzo(horaComienzoViernesCgpCaballito);
        horarioViernesCgpCaballito.setHoraFin(horaFinViernesCgpCaballito);

        HorarioDeAtencionEntity registroCivilHorarioAtencion = new HorarioDeAtencionEntity();
        registroCivilHorarioAtencion.agregarHorario(horarioJuevesCgpCaballito);
        registroCivilHorarioAtencion.agregarHorario(horarioViernesCgpCaballito);

        ServicioEntity registroCivil = new ServicioEntity();
        registroCivil.setNombre("Registro civil");
        registroCivil.setHorarioDeAtencion(registroCivilHorarioAtencion);

        cgpCaballito = new CGPEntity();
        cgpCaballito.setNombre("Centro Caballito");
        cgpCaballito.setIcono("/cgp_icono.jpg");
        cgpCaballito.setRangoCercania(0.5);  // Dejarlo siempre igual
        cgpCaballito.setComuna(comunaCaballito);
        cgpCaballito.setDireccion(direccionCgpCaballito);
        cgpCaballito.agregarServicio(registroCivil);
        cgpCaballito.agregarServicio(mesaDeInformes);
        
        cgpDAO.save(cgpBallester);
        cgpDAO.save(cgpCaballito);
        cgpDAO.save(cgpRecoleta);
        


        /*CREACION DE COLECTIVOS*/

        DireccionEntity direccionColectivo114 = new DireccionEntity();
        direccionColectivo114.setNumero(1401);
        direccionColectivo114.setBarrio("Belgrano");
        direccionColectivo114.setCallePrincipal("Arribeños");
        direccionColectivo114.setCodigoPostal(1490);
        direccionColectivo114.setCoordX(-34.564289);
        direccionColectivo114.setCoordY(-58.445050);
        direccionColectivo114.setDepartamento("");
        direccionColectivo114.setEntreCalle1("Zabala");
        direccionColectivo114.setEntreCalle2("Virrey Loreto");
        direccionColectivo114.setLocalidad("Capital Federal");
        direccionColectivo114.setPais("Argentina");
        direccionColectivo114.setPiso(0);
        direccionColectivo114.setProvincia("Buenos Aires");
        direccionColectivo114.setUnidad(0);

        DireccionEntity direccionColectivo114Parada1;
        direccionColectivo114Parada1 = new DireccionEntity();
        direccionColectivo114Parada1.setNumero(1730);
        direccionColectivo114Parada1.setBarrio("Belgrano");
        direccionColectivo114Parada1.setCallePrincipal("José Hernández");
        direccionColectivo114Parada1.setCodigoPostal(1490);
        direccionColectivo114Parada1.setCoordX(-34.562257);
        direccionColectivo114Parada1.setCoordY(-58.447283);
        direccionColectivo114Parada1.setDepartamento("");
        direccionColectivo114Parada1.setEntreCalle1("11 de Septiembre");
        direccionColectivo114Parada1.setEntreCalle2("Arribeños");
        direccionColectivo114Parada1.setLocalidad("Capital Federal");
        direccionColectivo114Parada1.setPais("Argentina");
        direccionColectivo114Parada1.setPiso(0);
        direccionColectivo114Parada1.setProvincia("Buenos Aires");
        direccionColectivo114Parada1.setUnidad(0);

        DireccionEntity direccionColectivo114Parada2;
        direccionColectivo114Parada2 = new DireccionEntity();
        direccionColectivo114Parada2.setNumero(2390);
        direccionColectivo114Parada2.setBarrio("Belgrano");
        direccionColectivo114Parada2.setCallePrincipal("José Hernández");
        direccionColectivo114Parada2.setCodigoPostal(1490);
        direccionColectivo114Parada2.setCoordX(-34.565476);
        direccionColectivo114Parada2.setCoordY(-58.452874);
        direccionColectivo114Parada2.setDepartamento("");
        direccionColectivo114Parada2.setEntreCalle1("Vuelta de Obligado");
        direccionColectivo114Parada2.setEntreCalle2("Av. Cabildo");
        direccionColectivo114Parada2.setLocalidad("Capital Federal");
        direccionColectivo114Parada2.setPais("Argentina");
        direccionColectivo114Parada2.setPiso(0);
        direccionColectivo114Parada2.setProvincia("Buenos Aires");
        direccionColectivo114Parada2.setUnidad(0);

        ParadaEntity parada1Colectivo114 = new ParadaEntity();
        parada1Colectivo114.setDireccion(direccionColectivo114Parada1);

        ParadaEntity parada2Colectivo114 = new ParadaEntity();
        parada2Colectivo114.setDireccion(direccionColectivo114Parada2);

        colectivo114 = new ColectivoEntity();
        colectivo114.setNombre("Colectivos SanMartin srl");
        colectivo114.setIcono("/colectivo_icono.jpg");
        colectivo114.setRangoCercania(0.1);  // Dejarlo siempre igual
        colectivo114.setNroLinea(114);
        colectivo114.setDireccion(direccionColectivo114);
        colectivo114.agregarParada(parada1Colectivo114);
        colectivo114.agregarParada(parada2Colectivo114);
        
        
        
        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------
        
        
        
        DireccionEntity direccionColectivo152 = new DireccionEntity();
        direccionColectivo152.setNumero(3200);
        direccionColectivo152.setBarrio("Olivos");
        direccionColectivo152.setCallePrincipal("Bermudez");
        direccionColectivo152.setCodigoPostal(1636);
        direccionColectivo152.setCoordX(-84.564289);
        direccionColectivo152.setCoordY(-120.445050);
        direccionColectivo152.setDepartamento("");
        direccionColectivo152.setEntreCalle1("O´higins");
        direccionColectivo152.setEntreCalle2("Sarmiento");
        direccionColectivo152.setLocalidad("Vicente lopez");
        direccionColectivo152.setPais("Argentina");
        direccionColectivo152.setPiso(0);
        direccionColectivo152.setProvincia("Buenos Aires");
        direccionColectivo152.setUnidad(0);

        DireccionEntity direccionColectivo152Parada1;
        direccionColectivo152Parada1 = new DireccionEntity();
        direccionColectivo152Parada1.setNumero(3000);
        direccionColectivo152Parada1.setBarrio("Olivos");
        direccionColectivo152Parada1.setCallePrincipal("Bermudez");
        direccionColectivo152Parada1.setCodigoPostal(1636);
        direccionColectivo152Parada1.setCoordX(-84.562257);
        direccionColectivo152Parada1.setCoordY(-90.447283);
        direccionColectivo152Parada1.setDepartamento("");
        direccionColectivo152Parada1.setEntreCalle1("Juan b justo");
        direccionColectivo152Parada1.setEntreCalle2("Pedro goyena");
        direccionColectivo152Parada1.setLocalidad("Vicente lopez");
        direccionColectivo152Parada1.setPais("Argentina");
        direccionColectivo152Parada1.setPiso(0);
        direccionColectivo152Parada1.setProvincia("Buenos Aires");
        direccionColectivo152Parada1.setUnidad(0);

        DireccionEntity direccionColectivo152Parada2;
        direccionColectivo152Parada2 = new DireccionEntity();
        direccionColectivo152Parada2.setNumero(2800);
        direccionColectivo152Parada2.setBarrio("Olivos");
        direccionColectivo152Parada2.setCallePrincipal("Bermudez");
        direccionColectivo152Parada2.setCodigoPostal(1636);
        direccionColectivo152Parada2.setCoordX(-84.565476);
        direccionColectivo152Parada2.setCoordY(-70.452874);
        direccionColectivo152Parada2.setDepartamento("");
        direccionColectivo152Parada2.setEntreCalle1("España");
        direccionColectivo152Parada2.setEntreCalle2("Ayacucho");
        direccionColectivo152Parada2.setLocalidad("Vicente lopez");
        direccionColectivo152Parada2.setPais("Argentina");
        direccionColectivo152Parada2.setPiso(0);
        direccionColectivo152Parada2.setProvincia("Buenos Aires");
        direccionColectivo152Parada2.setUnidad(0);
        
        DireccionEntity direccionColectivo152Parada3;
        direccionColectivo152Parada3 = new DireccionEntity();
        direccionColectivo152Parada3.setNumero(2600);
        direccionColectivo152Parada3.setBarrio("Olivos");
        direccionColectivo152Parada3.setCallePrincipal("Bermudez");
        direccionColectivo152Parada3.setCodigoPostal(1636);
        direccionColectivo152Parada3.setCoordX(-84.565476);
        direccionColectivo152Parada3.setCoordY(-50.452874);
        direccionColectivo152Parada3.setDepartamento("");
        direccionColectivo152Parada3.setEntreCalle1("J M paz");
        direccionColectivo152Parada3.setEntreCalle2("Jonas salk");
        direccionColectivo152Parada3.setLocalidad("Vicente lopez");
        direccionColectivo152Parada3.setPais("Argentina");
        direccionColectivo152Parada3.setPiso(0);
        direccionColectivo152Parada3.setProvincia("Buenos Aires");
        direccionColectivo152Parada3.setUnidad(0);

        ParadaEntity parada1Colectivo152 = new ParadaEntity();
        parada1Colectivo152.setDireccion(direccionColectivo152Parada1);

        ParadaEntity parada2Colectivo152 = new ParadaEntity();
        parada2Colectivo152.setDireccion(direccionColectivo152Parada2);
        
        ParadaEntity parada3Colectivo152 = new ParadaEntity();
        parada3Colectivo152.setDireccion(direccionColectivo152Parada3);

        colectivo152 = new ColectivoEntity();
        colectivo152.setNombre("Colectivos Vicente Lopez-Capital Federal");
        colectivo152.setIcono("/colectivo_icono.jpg");
        colectivo152.setRangoCercania(0.1);  // Dejarlo siempre igual
        colectivo152.setNroLinea(152);
        colectivo152.setDireccion(direccionColectivo152);
        colectivo152.agregarParada(parada1Colectivo152);
        colectivo152.agregarParada(parada2Colectivo152);
        colectivo152.agregarParada(parada3Colectivo152);
        
        
        
        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------
        
        

        DireccionEntity direccionColectivo19 = new DireccionEntity();
        direccionColectivo19.setNumero(4500);
        direccionColectivo19.setBarrio("Carapachay");
        direccionColectivo19.setCallePrincipal("Acassuso");
        direccionColectivo19.setCodigoPostal(1654);
        direccionColectivo19.setCoordX(-234.564289);
        direccionColectivo19.setCoordY(-58.445050);
        direccionColectivo19.setDepartamento("");
        direccionColectivo19.setEntreCalle1("Posadas");
        direccionColectivo19.setEntreCalle2("Washington");
        direccionColectivo19.setLocalidad("Vicente Lopez");
        direccionColectivo19.setPais("Argentina");
        direccionColectivo19.setPiso(0);
        direccionColectivo19.setProvincia("Buenos Aires");
        direccionColectivo19.setUnidad(0);

        DireccionEntity direccionColectivo19Parada1;
        direccionColectivo19Parada1 = new DireccionEntity();
        direccionColectivo19Parada1.setNumero(3300);
        direccionColectivo19Parada1.setBarrio("Carapachay");
        direccionColectivo19Parada1.setCallePrincipal("Acassuso");
        direccionColectivo19Parada1.setCodigoPostal(1490);
        direccionColectivo19Parada1.setCoordX(-234.562257);
        direccionColectivo19Parada1.setCoordY(-28.447283);
        direccionColectivo19Parada1.setDepartamento("");
        direccionColectivo19Parada1.setEntreCalle1("Grierson");
        direccionColectivo19Parada1.setEntreCalle2("Antartida argentina");
        direccionColectivo19Parada1.setLocalidad("Vicente lopez");
        direccionColectivo19Parada1.setPais("Argentina");
        direccionColectivo19Parada1.setPiso(0);
        direccionColectivo19Parada1.setProvincia("Buenos Aires");
        direccionColectivo19Parada1.setUnidad(0);

        ParadaEntity parada1Colectivo19 = new ParadaEntity();
        parada1Colectivo19.setDireccion(direccionColectivo19Parada1);


        colectivo19 = new ColectivoEntity();
        colectivo19.setNombre("Colectivos Carapachay-Plaza miserele");
        colectivo19.setIcono("/colectivo_icono.jpg");
        colectivo19.setRangoCercania(0.1);  // Dejarlo siempre igual
        colectivo19.setNroLinea(19);
        colectivo19.setDireccion(direccionColectivo19);
        colectivo19.agregarParada(parada1Colectivo19);
        
        colectivoDAO.save(colectivo114);
        colectivoDAO.save(colectivo152);
        colectivoDAO.save(colectivo19);
        
        /*CREACION DE LOCALES COMERCIALES*/



        DireccionEntity direccionLocalKiosko = new DireccionEntity();
        direccionLocalKiosko.setNumero(5366);
        direccionLocalKiosko.setBarrio("Villa Urquiza");
        direccionLocalKiosko.setCallePrincipal("Cullen");
        direccionLocalKiosko.setCodigoPostal(6780);
        direccionLocalKiosko.setCoordX(-34.574479);
        direccionLocalKiosko.setCoordY(-58.490787);
        direccionLocalKiosko.setDepartamento("");
        direccionLocalKiosko.setEntreCalle1("Andonaegui");
        direccionLocalKiosko.setEntreCalle2("Bucarelli");
        direccionLocalKiosko.setLocalidad("Capital Federal");
        direccionLocalKiosko.setPais("Argentina");
        direccionLocalKiosko.setPiso(0);
        direccionLocalKiosko.setProvincia("Buenos Aires");
        direccionLocalKiosko.setUnidad(0);

        RubroEntity rubroAlmacen = new RubroEntity();
        rubroAlmacen.setNombre("Almacen");
        rubroAlmacen.setRangoCercania(0.2);

        PalabraClaveEntity palabraClavePirulo = new PalabraClaveEntity();
        palabraClavePirulo.setPalabra("Pirulo");
        PalabraClaveEntity palabraClaveKiosko = new PalabraClaveEntity();
        palabraClaveKiosko.setPalabra("Kiosco");
        PalabraClaveEntity palabraClaveGolosinas = new PalabraClaveEntity();
        palabraClaveGolosinas.setPalabra("Golosinas");

        Calendar horaComienzoLunesKioskoPirulo = Calendar.getInstance();
        horaComienzoLunesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 2);
        horaComienzoLunesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinLunesKioskoPirulo = Calendar.getInstance();
        horaFinLunesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 2);
        horaFinLunesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioLunesKioskoPirulo = new HorarioEntity();
        horarioLunesKioskoPirulo.setDia(2);
        horarioLunesKioskoPirulo.setHoraComienzo(horaComienzoLunesKioskoPirulo);
        horarioLunesKioskoPirulo.setHoraFin(horaFinLunesKioskoPirulo);


        Calendar horaComienzoMartesKioskoPirulo = Calendar.getInstance();
        horaComienzoMartesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 3);
        horaComienzoMartesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinMartesKioskoPirulo = Calendar.getInstance();
        horaFinMartesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 3);
        horaFinMartesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioMartesKioskoPirulo = new HorarioEntity();
        horarioMartesKioskoPirulo.setDia(3);
        horarioMartesKioskoPirulo.setHoraComienzo(horaComienzoMartesKioskoPirulo);
        horarioMartesKioskoPirulo.setHoraFin(horaFinMartesKioskoPirulo);


        Calendar horaComienzoMiercolesKioskoPirulo = Calendar.getInstance();
        horaComienzoMiercolesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 4);
        horaComienzoMiercolesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinMiercolesKioskoPirulo = Calendar.getInstance();
        horaFinMiercolesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 4);
        horaFinMiercolesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioMiercolesKioskoPirulo = new HorarioEntity();
        horarioMiercolesKioskoPirulo.setDia(4);
        horarioMiercolesKioskoPirulo.setHoraComienzo(horaComienzoMiercolesKioskoPirulo);
        horarioMiercolesKioskoPirulo.setHoraFin(horaFinMiercolesKioskoPirulo);


        Calendar horaComienzoJuevesKioskoPirulo = Calendar.getInstance();
        horaComienzoJuevesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 5);
        horaComienzoJuevesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinJuevesKioskoPirulo = Calendar.getInstance();
        horaFinJuevesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 5);
        horaFinJuevesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioJuevesKioskoPirulo = new HorarioEntity();
        horarioJuevesKioskoPirulo.setDia(5);
        horarioJuevesKioskoPirulo.setHoraComienzo(horaComienzoJuevesKioskoPirulo);
        horarioJuevesKioskoPirulo.setHoraFin(horaFinJuevesKioskoPirulo);


        Calendar horaComienzoViernesKioskoPirulo = Calendar.getInstance();
        horaComienzoViernesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 6);
        horaComienzoViernesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinViernesKioskoPirulo = Calendar.getInstance();
        horaFinViernesKioskoPirulo.set(Calendar.DAY_OF_WEEK, 6);
        horaFinViernesKioskoPirulo.set(Calendar.HOUR_OF_DAY, 22);

        HorarioEntity horarioViernesKioskoPirulo = new HorarioEntity();
        horarioViernesKioskoPirulo.setDia(6);
        horarioViernesKioskoPirulo.setHoraComienzo(horaComienzoViernesKioskoPirulo);
        horarioViernesKioskoPirulo.setHoraFin(horaFinViernesKioskoPirulo);


        HorarioDeAtencionEntity horarioAtencionKioskoPirulo = new HorarioDeAtencionEntity();
        horarioAtencionKioskoPirulo.agregarHorario(horarioLunesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioMartesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioMiercolesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioJuevesKioskoPirulo);
        horarioAtencionKioskoPirulo.agregarHorario(horarioViernesKioskoPirulo);


        localKiosko = new LocalComercialEntity();
        localKiosko.setNombre("Kiosko Pirulo");
        localKiosko.setIcono("/local_comercial_icono.jpg");
        localKiosko.setRangoCercania(0d);
        localKiosko.setDireccion(direccionLocalKiosko);
        localKiosko.setRubro(rubroAlmacen);
        localKiosko.agregarPalabraClave(palabraClavePirulo);
        localKiosko.agregarPalabraClave(palabraClaveKiosko);
        localKiosko.agregarPalabraClave(palabraClaveGolosinas);
        localKiosko.setHorarioDeAtencion(horarioAtencionKioskoPirulo);



        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------



        DireccionEntity direccionLocalBurgerKing = new DireccionEntity();
        direccionLocalBurgerKing.setNumero(3000);
        direccionLocalBurgerKing.setBarrio("Olivos");
        direccionLocalBurgerKing.setCallePrincipal("Av.maipu");
        direccionLocalBurgerKing.setCodigoPostal(1636);
        direccionLocalBurgerKing.setCoordX(100.574479);
        direccionLocalBurgerKing.setCoordY(200.490787);
        direccionLocalBurgerKing.setDepartamento("");
        direccionLocalBurgerKing.setEntreCalle1("Pelliza");
        direccionLocalBurgerKing.setEntreCalle2("Roque saenz peña");
        direccionLocalBurgerKing.setLocalidad("Vicente lopez");
        direccionLocalBurgerKing.setPais("Argentina");
        direccionLocalBurgerKing.setPiso(0);
        direccionLocalBurgerKing.setProvincia("Buenos Aires");
        direccionLocalBurgerKing.setUnidad(0);

        RubroEntity rubroHamburgueseria = new RubroEntity();
        rubroHamburgueseria.setNombre("Gastronomico");
        rubroHamburgueseria.setRangoCercania(0.2);

        PalabraClaveEntity palabraClaveHamburguesa = new PalabraClaveEntity();
        palabraClaveHamburguesa.setPalabra("Hamburguesa");
        PalabraClaveEntity palabraClaveCombos = new PalabraClaveEntity();
        palabraClaveCombos.setPalabra("Combos");
        PalabraClaveEntity palabraClavePapasFritas = new PalabraClaveEntity();
        palabraClavePapasFritas.setPalabra("Papas");
        PalabraClaveEntity palabraClaveComidaRapida = new PalabraClaveEntity();
        palabraClaveComidaRapida.setPalabra("Stacker");

        Calendar horaComienzoLunesBurgerKing = Calendar.getInstance();
        horaComienzoLunesBurgerKing.set(Calendar.DAY_OF_WEEK, 2);
        horaComienzoLunesBurgerKing.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinLunesBurgerKing = Calendar.getInstance();
        horaFinLunesBurgerKing.set(Calendar.DAY_OF_WEEK, 2);
        horaFinLunesBurgerKing.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioLunesBurgerKing = new HorarioEntity();
        horarioLunesBurgerKing.setDia(2);
        horarioLunesBurgerKing.setHoraComienzo(horaComienzoLunesBurgerKing);
        horarioLunesBurgerKing.setHoraFin(horaFinLunesBurgerKing);


        Calendar horaComienzoMartesBurgerKing = Calendar.getInstance();
        horaComienzoMartesBurgerKing.set(Calendar.DAY_OF_WEEK, 3);
        horaComienzoMartesBurgerKing.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinMartesBurgerKing= Calendar.getInstance();
        horaFinMartesBurgerKing.set(Calendar.DAY_OF_WEEK, 3);
        horaFinMartesBurgerKing.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioMartesBurgerKing= new HorarioEntity();
        horarioMartesBurgerKing.setDia(3);
        horarioMartesBurgerKing.setHoraComienzo(horaComienzoMartesBurgerKing);
        horarioMartesBurgerKing.setHoraFin(horaFinMartesBurgerKing);


        Calendar horaComienzoMiercolesBurgerKing = Calendar.getInstance();
        horaComienzoMiercolesBurgerKing.set(Calendar.DAY_OF_WEEK, 4);
        horaComienzoMiercolesBurgerKing.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinMiercolesBurgerKing = Calendar.getInstance();
        horaFinMiercolesBurgerKing.set(Calendar.DAY_OF_WEEK, 4);
        horaFinMiercolesBurgerKing.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioMiercolesBurgerKing = new HorarioEntity();
        horarioMiercolesBurgerKing.setDia(4);
        horarioMiercolesBurgerKing.setHoraComienzo(horaComienzoMiercolesBurgerKing);
        horarioMiercolesBurgerKing.setHoraFin(horaFinMiercolesBurgerKing);


        Calendar horaComienzoJuevesBurgerKing = Calendar.getInstance();
        horaComienzoJuevesBurgerKing.set(Calendar.DAY_OF_WEEK, 5);
        horaComienzoJuevesBurgerKing.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinJuevesBurgerKing = Calendar.getInstance();
        horaFinJuevesBurgerKing.set(Calendar.DAY_OF_WEEK, 5);
        horaFinJuevesBurgerKing.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioJuevesBurgerKing = new HorarioEntity();
        horarioJuevesBurgerKing.setDia(5);
        horarioJuevesBurgerKing.setHoraComienzo(horaComienzoJuevesBurgerKing);
        horarioJuevesBurgerKing.setHoraFin(horaFinJuevesBurgerKing);


        Calendar horaComienzoViernesBurgerKing = Calendar.getInstance();
        horaComienzoViernesBurgerKing.set(Calendar.DAY_OF_WEEK, 6);
        horaComienzoViernesBurgerKing.set(Calendar.HOUR_OF_DAY, 8);

        Calendar horaFinViernesBurgerKing = Calendar.getInstance();
        horaFinViernesBurgerKing.set(Calendar.DAY_OF_WEEK, 6);
        horaFinViernesBurgerKing.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioViernesBurgerKing = new HorarioEntity();
        horarioViernesBurgerKing.setDia(6);
        horarioViernesBurgerKing.setHoraComienzo(horaComienzoViernesKioskoPirulo);
        horarioViernesBurgerKing.setHoraFin(horaFinViernesKioskoPirulo);


        Calendar horaComienzoSabadoBurgerKing = Calendar.getInstance();
        horaComienzoSabadoBurgerKing.set(Calendar.DAY_OF_WEEK, 7);
        horaComienzoSabadoBurgerKing.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinSabadoBurgerKing = Calendar.getInstance();
        horaFinSabadoBurgerKing.set(Calendar.DAY_OF_WEEK, 7);
        horaFinSabadoBurgerKing.set(Calendar.HOUR_OF_DAY, 7);

        HorarioEntity horarioSabadoBurgerKing= new HorarioEntity();
        horarioSabadoBurgerKing.setDia(7);
        horarioSabadoBurgerKing.setHoraComienzo(horaComienzoSabadoBurgerKing);
        horarioSabadoBurgerKing.setHoraFin(horaFinSabadoBurgerKing);

        HorarioDeAtencionEntity horarioAtencionBurgerKing = new HorarioDeAtencionEntity();
        horarioAtencionBurgerKing.agregarHorario(horarioLunesBurgerKing);
        horarioAtencionBurgerKing.agregarHorario(horarioMartesBurgerKing);
        horarioAtencionBurgerKing.agregarHorario(horarioMiercolesBurgerKing);
        horarioAtencionBurgerKing.agregarHorario(horarioJuevesBurgerKing);
        horarioAtencionBurgerKing.agregarHorario(horarioViernesBurgerKing);
        horarioAtencionBurgerKing.agregarHorario(horarioSabadoBurgerKing);


        burgerKing = new LocalComercialEntity();
        burgerKing.setNombre("Burger King");
        burgerKing.setIcono("/local_comercial_icono.jpg");
        burgerKing.setRangoCercania(0d);
        burgerKing.setDireccion(direccionLocalBurgerKing);
        burgerKing.setRubro(rubroHamburgueseria);
        burgerKing.agregarPalabraClave(palabraClaveHamburguesa);
        burgerKing.agregarPalabraClave(palabraClaveCombos);
        burgerKing.agregarPalabraClave(palabraClavePapasFritas);
        burgerKing.agregarPalabraClave(palabraClaveComidaRapida);
        burgerKing.setHorarioDeAtencion(horarioAtencionBurgerKing);



        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------



        DireccionEntity direccionLocalUnicenter = new DireccionEntity();
        direccionLocalUnicenter.setNumero(4000);
        direccionLocalUnicenter.setBarrio("Martinez");
        direccionLocalUnicenter.setCallePrincipal("Parana");
        direccionLocalUnicenter.setCodigoPostal(1648);
        direccionLocalUnicenter.setCoordX(50.574479);
        direccionLocalUnicenter.setCoordY(100.490787);
        direccionLocalUnicenter.setDepartamento("");
        direccionLocalUnicenter.setEntreCalle1("Mexico");
        direccionLocalUnicenter.setEntreCalle2("Costa rica");
        direccionLocalUnicenter.setLocalidad("San isidro");
        direccionLocalUnicenter.setPais("Argentina");
        direccionLocalUnicenter.setPiso(0);
        direccionLocalUnicenter.setProvincia("Buenos Aires");
        direccionLocalUnicenter.setUnidad(0);

        RubroEntity rubroShopping = new RubroEntity();
        rubroShopping.setNombre("Shopping");
        rubroShopping.setRangoCercania(0.2);

        PalabraClaveEntity palabraClaveShopping = new PalabraClaveEntity();
        palabraClaveShopping.setPalabra("Shopping");
        PalabraClaveEntity palabraClaveCompras= new PalabraClaveEntity();
        palabraClaveCompras.setPalabra("Compras");

        Calendar horaComienzoLunesUnicenter = Calendar.getInstance();
        horaComienzoLunesUnicenter.set(Calendar.DAY_OF_WEEK, 2);
        horaComienzoLunesUnicenter.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinLunesUnicenter = Calendar.getInstance();
        horaFinLunesUnicenter.set(Calendar.DAY_OF_WEEK, 2);
        horaFinLunesUnicenter.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioLunesUnicenter = new HorarioEntity();
        horarioLunesUnicenter.setDia(2);
        horarioLunesUnicenter.setHoraComienzo(horaComienzoLunesUnicenter);
        horarioLunesUnicenter.setHoraFin(horaFinLunesUnicenter);


        Calendar horaComienzoMartesUnicenter = Calendar.getInstance();
        horaComienzoMartesUnicenter.set(Calendar.DAY_OF_WEEK, 3);
        horaComienzoMartesUnicenter.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinMartesUnicenter= Calendar.getInstance();
        horaFinMartesUnicenter.set(Calendar.DAY_OF_WEEK, 3);
        horaFinMartesUnicenter.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioMartesUnicenter= new HorarioEntity();
        horarioMartesUnicenter.setDia(3);
        horarioMartesUnicenter.setHoraComienzo(horaComienzoMartesUnicenter);
        horarioMartesUnicenter.setHoraFin(horaFinMartesUnicenter);


        Calendar horaComienzoMiercolesUnicenter = Calendar.getInstance();
        horaComienzoMiercolesUnicenter.set(Calendar.DAY_OF_WEEK, 4);
        horaComienzoMiercolesUnicenter.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinMiercolesUnicenter = Calendar.getInstance();
        horaFinMiercolesUnicenter.set(Calendar.DAY_OF_WEEK, 4);
        horaFinMiercolesUnicenter.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioMiercolesUnicenter= new HorarioEntity();
        horarioMiercolesUnicenter.setDia(4);
        horarioMiercolesUnicenter.setHoraComienzo(horaComienzoMiercolesUnicenter);
        horarioMiercolesUnicenter.setHoraFin(horaFinMiercolesUnicenter);


        Calendar horaComienzoJuevesUnicenter = Calendar.getInstance();
        horaComienzoJuevesUnicenter.set(Calendar.DAY_OF_WEEK, 5);
        horaComienzoJuevesUnicenter.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinJuevesUnicenter = Calendar.getInstance();
        horaFinJuevesUnicenter.set(Calendar.DAY_OF_WEEK, 5);
        horaFinJuevesUnicenter.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioJuevesUnicenter = new HorarioEntity();
        horarioJuevesUnicenter.setDia(5);
        horarioJuevesUnicenter.setHoraComienzo(horaComienzoJuevesUnicenter);
        horarioJuevesUnicenter.setHoraFin(horaFinJuevesUnicenter);


        Calendar horaComienzoViernesUnicenter = Calendar.getInstance();
        horaComienzoViernesUnicenter.set(Calendar.DAY_OF_WEEK, 6);
        horaComienzoViernesUnicenter.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinViernesUnicenter = Calendar.getInstance();
        horaFinViernesUnicenter.set(Calendar.DAY_OF_WEEK, 6);
        horaFinViernesUnicenter.set(Calendar.HOUR_OF_DAY, 24);

        HorarioEntity horarioViernesUnicenter = new HorarioEntity();
        horarioViernesUnicenter.setDia(6);
        horarioViernesUnicenter.setHoraComienzo(horaComienzoViernesUnicenter);
        horarioViernesUnicenter.setHoraFin(horaFinViernesUnicenter);


        Calendar horaComienzoSabadoUnicenter = Calendar.getInstance();
        horaComienzoSabadoUnicenter.set(Calendar.DAY_OF_WEEK, 7);
        horaComienzoSabadoUnicenter.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinSabadoUnicenter = Calendar.getInstance();
        horaFinSabadoUnicenter.set(Calendar.DAY_OF_WEEK, 7);
        horaFinSabadoUnicenter.set(Calendar.HOUR_OF_DAY, 3);

        HorarioEntity horarioSabadoUnicenter = new HorarioEntity();
        horarioSabadoUnicenter.setDia(7);
        horarioSabadoUnicenter.setHoraComienzo(horaComienzoSabadoUnicenter);
        horarioSabadoUnicenter.setHoraFin(horaFinSabadoUnicenter);

        Calendar horaComienzoDomingoUnicenter = Calendar.getInstance();
        horaComienzoDomingoUnicenter.set(Calendar.DAY_OF_WEEK, 1);
        horaComienzoDomingoUnicenter.set(Calendar.HOUR_OF_DAY, 9);

        Calendar horaFinDomingoUnicenter = Calendar.getInstance();
        horaFinDomingoUnicenter.set(Calendar.DAY_OF_WEEK, 1);
        horaFinDomingoUnicenter.set(Calendar.HOUR_OF_DAY, 23);

        HorarioEntity horarioDomingoUnicenter = new HorarioEntity();
        horarioDomingoUnicenter.setDia(1);
        horarioDomingoUnicenter.setHoraComienzo(horaComienzoDomingoUnicenter);
        horarioDomingoUnicenter.setHoraFin(horaFinDomingoUnicenter);

        HorarioDeAtencionEntity horarioAtencionUnicenter = new HorarioDeAtencionEntity();
        horarioAtencionUnicenter.agregarHorario(horarioLunesUnicenter);
        horarioAtencionUnicenter.agregarHorario(horarioMartesUnicenter);
        horarioAtencionUnicenter.agregarHorario(horarioMiercolesUnicenter);
        horarioAtencionUnicenter.agregarHorario(horarioJuevesUnicenter);
        horarioAtencionUnicenter.agregarHorario(horarioViernesUnicenter);
        horarioAtencionUnicenter.agregarHorario(horarioSabadoUnicenter);
        horarioAtencionUnicenter.agregarHorario(horarioDomingoUnicenter);


        unicenter = new LocalComercialEntity();
        unicenter.setNombre("Unicenter");
        unicenter.setIcono("/local_comercial_icono.jpg");
        unicenter.setRangoCercania(0d);
        unicenter.setDireccion(direccionLocalUnicenter);
        unicenter.setRubro(rubroShopping);
        unicenter.agregarPalabraClave(palabraClaveShopping);
        unicenter.agregarPalabraClave(palabraClaveCompras);
        unicenter.setHorarioDeAtencion(horarioAtencionUnicenter);


        localDAO.save(burgerKing);
        localDAO.save(localKiosko);
        localDAO.save(unicenter);

        /*CREACION DE SUCURSALES BANCO*/
        
        DireccionEntity direccionSucursalHSBC = new DireccionEntity();
        direccionSucursalHSBC.setNumero(1049);
        direccionSucursalHSBC.setBarrio("Vicente Lopez");
        direccionSucursalHSBC.setCallePrincipal("Carlos Melo");
        direccionSucursalHSBC.setCodigoPostal(2346);
        direccionSucursalHSBC.setCoordX(-34.527079);
        direccionSucursalHSBC.setCoordY(-58.475222);
        direccionSucursalHSBC.setDepartamento("");
        direccionSucursalHSBC.setEntreCalle1("Gaspar Campos");
        direccionSucursalHSBC.setEntreCalle2("Dr. Eduardo Madero");
        direccionSucursalHSBC.setLocalidad("Vicente Lopez");
        direccionSucursalHSBC.setPais("Argentina");
        direccionSucursalHSBC.setPiso(0);
        direccionSucursalHSBC.setProvincia("Buenos Aires");
        direccionSucursalHSBC.setUnidad(0);

        ServicioEntity servicioAtencionAlCliente = new ServicioEntity();
        servicioAtencionAlCliente.setNombre("Atencion al cliente");

        ServicioEntity servicioCreditos = new ServicioEntity();
        servicioCreditos.setNombre("Créditos");

        bancoHSBC = new SucursalBancoEntity();
        bancoHSBC.setNombre("HSBC Vicente Lopez");
        bancoHSBC.setIcono("/sucursal_banco_icono.jpg");
        bancoHSBC.setDireccion(direccionSucursalHSBC);
        bancoHSBC.setRangoCercania(0.5); // Dejarlo siempre igual
        bancoHSBC.setHorarioDeAtencion(new HorarioDeAtencionMapper().mapToEntity(new SucursalBanco().getHorarioAtencion()));
        bancoHSBC.agregarServicio(servicioAtencionAlCliente);
        bancoHSBC.agregarServicio(servicioCreditos);
        
        
        
        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------
        
        
        
        DireccionEntity direccionBancoFrances = new DireccionEntity();
        direccionBancoFrances.setNumero(3200);
        direccionBancoFrances.setBarrio("Olivos");
        direccionBancoFrances.setCallePrincipal("Av.maipu");
        direccionBancoFrances.setCodigoPostal(1636);
        direccionBancoFrances.setCoordX(-100.527079);
        direccionBancoFrances.setCoordY(-200.475222);
        direccionBancoFrances.setDepartamento("");
        direccionBancoFrances.setEntreCalle1("Pelliza");
        direccionBancoFrances.setEntreCalle2("Roque saenz peña");
        direccionBancoFrances.setLocalidad("Vicente Lopez");
        direccionBancoFrances.setPais("Argentina");
        direccionBancoFrances.setPiso(0);
        direccionBancoFrances.setProvincia("Buenos Aires");
        direccionBancoFrances.setUnidad(0);

        ServicioEntity servicioCaja = new ServicioEntity();
        servicioCaja.setNombre("Trámites por caja");

        ServicioEntity servicioPromociones = new ServicioEntity();
        servicioPromociones.setNombre("Promociones");

        bancoFrances = new SucursalBancoEntity();
        bancoFrances.setNombre("Banco frances Olivos");
        bancoFrances.setIcono("/sucursal_banco_icono.jpg");
        bancoFrances.setDireccion(direccionBancoFrances);
        bancoFrances.setRangoCercania(0.5); // Dejarlo siempre igual
        bancoFrances.setHorarioDeAtencion(new HorarioDeAtencionMapper().mapToEntity(new SucursalBanco().getHorarioAtencion()));
        bancoFrances.agregarServicio(servicioCaja);
        bancoFrances.agregarServicio(servicioPromociones);
        
        
        
        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------
        
        
        
        DireccionEntity direccionBancoItau = new DireccionEntity();
        direccionBancoItau.setNumero(1049);
        direccionBancoItau.setBarrio("Martinez");
        direccionBancoItau.setCallePrincipal("Av.del libertador");
        direccionBancoItau.setCodigoPostal(1548);
        direccionBancoItau.setCoordX(-50.527079);
        direccionBancoItau.setCoordY(-95.475222);
        direccionBancoItau.setDepartamento("");
        direccionBancoItau.setEntreCalle1("Moreno");
        direccionBancoItau.setEntreCalle2("Diaz velez");
        direccionBancoItau.setLocalidad("San isidro");
        direccionBancoItau.setPais("Argentina");
        direccionBancoItau.setPiso(0);
        direccionBancoItau.setProvincia("Buenos Aires");
        direccionBancoItau.setUnidad(0);

        ServicioEntity servicioCanjePuntosLatam = new ServicioEntity();
        servicioCanjePuntosLatam.setNombre("Canje de puntos acumulados en tarjeta");

        ServicioEntity servicioCuentas = new ServicioEntity();
        servicioCuentas.setNombre("Gestion de cuentas");

        bancoItau = new SucursalBancoEntity();
        bancoItau.setNombre("ITAU");
        bancoItau.setIcono("/sucursal_banco_icono.jpg");
        bancoItau.setDireccion(direccionBancoItau);
        bancoItau.setRangoCercania(0.5); // Dejarlo siempre igual
        bancoItau.setHorarioDeAtencion(new HorarioDeAtencionMapper().mapToEntity(new SucursalBanco().getHorarioAtencion()));
        bancoItau.agregarServicio(servicioCanjePuntosLatam);
        bancoItau.agregarServicio(servicioCuentas);

      bancoDAO.save(bancoFrances);
      bancoDAO.save(bancoHSBC);
      bancoDAO.save(bancoItau);
        
    }

}
