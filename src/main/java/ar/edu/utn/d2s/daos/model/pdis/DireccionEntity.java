package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;

@Entity
@Table(name = "DIRECCION")
public class DireccionEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(name = "calle_principal", nullable = false)
	private String callePrincipal;
	@Column(name = "entre_calle_1")
	private String entreCalle1;
	@Column(name = "entre_calle_2")
	private String entreCalle2;
	@Column(name = "numero", nullable = false)
	private int numero;
	@Column(name = "piso")
	private Integer piso;
	@Column(name = "departamento")
	private String departamento;
	@Column(name = "unidad")
	private Integer unidad;
	@Column(name = "codigo_postal", nullable = false)
	private int codigoPostal;
	@Column(name = "localidad", nullable = false)
	private String localidad;
	@Column(name = "barrio", nullable = false)
	private String barrio;
	@Column(name = "provincia", nullable = false)
	private String provincia;
	@Column(name = "pais", nullable = false)
	private String pais;
	@Column(name = "coord_x", nullable = false)
	private double coordX;
	@Column(name = "coord_y", nullable = false)
	private double coordY;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCallePrincipal() {
		return callePrincipal;
	}

	public void setCallePrincipal(String callePrincipal) {
		this.callePrincipal = callePrincipal;
	}

	public String getEntreCalle1() {
		return entreCalle1;
	}

	public void setEntreCalle1(String entreCalle1) {
		this.entreCalle1 = entreCalle1;
	}

	public String getEntreCalle2() {
		return entreCalle2;
	}

	public void setEntreCalle2(String entreCalle2) {
		this.entreCalle2 = entreCalle2;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getPiso() {
		return piso;
	}

	public void setPiso(Integer piso) {
		this.piso = piso;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public int getUnidad() {
		return unidad;
	}

	public void setUnidad(Integer unidad) {
		this.unidad = unidad;
	}

	public int getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public double getCoordX() {
		return coordX;
	}

	public void setCoordX(double coordX) {
		this.coordX = coordX;
	}

	public double getCoordY() {
		return coordY;
	}

	public void setCoordY(double coordY) {
		this.coordY = coordY;
	}
}
