package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.ColectivoEntity;
import ar.edu.utn.d2s.daos.model.pdis.ParadaEntity;
import ar.edu.utn.d2s.pdis.Colectivo;
import ar.edu.utn.d2s.pdis.Parada;

import java.util.ArrayList;
import java.util.List;

public class ColectivoMapper implements Mapper<ColectivoEntity, Colectivo> {

    DireccionMapper direccionMapper = new DireccionMapper();
    ParadaMapper paradaMapper = new ParadaMapper();

    @Override
    public Colectivo mapFromEntity(ColectivoEntity entity) {
        Colectivo colectivo = new Colectivo();
        colectivo.setId(entity.getId());
        colectivo.setNombre(entity.getNombre());
        colectivo.setIcono(entity.getIcono());
        colectivo.setDireccion(direccionMapper.mapFromEntity(entity.getDireccion()));

        List<Parada> paradas = new ArrayList<>();
        entity.getParadas().forEach(paradaEntity -> paradas.add(paradaMapper.mapFromEntity(paradaEntity)));
        colectivo.setParadas(paradas);

        colectivo.setNroLinea(entity.getNroLinea());
        return colectivo;
    }

    @Override
    public ColectivoEntity mapToEntity(Colectivo colectivo) {
        ColectivoEntity entity = new ColectivoEntity();
        entity.setId(colectivo.getId());
        entity.setNombre(colectivo.getNombre());
        entity.setIcono(colectivo.getIcono());
        entity.setDireccion(direccionMapper.mapToEntity(colectivo.getDireccion()));

        List<ParadaEntity> paradas = new ArrayList<>();
        colectivo.getParadas().forEach(parada -> paradas.add(paradaMapper.mapToEntity(parada)));
        entity.setParadas(paradas);

        entity.setNroLinea(colectivo.getNroLinea());
        return entity;
    }
}
