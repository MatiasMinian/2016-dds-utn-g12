package ar.edu.utn.d2s.daos.impl;

import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.daos.mappers.SucursalBancoMapper;
import ar.edu.utn.d2s.daos.model.pdis.SucursalBancoEntity;
import ar.edu.utn.d2s.pdis.SucursalBanco;

public class SucursalBancoDao extends GenericDaoImpl<SucursalBancoEntity, Long> {

	private SucursalBancoMapper sucursalBancoMapper = new SucursalBancoMapper();

	public List<SucursalBanco> findAll() {
		return super.findAll(SucursalBancoEntity.class).stream().map(sucursalBancoEntity -> sucursalBancoMapper.mapFromEntity(sucursalBancoEntity)).collect(Collectors.toList());
	}
}
