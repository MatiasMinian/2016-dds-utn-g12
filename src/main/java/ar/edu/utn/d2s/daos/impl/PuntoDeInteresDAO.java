package ar.edu.utn.d2s.daos.impl;

import java.util.ArrayList;
import java.util.List;

import ar.edu.utn.d2s.daos.mappers.CGPMapper;
import ar.edu.utn.d2s.daos.mappers.ColectivoMapper;
import ar.edu.utn.d2s.daos.mappers.LocalComercialMapper;
import ar.edu.utn.d2s.daos.mappers.SucursalBancoMapper;
import ar.edu.utn.d2s.daos.model.pdis.PuntoDeInteresEntity;
import ar.edu.utn.d2s.pdis.*;

public class PuntoDeInteresDAO  extends GenericDaoImpl<PuntoDeInteresEntity, Long>  {

	CGPMapper cgpMapper = new CGPMapper();
	ColectivoMapper colectivoMapper = new ColectivoMapper();
	LocalComercialMapper localComercialMapper = new LocalComercialMapper();
	SucursalBancoMapper sucursalBancoMapper = new SucursalBancoMapper();
	
	public List<PuntoDeInteres> findAll() {

		CGPDao cgpDao = new CGPDao();
		ColectivoDao colectivoDao = new ColectivoDao();
		LocalComercialDao localComercialDao = new LocalComercialDao();
		SucursalBancoDao sucursalBancoDao = new SucursalBancoDao();

		List<PuntoDeInteres> puntosDeInteres = new ArrayList<>();

		puntosDeInteres.addAll(cgpDao.findAll());

		puntosDeInteres.addAll(colectivoDao.findAll());

		puntosDeInteres.addAll(localComercialDao.findAll());

		puntosDeInteres.addAll(sucursalBancoDao.findAll());



		return puntosDeInteres;
	}
}
