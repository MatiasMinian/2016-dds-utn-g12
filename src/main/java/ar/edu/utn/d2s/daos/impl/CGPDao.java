package ar.edu.utn.d2s.daos.impl;

import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.daos.mappers.CGPMapper;
import ar.edu.utn.d2s.daos.model.pdis.CGPEntity;
import ar.edu.utn.d2s.pdis.CGP;

public class CGPDao extends GenericDaoImpl<CGPEntity, Long> {

	private CGPMapper cgpMapper = new CGPMapper();

	public List<CGP> findAll() {
		return super.findAll(CGPEntity.class).stream().map(cgpEntity -> cgpMapper.mapFromEntity(cgpEntity)).collect(Collectors.toList());
	}
}
