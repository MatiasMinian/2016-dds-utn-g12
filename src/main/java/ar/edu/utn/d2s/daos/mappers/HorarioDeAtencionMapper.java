package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.HorarioDeAtencionEntity;
import ar.edu.utn.d2s.daos.model.pdis.HorarioEntity;
import ar.edu.utn.d2s.pdis.Horario;
import ar.edu.utn.d2s.pdis.HorarioAtencion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HorarioDeAtencionMapper implements Mapper<HorarioDeAtencionEntity, HorarioAtencion> {

    HorarioMapper horarioMapper = new HorarioMapper();

    @Override
    public HorarioAtencion mapFromEntity(HorarioDeAtencionEntity entity) {
        HorarioAtencion horarioAtencion = new HorarioAtencion();
        horarioAtencion.setId(entity.getId());
        horarioAtencion.setHorarios(entity.getHorarios().stream().map(horarioEntity -> horarioMapper.mapFromEntity(horarioEntity)).collect(Collectors.toList()));
        return horarioAtencion;
    }

    @Override
    public HorarioDeAtencionEntity mapToEntity(HorarioAtencion horarioAtencion) {
        HorarioDeAtencionEntity entity = new HorarioDeAtencionEntity();
        entity.setId(horarioAtencion.getId());

        horarioAtencion.getHorarios().forEach(horario -> entity.agregarHorario(horarioMapper.mapToEntity(horario))/*horarios.add(horarioMapper.mapToEntity(horario)*/);

        return entity;
    }
}
