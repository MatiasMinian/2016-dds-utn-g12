package ar.edu.utn.d2s.daos.mappers;

public interface Mapper<E, M> {

    public M mapFromEntity(E entity);

    public E mapToEntity(M model);
}
