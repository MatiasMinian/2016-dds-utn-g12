package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.PalabraClaveEntity;
import ar.edu.utn.d2s.pdis.PalabraClave;

public class PalabraClaveMapper implements Mapper<PalabraClaveEntity, PalabraClave> {

    @Override
    public PalabraClave mapFromEntity(PalabraClaveEntity entity) {
        return new PalabraClave(entity.getId(), entity.getPalabra());
    }

    @Override
    public PalabraClaveEntity mapToEntity(PalabraClave model) {
        PalabraClaveEntity palabraClave = new PalabraClaveEntity();
        palabraClave.setPalabra(model.getPalabra());
        palabraClave.setId(model.getId());
        palabraClave.setLocalComercialEntities(model.getLocalComercialEntities());

        return palabraClave;
    }
}
