package ar.edu.utn.d2s.daos.model.pdis;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "POINT")
public class PointEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "x", nullable = false, unique = true)
    private double x;

    @Column(name = "y", nullable = false, unique = true)
    private double y;

    @ManyToOne()
    @JoinColumn(name = "comuna_id", nullable = false)
    private ComunaEntity comuna;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public ComunaEntity getComuna() {
        return comuna;
    }

    public void setComuna(ComunaEntity comuna) {
        this.comuna = comuna;
    }
}
