package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.mappers.AdministradorMapper;
import ar.edu.utn.d2s.daos.model.AdministradorEntity;
import ar.edu.utn.d2s.usuarios.Administrador;

import java.util.List;
import java.util.stream.Collectors;

public class AdministradorDAO extends GenericDaoImpl<AdministradorEntity, Long> {

    private AdministradorMapper administradorMapper = new AdministradorMapper();

    public List<Administrador> findAll(){
        return findAll(AdministradorEntity.class).stream().map(administradorEntity -> administradorMapper.mapFromEntity(administradorEntity)).collect(Collectors.toList());

    }
}
