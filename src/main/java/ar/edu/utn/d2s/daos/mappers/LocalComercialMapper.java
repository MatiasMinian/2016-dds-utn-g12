package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.LocalComercialEntity;
import ar.edu.utn.d2s.daos.model.pdis.PalabraClaveEntity;
import ar.edu.utn.d2s.pdis.LocalComercial;
import ar.edu.utn.d2s.pdis.PalabraClave;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LocalComercialMapper implements Mapper<LocalComercialEntity, LocalComercial> {

    DireccionMapper direccionMapper = new DireccionMapper();
    HorarioDeAtencionMapper horarioDeAtencionMapper = new HorarioDeAtencionMapper();
    RubroMapper rubroMapper = new RubroMapper();
    PalabraClaveMapper palabraClaveMapper = new PalabraClaveMapper();

    @Override
    public LocalComercial mapFromEntity(LocalComercialEntity entity) {
        LocalComercial localComercial = new LocalComercial();
        localComercial.setId(entity.getId());
        localComercial.setNombre(entity.getNombre());
        localComercial.setIcono(entity.getIcono());
        localComercial.setDireccion(direccionMapper.mapFromEntity(entity.getDireccion()));
        localComercial.setHorarioAtencion(horarioDeAtencionMapper.mapFromEntity(entity.getHorarioDeAtencion()));
        localComercial.setRubro(rubroMapper.mapFromEntity(entity.getRubro()));

        List<PalabraClave> palabrasClave = new ArrayList<>();
        entity.getPalabrasClave().forEach(palabraClaveEntity -> palabrasClave.add(palabraClaveMapper.mapFromEntity(palabraClaveEntity)));
        localComercial.setPalabrasClave(palabrasClave);

        return localComercial;
    }

    @Override
    public LocalComercialEntity mapToEntity(LocalComercial localComercial) {
        LocalComercialEntity entity = new LocalComercialEntity();
        entity.setId(localComercial.getId());
        entity.setNombre(localComercial.getNombre());
        entity.setIcono(localComercial.getIcono());
        entity.setDireccion(direccionMapper.mapToEntity(localComercial.getDireccion()));
        entity.setHorarioDeAtencion(horarioDeAtencionMapper.mapToEntity(localComercial.getHorarioAtencion()));
        entity.setRubro(rubroMapper.mapToEntity(localComercial.getRubro()));

        Set<PalabraClaveEntity> palabrasClave = new HashSet<>();
        localComercial.getPalabrasClave().forEach(palabraClave -> palabrasClave.add(palabraClaveMapper.mapToEntity(palabraClave)));
        entity.setPalabrasClave(palabrasClave);

        return entity;
    }
}
