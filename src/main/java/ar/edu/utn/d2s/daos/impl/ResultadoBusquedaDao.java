package ar.edu.utn.d2s.daos.impl;

import ar.edu.utn.d2s.daos.mappers.ResultadoBusquedaMapper;
import ar.edu.utn.d2s.daos.model.pdis.ResultadoBusquedaEntity;
import ar.edu.utn.d2s.pdis.ResultadoBusqueda;

public class ResultadoBusquedaDao extends GenericDaoImpl<ResultadoBusquedaEntity, Long>  {

    private ResultadoBusquedaMapper resultadoBusquedaMapper = new ResultadoBusquedaMapper();

    public void save(ResultadoBusqueda resultadoBusqueda){
        save(resultadoBusquedaMapper.mapToEntity(resultadoBusqueda));
    }

}
