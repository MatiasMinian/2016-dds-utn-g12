package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "HORARIO")
public class HorarioEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "hora_comienzo", nullable = false)
    private Calendar horaComienzo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "hora_fin", nullable = false)
    private Calendar horaFin;

    @Column(name = "dia", nullable = false)
    private Integer dia;

    @ManyToOne()
    @JoinColumn(name = "horario_atencion_id", nullable = false)
    private HorarioDeAtencionEntity horarioDeAtencion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getHoraComienzo() {
        return horaComienzo;
    }

    public void setHoraComienzo(Calendar horaComienzo) {
        this.horaComienzo = horaComienzo;
    }

    public Calendar getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Calendar horaFin) {
        this.horaFin = horaFin;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public HorarioDeAtencionEntity getHorarioDeAtencion() {
        return horarioDeAtencion;
    }

    public void setHorarioDeAtencion(HorarioDeAtencionEntity horarioDeAtencion) {
        this.horarioDeAtencion = horarioDeAtencion;
    }
}