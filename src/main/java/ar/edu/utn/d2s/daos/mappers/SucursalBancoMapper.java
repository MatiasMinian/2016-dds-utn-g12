package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.ServicioEntity;
import ar.edu.utn.d2s.daos.model.pdis.SucursalBancoEntity;
import ar.edu.utn.d2s.pdis.Servicio;
import ar.edu.utn.d2s.pdis.SucursalBanco;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SucursalBancoMapper implements Mapper<SucursalBancoEntity, SucursalBanco> {

    DireccionMapper direccionMapper = new DireccionMapper();
    ServicioMapper servicioMapper = new ServicioMapper();
    HorarioDeAtencionMapper horarioDeAtencionMapper = new HorarioDeAtencionMapper();

    @Override
    public SucursalBanco mapFromEntity(SucursalBancoEntity entity) {
        SucursalBanco sucursalBanco = new SucursalBanco();
        sucursalBanco.setId(entity.getId());
        sucursalBanco.setNombre(entity.getNombre());
        sucursalBanco.setIcono(entity.getIcono());
        sucursalBanco.setDireccion(direccionMapper.mapFromEntity(entity.getDireccion()));
        sucursalBanco.setRangoDeCercania(entity.getRangoCercania());
        sucursalBanco.setServicios(entity.getServicios().stream().map(servicioEntity -> servicioMapper.mapFromEntity(servicioEntity)).collect(Collectors.toList()));
        sucursalBanco.setHorarioAtencion(horarioDeAtencionMapper.mapFromEntity(entity.getHorarioDeAtencion()));
        return sucursalBanco;
    }

    @Override
    public SucursalBancoEntity mapToEntity(SucursalBanco sucursalBanco) {
        SucursalBancoEntity entity = new SucursalBancoEntity();
        entity.setId(sucursalBanco.getId());
        entity.setNombre(sucursalBanco.getNombre());
        entity.setIcono(sucursalBanco.getIcono());
        entity.setDireccion(direccionMapper.mapToEntity(sucursalBanco.getDireccion()));
        entity.setRangoCercania(sucursalBanco.getRangoDeCercania());

        List<ServicioEntity> servicios = new ArrayList<>();
        sucursalBanco.getServicios().forEach(servicio -> servicios.add(servicioMapper.mapToEntity(servicio)));
        entity.setServicios(servicios);

        entity.setHorarioDeAtencion(horarioDeAtencionMapper.mapToEntity(sucursalBanco.getHorarioAtencion()));

        return entity;
    }
}
