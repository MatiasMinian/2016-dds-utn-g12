package ar.edu.utn.d2s.daos.model.pdis;

import ar.edu.utn.d2s.pdis.Servicio;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SUCURSAL_BANCO")
@PrimaryKeyJoinColumn(name="id")
public class SucursalBancoEntity extends PuntoDeInteresEntity {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "horario_atencion_id"/*, nullable = false*/)
    private HorarioDeAtencionEntity horarioDeAtencion;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sucursalBanco", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ServicioEntity> servicios = new ArrayList<>();

    public HorarioDeAtencionEntity getHorarioDeAtencion() {
        return horarioDeAtencion;
    }

    public void setHorarioDeAtencion(HorarioDeAtencionEntity horarioDeAtencion) {
        this.horarioDeAtencion = horarioDeAtencion;
    }

    public List<ServicioEntity> getServicios() {
        return servicios;
    }

    public void setServicios(List<ServicioEntity> servicios) {
        this.servicios = servicios;
    }

    public void agregarServicio(ServicioEntity servicio) {
        servicio.setSucursalBanco(this);
        servicio.setHorarioDeAtencion(this.getHorarioDeAtencion());
        servicios.add(servicio);
    }
}
