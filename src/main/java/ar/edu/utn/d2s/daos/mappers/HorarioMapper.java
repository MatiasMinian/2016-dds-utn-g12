package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.daos.model.pdis.HorarioEntity;
import ar.edu.utn.d2s.pdis.Horario;

public class HorarioMapper implements Mapper<HorarioEntity, Horario> {

    @Override
    public Horario mapFromEntity(HorarioEntity entity) {
        Horario horario = new Horario();
        horario.setId(entity.getId());
        horario.setHoraComienzo(entity.getHoraComienzo());
        horario.setHoraFin(entity.getHoraFin());
        horario.setDay(entity.getDia());
        return horario;
    }

    @Override
    public HorarioEntity mapToEntity(Horario horario) {
        HorarioEntity entity = new HorarioEntity();
        entity.setId(horario.getId());
        entity.setHoraComienzo(horario.getHoraComienzo());
        entity.setHoraFin(horario.getHoraFin());
        entity.setDia(horario.getDay());
        return entity;
    }
}
