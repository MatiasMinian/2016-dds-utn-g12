package ar.edu.utn.d2s.daos.model;

import ar.edu.utn.d2s.acciones.Accion;
import ar.edu.utn.d2s.daos.model.acciones.AccionEntity;
import ar.edu.utn.d2s.daos.model.pdis.ResultadoBusquedaEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TERMINAL")
@PrimaryKeyJoinColumn(name="id")
public class TerminalEntity extends UsuarioEntity {

    @Column(name = "guardar_resultados_busqueda", nullable = false)
    private boolean guardarResultadosBusqueda;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "terminal", orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<AccionEntity> acciones = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "terminal")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ResultadoBusquedaEntity> resultadoBusquedas = new ArrayList<>();

    @Column(name = "nombre", nullable = false)
    private String nombre;

    public TerminalEntity() {};

    public TerminalEntity(String username, String password, String mail, String salt, boolean guardarResultadosBusqueda, List<AccionEntity> acciones, List<ResultadoBusquedaEntity> resultadoBusquedas, String nombre) {
        super(username, password, mail, salt);
        this.guardarResultadosBusqueda = guardarResultadosBusqueda;
        this.acciones = acciones;
        this.resultadoBusquedas = resultadoBusquedas;
        this.nombre = nombre;
    }

    public List<AccionEntity> getAcciones() {
        return acciones;
    }

    public void setAcciones(List<AccionEntity> acciones) {
        this.acciones = acciones;
    }

    public boolean isGuardarResultadosBusqueda() {
        return guardarResultadosBusqueda;
    }

    public void setGuardarResultadosBusqueda(boolean guardarResultadosBusqueda) {
        this.guardarResultadosBusqueda = guardarResultadosBusqueda;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<ResultadoBusquedaEntity> getResultadoBusquedas() {
        return resultadoBusquedas;
    }

    public void setResultadoBusquedas(List<ResultadoBusquedaEntity> resultadoBusquedas) {
        this.resultadoBusquedas = resultadoBusquedas;
    }

    public void agregarAccion(AccionEntity accion) {
        accion.setTerminal(this);
        acciones.add(accion);
    }

    public void agregarResultadoBusqueda(ResultadoBusquedaEntity resultadoBusqueda) {
        resultadoBusqueda.setTerminal(this);
        resultadoBusquedas.add(resultadoBusqueda);
    }
}
