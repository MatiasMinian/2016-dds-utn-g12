package ar.edu.utn.d2s.daos.mappers;

import ar.edu.utn.d2s.acciones.Accion;
import ar.edu.utn.d2s.acciones.ReporteDeBusquedaPorFechaPorTerminal;
import ar.edu.utn.d2s.acciones.ReporteDeBusquedaPorTerminal;
import ar.edu.utn.d2s.daos.model.acciones.AccionEntity;


public class AccionMapper implements Mapper<AccionEntity,Accion> {

    private Accion accion;

    @Override
    public Accion mapFromEntity(AccionEntity entity) {
        if(entity.getId_Accion() == ReporteDeBusquedaPorFechaPorTerminal.idAccion)
            accion = new ReporteDeBusquedaPorFechaPorTerminal();
        if(entity.getId_Accion() == ReporteDeBusquedaPorTerminal.idAccion)
            accion = new ReporteDeBusquedaPorTerminal();

        accion.setId(entity.getId());

        return accion;
    }

    @Override
    public AccionEntity mapToEntity(Accion model) {

            AccionEntity accion = new AccionEntity();
            accion.setId(model.getId());
            accion.setId_Accion(model.getActionId());

            return accion;

    }
}
