package ar.edu.utn.d2s.daos.model.pdis;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CGP")
@PrimaryKeyJoinColumn(name="id")
public class CGPEntity extends PuntoDeInteresEntity {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "comuna_id", nullable = false)
    private ComunaEntity comuna;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "cgp")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<ServicioEntity> servicios = new ArrayList<>();

    public ComunaEntity getComuna() {
        return comuna;
    }

    public void setComuna(ComunaEntity comuna) {
        this.comuna = comuna;
    }

    public List<ServicioEntity> getServicios() {
        return servicios;
    }

    public void setServicios(List<ServicioEntity> servicios) {
        this.servicios = servicios;
    }

    public void agregarServicio(ServicioEntity servicio) {
        servicio.setCgp(this);
        servicios.add(servicio);
    }
}
