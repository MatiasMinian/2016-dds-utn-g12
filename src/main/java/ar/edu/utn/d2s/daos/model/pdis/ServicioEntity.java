package ar.edu.utn.d2s.daos.model.pdis;

import javax.persistence.*;

@Entity
@Table(name = "SERVICIO")
public class ServicioEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "horario_atencion_id")
    private HorarioDeAtencionEntity horarioDeAtencion;

    @ManyToOne()
    private SucursalBancoEntity sucursalBanco;

    @ManyToOne()
    private CGPEntity cgp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public HorarioDeAtencionEntity getHorarioDeAtencion() {
        return horarioDeAtencion;
    }

    public void setHorarioDeAtencion(HorarioDeAtencionEntity horarioDeAtencion) {
        this.horarioDeAtencion = horarioDeAtencion;
    }

    public SucursalBancoEntity getSucursalBanco() {
        return sucursalBanco;
    }

    public void setSucursalBanco(SucursalBancoEntity sucursalBanco) {
        this.sucursalBanco = sucursalBanco;
    }

    public CGPEntity getCgp() {
        return cgp;
    }

    public void setCgp(CGPEntity cgp) {
        this.cgp = cgp;
    }
}
