package ar.edu.utn.d2s.usuarios;

import java.util.List;

public class AdministradorDeUsuarios {
	
	private List<Usuario> usuarios;
	
	public void agregarUsuario(Usuario usuario) {
		usuarios.add(usuario);
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
}
