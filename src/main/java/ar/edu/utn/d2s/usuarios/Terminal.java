package ar.edu.utn.d2s.usuarios;
import java.util.List;

import ar.edu.utn.d2s.acciones.Accion;
import ar.edu.utn.d2s.pdis.ResultadoBusqueda;

public class Terminal extends Usuario {

	private boolean almacenamientoHabilitado;
    private String nombre;
	private List<Accion> acciones;
    private List<ResultadoBusqueda> resultadoBusquedas;
	public Terminal(String mail) {
		super(mail);
	}
	
	/* ---- Getters & Setters ---- */

	public boolean isAlmacenamientoHabilitado() {
		return almacenamientoHabilitado;
	}

	public void setAlmacenamientoHabilitado(boolean almacenamientoHabilitado) {
		this.almacenamientoHabilitado = almacenamientoHabilitado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

    public List<ResultadoBusqueda> getResultadoBusquedas() {
        return resultadoBusquedas;
    }

    public void setResultadoBusquedas(List<ResultadoBusqueda> resultadoBusquedas) {
        this.resultadoBusquedas = resultadoBusquedas;
    }

    public void agregarAccion(Accion accion) {
		acciones.add(accion);
	}

	public void agregarAcciones(List<Accion> acciones) {
		this.acciones.addAll(acciones);
	}

    public void setAcciones(List<Accion> acciones) {
        this.acciones = acciones;
    }

    public List<Accion> getAcciones(){
        return acciones;
    }

	
	/* --------------------------- */
	
	/* --------- Métodos --------- */

	public void almacenarResultadosDeBusqueda(ResultadoBusqueda resultadoBusqueda) {

        if(this.almacenamientoHabilitado){
            resultadoBusquedas.add(resultadoBusqueda);

		}
	}

	public void ejecutarAcciones (){
		acciones.forEach((Accion accion) -> accion.accionar(this));
	}


	/* --------------------------- */
}
