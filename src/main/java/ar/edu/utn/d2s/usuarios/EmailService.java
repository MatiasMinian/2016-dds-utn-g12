package ar.edu.utn.d2s.usuarios;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailService {
	
	private static EmailService instance = null;
	private static String emailFrom = "ddsutng12@gmail.com";
	private static String emailPassword = "ddsg12";
	private Session session;

	public static EmailService getInstance() {
		if (instance == null)
			instance = new EmailService();
		return instance;
	}
	
	// Privatizo constructor
	private EmailService() {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		
		session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailFrom, emailPassword);
			}
		});
	}
	
	private Message crearNuevoMensaje() throws AddressException, MessagingException{
		Message message = new MimeMessage(session);
    	message.setFrom(new InternetAddress(emailFrom));
    	
    	return message;
	}

	public void enviarMailDeBusqueda(String to, String busqueda, Integer duracion) {

		try {
	    	Message message = this.crearNuevoMensaje();
	    	message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	    	message.setSubject("Notificación de búsqueda exhaustiva");
	    	
	    	StringBuffer msgText = new StringBuffer();
	    	msgText.append("Busqueda: " + busqueda + ".\n");
	    	msgText.append("Duracion: " + duracion.toString());
	    	
	    	message.setText(msgText.toString());
	    	
	    	Transport.send(message);
	    	
		} catch (MessagingException msg) {
	    	System.out.println("Mail Exception: " + msg);
	    }
		
	}
	
	public void enviarMailErrorProceso(String to, String fallo) {

		try {
	    	Message message = this.crearNuevoMensaje();
	    	message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	    	message.setSubject("Notificación de Error Proceso");
	    	
	    	StringBuffer msgText = new StringBuffer();
	    	msgText.append("Fallo el proceso: " + fallo + ".\n");
	    	
	    	message.setText(msgText.toString());
	    	
	    	Transport.send(message);
	    	
		} catch (MessagingException msg) {
	    	System.out.println("Mail Exception: " + msg);
	    }
		
	}
}
