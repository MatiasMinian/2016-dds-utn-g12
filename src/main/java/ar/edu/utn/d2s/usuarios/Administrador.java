package ar.edu.utn.d2s.usuarios;

import ar.edu.utn.d2s.observadores.ObservadorDeBusquedasExhaustivas;

public class Administrador extends Usuario implements ObservadorDeBusquedasExhaustivas {

	private Long tiempoLimiteBusqueda;
	public Administrador(String mail) {
		super(mail);
	}

	/* ---- Getters & Setters ---- */



	/* --------------------------- */

	/* --------- Métodos --------- */
	
	@Override
	public void actualizar(String busqueda) {
		EmailService.getInstance().enviarMailDeBusqueda(mail, busqueda, 5);
	}

    public Long getTiempoLimiteBusqueda() {
        return tiempoLimiteBusqueda;
    }

    public void setTiempoLimiteBusqueda(Long tiempoLimiteBusqueda) {
        this.tiempoLimiteBusqueda = tiempoLimiteBusqueda;
    }

	/* --------------------------- */
}
