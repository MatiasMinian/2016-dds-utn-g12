package ar.edu.utn.d2s.usuarios;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ar.edu.utn.d2s.acciones.Accion;
import ar.edu.utn.d2s.procesos.AlFinalizarProceso;
import ar.edu.utn.d2s.procesos.Proceso;

public class Usuario {

    protected Long id;
	protected String mail;
	protected Proceso proceso;
	protected AlFinalizarProceso estrategiaEjecucionFinalizada;
    protected String password;
    protected String username;
    protected String salt;
	
	public Usuario(String mail) {
		this.mail = mail;
		this.setEstrategiaEjecucionVacia();
	}
	
	/* ---- Getters & Setters ---- */

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Proceso getProceso() {
		return proceso;
	}

	public void setProceso(Proceso proceso) {
		this.proceso = proceso;
	}
	
	public void setEstrategiaEjecucionVacia(){
		this.estrategiaEjecucionFinalizada = new AlFinalizarProceso() {
		    
		    public void correr(Boolean ejecucionExitosa) {
		    	super.correr(ejecucionExitosa);

		    }
		};
	}
	
	public void setEstrategiaEjecucionEnviarCorreo(){
		this.estrategiaEjecucionFinalizada = new AlFinalizarProceso() {
		    
		    public void correr(Boolean ejecucionExitosa) {
		    	super.correr(ejecucionExitosa);

		    	if(!ejecucionExitosa)
		    		EmailService.getInstance().enviarMailErrorProceso(mail, "Fallo el proceso");
		    }
		};
	}
	
	public void setEstrategiaEjecucionMultiplesIntentos(int intentosMaximos){
		
		this.estrategiaEjecucionFinalizada = new AlFinalizarProceso() {
		    private int cantidadDeIntentos = 1;

			
		    public void correr(Boolean ejecucionExitosa) {
		    	super.correr(ejecucionExitosa);
		    	
		    	if(!ejecucionExitosa && (cantidadDeIntentos < intentosMaximos)){
		    		cantidadDeIntentos++;
		    		
		    		proceso.ejecutar(this);
		    	}

		    }
		};
	}

	public void ejecutarProceso(){
		if(this.proceso != null){
			this.proceso.ejecutar(this.estrategiaEjecucionFinalizada);
		}
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
