package ar.edu.utn.d2s.servicioGobierno;

import java.util.HashMap;

public class ServicioGobiernoMock implements BajaPDISGobierno {

	private HashMap<String,String> listaBajas;
	
	public void inicializar(){
		
	listaBajas = new HashMap<String,String>();	
		
	}
	
	public HashMap<String,String> listaBajaPDIS(){

	this.listaBajas.put("19","01/08/2016");
	this.listaBajas.put("frances","02/08/2016");
					
		return listaBajas;
		
	}
	
}
