package ar.edu.utn.d2s.procesos;

import java.util.HashMap;
import java.util.Iterator;

import ar.edu.utn.d2s.Mapa;
import ar.edu.utn.d2s.pdis.PuntoDeInteres;
import ar.edu.utn.d2s.servicioGobierno.ServicioGobiernoMock;


public class BajaPDIS implements Proceso{

	private static BajaPDIS bajaPdis = null;
	private ServicioGobiernoMock servicioBajasGobierno;

	
	public static BajaPDIS getInstance() {
		if (bajaPdis == null)
			bajaPdis = new BajaPDIS();
		return bajaPdis;
	}

	public void ejecutar(AlFinalizarProceso alFinalizar){

		long tiempoComienzo = System.currentTimeMillis();	
    
	    new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				HashMap<String,String> listaDeBajas = servicioBajasGobierno.listaBajaPDIS();
				
				if(listaDeBajas.size() < 1){
					alFinalizar.correr(EJECUCION_FALLIDA);
					return;
				}
	
				Iterator<String> iteradorPuntos = listaDeBajas.keySet().iterator();
	
				while(iteradorPuntos.hasNext()){
	
					String valorBusqueda = iteradorPuntos.next();
	
					for(PuntoDeInteres unPunto : Mapa.getInstance().getPuntosDeInteres()){
	
						if(unPunto.verificaReferencia(valorBusqueda)){
	
							Mapa.getInstance().getPuntosDeInteres().remove(unPunto);
							
							break;
	
						}
	
					}
	
				}
				
				alFinalizar.correr(EJECUCION_EXITOSA);
				
			}
		}).start();
	
	    long tiempoFin = System.currentTimeMillis();
    
	    long tiempoProceso = (tiempoFin - tiempoComienzo)/1000;

	}
	
	public ServicioGobiernoMock getServicioBajasGobierno() {
		return servicioBajasGobierno;
	}

	public void setServicioBajasGobierno(ServicioGobiernoMock servicioBajasGobierno) {
		this.servicioBajasGobierno = servicioBajasGobierno;
	}
	
}
