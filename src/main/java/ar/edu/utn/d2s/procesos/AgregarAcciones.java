package ar.edu.utn.d2s.procesos;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.d2s.acciones.Accion;
import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.daos.mappers.TerminalMapper;
import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.usuarios.AdministradorDeUsuarios;
import ar.edu.utn.d2s.usuarios.Terminal;

public class AgregarAcciones extends Thread implements Proceso {

	private static AgregarAcciones agregarAcciones = null;

	private TerminalDAO terminalDAO = new TerminalDAO();
	private TerminalMapper terminalMapper = new TerminalMapper();

	private List<Accion> acciones = new ArrayList<>();

	public static AgregarAcciones getInstance() {
		if (agregarAcciones == null)
			agregarAcciones = new AgregarAcciones();
		return agregarAcciones;
	}
	
	public void limpiarAcciones(List<Accion> acciones) {
		this.acciones.clear();
	}
	
	public void setAcciones(List<Accion> acciones){
		this.acciones = acciones;
	}
	
	public void ejecutar(AlFinalizarProceso alFinalizar) {

		new Thread(new Runnable() {

			@Override
			public void run() {

				List<Terminal> terminales = terminalDAO.findAll(TerminalEntity.class).stream().map(terminalEntity -> terminalMapper.mapFromEntity(terminalEntity)).collect(Collectors.toList());

				terminales.forEach(terminal -> terminal.agregarAcciones(acciones));
				alFinalizar.correr(EJECUCION_EXITOSA);
			}
		});
	}

}
