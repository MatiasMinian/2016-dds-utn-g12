package ar.edu.utn.d2s.procesos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ar.edu.utn.d2s.Mapa;
import ar.edu.utn.d2s.pdis.*;

public class ActualizarLocalesComerciales implements Proceso {

	private static ActualizarLocalesComerciales actualizarLocalesComerciales = null;
	private String path = "C:\\test.txt";

	public static ActualizarLocalesComerciales getInstance() {
		if (actualizarLocalesComerciales == null)
			actualizarLocalesComerciales = new ActualizarLocalesComerciales();
		return actualizarLocalesComerciales;
	}
	
	// Privatizo constructor
	private ActualizarLocalesComerciales() {}
	
	
	/* Getters & Setters */
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	/* ----------------- */
	
	public void ejecutar(AlFinalizarProceso alFinalizar) {
		
		String line;
		
		try {
		    
			FileReader file = new FileReader(this.getPath());
		    BufferedReader buffer = new BufferedReader(file);
		    while( (line = buffer.readLine()) != null ) {
		    	System.out.println(line);
		    	
		    	this.procesarLinea(line);
		    	
		    }
		    buffer.close();
		    
		    alFinalizar.correr(EJECUCION_EXITOSA);
		
		} catch(Exception e) {
			e.printStackTrace();
			alFinalizar.correr(EJECUCION_FALLIDA);
		}
	}
	
	// Interpreta la línea y si tiene formato válido Actualiza el Local Comercial
	private boolean procesarLinea(String line) {
		
		boolean result = true;
		String localComercial;
		String[] palabrasClave;
		String[] lineArr = line.split(";");
		
		if(lineArr.length > 0) {
			localComercial = lineArr[0];
			palabrasClave = lineArr[1].split(" ");
			this.actualizarLocalComercial(localComercial, palabrasClave);
			result = false;
		}
		
		return result;
	}

	// Actualiza el Local Comercial: Si no existe lo crea, si existe lo actualiza
	private void actualizarLocalComercial(String localComercial, String[] palabrasClave) {
		
		Mapa mapa = Mapa.getInstance();
		LocalComercial nuevoLocalComercial;
		LocalComercial unLocalComercial;
		Rubro nuevoRubro;
		boolean exists = false;

		List<PalabraClave> palabrasClaveNuevas = new ArrayList<>();
		for (String palabraClave : palabrasClave) {
			palabrasClaveNuevas.add(new PalabraClave(null, palabraClave));
		}
		
		try {
			
			ResultadoBusqueda resultadoBusqueda = mapa.buscarPuntoSegun(localComercial);
			for (PuntoDeInteres pdi:resultadoBusqueda.getResultado()) {

				if(pdi.getNombre().equals(localComercial) && pdi.getClass().getSimpleName().equals("LocalComercial")) {
					unLocalComercial = (LocalComercial) pdi;

					unLocalComercial.setPalabrasClave(palabrasClaveNuevas);
					exists = true;
				}
			}
				


		} catch(Exception e) {
			
		}
		
		// Crea nuevo local comercial y su rubro con las palabras clave
		if(!exists) {
			nuevoRubro = new Rubro(null, localComercial, 0);
			nuevoLocalComercial = new LocalComercial(localComercial, "", null, null, nuevoRubro, palabrasClaveNuevas);
			mapa.altaDePunto(nuevoLocalComercial);
		}
	}
}
