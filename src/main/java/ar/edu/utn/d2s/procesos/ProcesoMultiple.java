package ar.edu.utn.d2s.procesos;

import java.util.List;

public class ProcesoMultiple extends Thread implements Proceso{

	private List<Proceso> procesos;
	
	public void agregarProceso(Proceso proceso){
		
		this.procesos.add(proceso);
		
	}
	
	public void ejecutar(AlFinalizarProceso alFinalizar){
		
		procesos.stream().forEach(proceso -> proceso.ejecutar(alFinalizar));
						
	}

	public void setProcesos(List<Proceso> procesos) {
		this.procesos = procesos;
	}
	
}
