package ar.edu.utn.d2s.procesos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ar.edu.utn.d2s.usuarios.Terminal;

public class AlFinalizarProceso {
    private static final Logger logger = LogManager.getLogger(Terminal.class);

	public void correr(Boolean esFinExitoso){
		if(esFinExitoso)
			logger.trace("Finalizo el proceso de manera exitosa");
		else
			logger.trace("Finalizo el proceso de manera fallida");
	
	}
}
