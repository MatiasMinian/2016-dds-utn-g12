package ar.edu.utn.d2s.procesos;

public interface Proceso {
	
	  static public final boolean EJECUCION_EXITOSA  = true;
	  static public final boolean EJECUCION_FALLIDA  = false;
	
	public void ejecutar(AlFinalizarProceso alFinalizar);

}
