package ar.edu.utn.d2s.AplicacionWebUtil;

public class Path {

    public static class Web {
        public static final String INDEX = "/";
        public static final String LOGIN = "/login/";
        public static final String LOGOUT = "/logout/";
		public static final String BUSCAR = "/buscar/";
		public static final String ACCIONES = "/acciones/";
		public static final String ACCION = "/accion/:id/";
		public static final String HISTORIAL = "/historial/";
		public static final String HISTORIALBUSQUEDA = "/historialBusqueda/";
        public static final String PDIS = "/pdis";
        
        public static String getINDEX() {
			return INDEX;
		}
		public static String getLOGIN() {
			return LOGIN;
		}
		public static String getLOGOUT() {
			return LOGOUT;
		}
		public static String getBUSCAR() {
			return BUSCAR;
		}
		 public static String getPDIS() {
				return PDIS;
		}
		public static String getACCIONES() {
			return ACCIONES;
		}
		public static String getHISTORIAL() {
			return HISTORIAL;
		}
		public static String getBUSCARHISTORIAL() {
			return HISTORIALBUSQUEDA;
		}
    }

    public static class Template {
        public final static String INDEX = "/velocity/index/index.vm";
        public final static String LOGIN = "/velocity/login/login.vm";
        public final static String BUSCAR = "/velocity/busqueda.vm";
        public static final String NOT_FOUND = "/velocity/notFound.vm";
        public static final String ACCIONES = "/velocity/acciones.vm";
        public static final String HISTORIAL = "/velocity/historial.vm";
    }

}
