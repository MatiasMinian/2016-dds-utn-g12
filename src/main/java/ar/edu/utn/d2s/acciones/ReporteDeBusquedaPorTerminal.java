package ar.edu.utn.d2s.acciones;

import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.daos.mappers.TerminalMapper;
import ar.edu.utn.d2s.daos.model.TerminalEntity;
import ar.edu.utn.d2s.pdis.ResultadoBusqueda;
import ar.edu.utn.d2s.usuarios.Terminal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ReporteDeBusquedaPorTerminal implements Accion {

    public static Integer idAccion = 2;
    public static String nombre = "Reporte de búsqueda por Terminal";
    private Long id;
    private TerminalDAO terminalDAO = new TerminalDAO();
    private TerminalMapper terminalMapper = new TerminalMapper();


    @Override
    public void accionar(Terminal terminal) {
        List<Terminal> terminales = terminalDAO.findAll(TerminalEntity.class).stream().map(terminalEntity -> terminalMapper.mapFromEntity(terminalEntity)).collect(Collectors.toList());
        Map<Terminal, Integer> resultadosPerTerminal = terminales.stream().collect(Collectors.toMap(Function.identity(), terminal1 -> terminal1.getResultadoBusquedas().stream().collect(Collectors.summingInt(ResultadoBusqueda::getCantidadDeResultados))));
        //Map<Terminal, Long> resultadosPorTerminal = terminales.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Logger logger = LogManager.getLogger(this.getClass());
        logger.info("User, Total Results");
        resultadosPerTerminal.forEach(((usuarioTerminal, resultados) -> logger.info(usuarioTerminal.getNombre()+", "+ resultados.toString())));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNombre() {
    	return ReporteDeBusquedaPorTerminal.nombre;
    }

    @Override
    public Integer getActionId() {
        return ReporteDeBusquedaPorTerminal.idAccion;
    }
}
