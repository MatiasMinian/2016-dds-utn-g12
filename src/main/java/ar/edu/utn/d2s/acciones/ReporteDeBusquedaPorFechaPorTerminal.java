package ar.edu.utn.d2s.acciones;


import ar.edu.utn.d2s.pdis.ResultadoBusqueda;
import ar.edu.utn.d2s.usuarios.Terminal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;
import java.util.Map;
import java.util.stream.Collectors;

public class ReporteDeBusquedaPorFechaPorTerminal implements Accion {

    public static Integer idAccion = 1;
    public static String nombre = "Reporte de búsqueda por fecha por Terminal";
    private Long id;
    @Override
    public void accionar(Terminal terminal) {

        Map<Calendar, Long> resultadosPorFecha = terminal.getResultadoBusquedas().stream().collect(Collectors.groupingBy(ResultadoBusqueda::getFecha, Collectors.counting()));

        Logger logger = LogManager.getLogger(this.getClass());
        logger.info("Date, Number of Results");
        resultadosPorFecha.forEach((calendar, cantidadResultados) -> logger.info(calendar.getTime().toString()+", "+cantidadResultados.toString()));
    }

    @Override
    public String getNombre() {
    	return ReporteDeBusquedaPorFechaPorTerminal.nombre;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getActionId() {
        return ReporteDeBusquedaPorFechaPorTerminal.idAccion;
    }
}
