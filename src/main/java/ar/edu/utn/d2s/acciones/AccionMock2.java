package ar.edu.utn.d2s.acciones;

import ar.edu.utn.d2s.usuarios.Terminal;

public class AccionMock2 implements Accion {

	public String nombre;

	@Override
	public void accionar(Terminal terminal) {
		System.out.println("accionando accion 2");
		
	}

	@Override
	public Long getId() {
		return null;
	}

	@Override
	public void setId(Long id) {

	}

	@Override
	public Integer getActionId() {
		return null;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
