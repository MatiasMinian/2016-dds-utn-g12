package ar.edu.utn.d2s.acciones;

import ar.edu.utn.d2s.usuarios.Terminal;

public interface Accion {
	 void accionar(Terminal terminal);
     Long getId();
     void setId(Long id);
     Integer getActionId();
     String getNombre();
}
