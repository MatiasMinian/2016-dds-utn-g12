package ar.edu.utn.d2s;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ar.edu.utn.d2s.cache.CacheManager;
import ar.edu.utn.d2s.daos.impl.PuntoDeInteresDAO;
import ar.edu.utn.d2s.daos.impl.TerminalDAO;
import ar.edu.utn.d2s.observadores.ObservadorDeBusquedasExhaustivas;
import ar.edu.utn.d2s.pdis.CGP;
import ar.edu.utn.d2s.pdis.PuntoDeInteres;
import ar.edu.utn.d2s.pdis.ResultadoBusqueda;
import ar.edu.utn.d2s.pdis.SucursalBanco;
import ar.edu.utn.d2s.serviciosExternos.InterpreteServiciosExternos;
import ar.edu.utn.d2s.serviciosExternos.CGP.ConsultorCGPs;
import ar.edu.utn.d2s.serviciosExternos.CGP.ConsultorCGPsMock;
import ar.edu.utn.d2s.serviciosExternos.banco.ConsultorBancos;
import ar.edu.utn.d2s.serviciosExternos.banco.ConsultorBancosMock;
import ar.edu.utn.d2s.usuarios.Terminal;
import ar.edu.utn.d2s.utils.StringUtil;

public class Mapa {

    private static Mapa mapa = null;

    private PuntoDeInteresDAO puntoDeInteresDao = new PuntoDeInteresDAO();
    private TerminalDAO terminalDAO = new TerminalDAO();
    private List<PuntoDeInteres> puntosDeInteres;
    private InterpreteServiciosExternos interprete;
    private ConsultorBancos consultorBancos;
    private ConsultorCGPs consultorCGPs;
    private List<Terminal> terminales;
    private List<ObservadorDeBusquedasExhaustivas> observadoresDeBusquedasExhaustivas;
    private long tiempoNotificacionBusquedasExhaustivas;
    private static final Logger logger = LogManager.getLogger(Mapa.class.getName());


    private Mapa() {

    }

	/* ---- Getters & Setters ---- */

    public static Mapa getInstance() {
        if (mapa == null) {
            mapa = new Mapa();
            mapa.inicializar();
        }
        return mapa;
    }

    public List<PuntoDeInteres> getPuntosDeInteres() {
        return this.puntosDeInteres;
    }

    public void setPuntosDeInteres(List<PuntoDeInteres> puntosDeInteres) {
        this.puntosDeInteres = puntosDeInteres;
    }

    public InterpreteServiciosExternos getInterprete() {
        return interprete;
    }

    public void setInterprete(InterpreteServiciosExternos interprete) {
        this.interprete = interprete;
    }

    public ConsultorBancos getConsultorBancos() {
        return consultorBancos;
    }

    public void setConsultorBancos(ConsultorBancos consultorBancos) {
        this.consultorBancos = consultorBancos;
    }

    public ConsultorCGPs getConsultorCGPs() {
        return consultorCGPs;
    }

    public void setConsultorCGPs(ConsultorCGPs consultorCGPs) {
        this.consultorCGPs = consultorCGPs;
    }

    public List<Terminal> getTerminales() {
        return terminales;
    }

    public void setTerminales(List<Terminal> terminales) {
        this.terminales = terminales;
    }

    public List<ObservadorDeBusquedasExhaustivas> getObservadoresDeBusquedasExhaustivas() {
        return observadoresDeBusquedasExhaustivas;
    }

    public void setObservadoresDeBusquedasExhaustivas(
            List<ObservadorDeBusquedasExhaustivas> observadoresDeBusquedasExhaustivas) {
        this.observadoresDeBusquedasExhaustivas = observadoresDeBusquedasExhaustivas;
    }

    public long getTiempoNotificacionBusquedasExhaustivas() {
        return this.tiempoNotificacionBusquedasExhaustivas;
    }

    public void setTiempoNotificacionBusquedasExhaustivas(long tiempoNotificacionBusquedasExhaustivas) {
        this.tiempoNotificacionBusquedasExhaustivas = tiempoNotificacionBusquedasExhaustivas;
    }


	/* --------------------------- */

	/* --------- Métodos --------- */

    public void inicializar() {

        this.setPuntosDeInteres(puntoDeInteresDao.findAll());
        this.setInterprete(new InterpreteServiciosExternos());
        this.setConsultorBancos(new ConsultorBancosMock());
        this.setConsultorCGPs(new ConsultorCGPsMock());
        this.setTerminales(terminalDAO.findAll());
        this.setObservadoresDeBusquedasExhaustivas(new ArrayList<>());
        this.setTiempoNotificacionBusquedasExhaustivas(3);
    }

    public ResultadoBusqueda buscarPuntoSegun(String texto) throws Exception {

        ResultadoBusqueda resultadoBusqueda = new ResultadoBusqueda();
        Set<PuntoDeInteres> resultado;
        LocalDateTime comienzoBusqueda = LocalDateTime.now();
        LocalDateTime finBusqueda;
        long segundosBusqueda;

        if (texto.isEmpty()) {
            throw new IllegalArgumentException("El campo texto no puede ser vacío");
        }

        resultado = CacheManager.getInstance().matchSearch(texto);

        if (resultado != null){
            finBusqueda = LocalDateTime.now();
            segundosBusqueda = Duration.between(comienzoBusqueda, finBusqueda).getSeconds();
        }else{

            // No esta en la cache
            resultado = new HashSet<>();
            String[] palabras = StringUtil.getUniqueWordsFromText(texto);

            for (String palabra : palabras) {
                resultado.addAll(this.puntosDeInteres.stream()
                        .filter(puntosDeInteres -> puntosDeInteres.verificaReferencia(palabra))
                        .collect(Collectors.toSet()));
                resultado.addAll(consultarCGPexternos(palabra));
            }

            int palabrasLenght = palabras.length;

            if (palabrasLenght > 1) {
                List<String> todasLasOpciones = getTodasLasOpciones(palabras);
                for (String palabra : todasLasOpciones) {
                    for (String palabra2 : todasLasOpciones) {
                        resultado.addAll(consultarBancosExternos(palabra,palabra2));
                    }
                }
            }


            finBusqueda = LocalDateTime.now();
            segundosBusqueda = Duration.between(comienzoBusqueda, finBusqueda).getSeconds();



        }

        if (segundosBusqueda >= this.tiempoNotificacionBusquedasExhaustivas){
            this.notificarBusquedaExhaustivaAObservadores(texto);
        }

        int cantidadResultados = resultado.size();

        CacheManager.getInstance().addSearch(texto, resultado);

       // this.notificarBusquedaATerminales(resultadoBusqueda);
        logger.info("Palabra buscada: \"{}\" | Cant. resultados: {} | Tiempo de respuesta: {}", texto, resultado.size(), segundosBusqueda);

        resultadoBusqueda.setCantidadDeResultados(cantidadResultados);
        resultadoBusqueda.setFecha(Calendar.getInstance());
        resultadoBusqueda.setTexto(texto);
        resultadoBusqueda.setTiempoBusqueda(segundosBusqueda);
        resultadoBusqueda.setResultado(resultado);

        return resultadoBusqueda;
    }

    public void altaDePunto(PuntoDeInteres punto) {
        this.puntosDeInteres.add(punto);

    }

    public void bajaDePunto(long ID) throws Exception {
        if (!this.puntosDeInteres.removeIf(punto -> punto.getId() == ID))
            throw new Exception("Punto no esta en el mapa");
    }

    public PuntoDeInteres obtenerPunto(long ID) throws IllegalStateException {

        List<PuntoDeInteres> resultado = this.puntosDeInteres.stream().filter(punto -> punto.getId() == ID).collect(Collectors.toList());

        if (resultado.size() != 1) {
            throw new IllegalStateException();
        }

        return resultado.get(0);
    }

    public void modificacionDePunto(long ID, PuntoDeInteres punto) throws Exception {
        this.bajaDePunto(ID);
        this.altaDePunto(punto);
    }

    private List<SucursalBanco> consultarBancosExternos(String nombre, String servicio) {

        return interprete.interpretarBancos(this.consultorBancos.consultarBancos(nombre, servicio));
    }

    private List<CGP> consultarCGPexternos(String texto) {

        return interprete.interpretarCGPs(this.consultorCGPs.consultarCGPs(texto));
    }

    private void notificarBusquedaExhaustivaAObservadores(String busqueda) {
        observadoresDeBusquedasExhaustivas.forEach(observador -> observador.actualizar(busqueda));
    }

    public List<String> getTodasLasOpciones(String[] palabras){
        List<String> resultado = new ArrayList<>();
        String opcion = "";
        for (int i = 0; i < palabras.length; i++) {
            resultado.add(palabras[i]);
            for (int j = i +1; j < palabras.length; j++) {
                opcion = palabras[i];
                for (int k = i+1; k <= j; k++) {
                    opcion = opcion + " " + palabras[k];
                }
                resultado.add(opcion);
            }
        }
        return resultado;
    }




/* TODO esto es realmente asi? o la idea es que cada uno decida si guarda sus propias busquedas?
    public void notificarBusquedaATerminales(ResultadoBusqueda resultadoBusqueda) {
        terminales.forEach(terminal -> terminal.almacenarResultadosDeBusqueda(resultadoBusqueda));
    }
*/
	/* --------------------------- */
}
