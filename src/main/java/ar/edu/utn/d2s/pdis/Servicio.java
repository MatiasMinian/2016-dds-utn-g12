package ar.edu.utn.d2s.pdis;

import java.util.Calendar;

public class Servicio {

	private Long id;
	private HorarioAtencion horarioAtencion;
	private String nombre = "";

	public Servicio() {};

	public Servicio(HorarioAtencion horario, String nombre) {
		this.horarioAtencion = horario;
		this.nombre = nombre;
	}
	
	/* ---- Getters & Setters ---- */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public HorarioAtencion getHorarioAtencion() {
		return horarioAtencion;
	}

	public void setHorarioAtencion(HorarioAtencion horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/* --------------------------- */
	
	/* --------- Métodos --------- */
	
	public Boolean estaDisponible(Calendar momento){
		return horarioAtencion.esHorarioAtencion(momento);
	}

	/* --------------------------- */
}
