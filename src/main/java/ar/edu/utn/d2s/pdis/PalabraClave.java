package ar.edu.utn.d2s.pdis;

import ar.edu.utn.d2s.daos.model.pdis.LocalComercialEntity;

import java.util.ArrayList;
import java.util.List;

public class PalabraClave {

    private Long id;
    private String palabra;
    private List<LocalComercialEntity> localComercialEntities = new ArrayList<>();
    public PalabraClave(Long id, String palabra) {
        this.id = id;
        this.palabra = palabra;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public List<LocalComercialEntity> getLocalComercialEntities() {
        return localComercialEntities;
    }

    public void setLocalComercialEntities(List<LocalComercialEntity> localComercialEntities) {
        this.localComercialEntities = localComercialEntities;
    }
}
