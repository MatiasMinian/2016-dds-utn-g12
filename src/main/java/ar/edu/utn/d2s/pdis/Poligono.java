package ar.edu.utn.d2s.pdis;

import org.uqbar.geodds.Point;
import org.uqbar.geodds.Polygon;

import java.util.ArrayList;
import java.util.List;

public class Poligono {

    private List<Point> surface = new ArrayList<>();
    private Polygon polygon = new Polygon();

    public Poligono() {};

    public Poligono(List<Point> surface) {
        this.surface.addAll(surface);
        this.polygon = new Polygon(surface);
    }

    public boolean isInside(Point point) {
        return polygon.isInside(point);
    }

    public boolean add(Point point) {
        surface.add(point);
        return polygon.add(point);
    }

    public List<Point> getSurface() {
        return surface;
    }

    public void setSurface(List<Point> surface) {
        this.surface = surface;
    }
}
