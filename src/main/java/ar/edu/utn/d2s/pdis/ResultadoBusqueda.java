package ar.edu.utn.d2s.pdis;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;

public class ResultadoBusqueda {

    private Long id;
    private String texto;
    private int cantidadDeResultados;
    private long tiempoBusqueda;
    private Calendar fecha;
    private Set<PuntoDeInteres> resultado;

    public ResultadoBusqueda() {};

    public ResultadoBusqueda(String texto, int cantidadDeResultados, long tiempoBusqueda, Calendar fecha) {
        this.id = id;
        this.texto = texto;
        this.cantidadDeResultados = cantidadDeResultados;
        this.tiempoBusqueda = tiempoBusqueda;
        this.fecha = fecha;
    }

    //-------------------- GETTERS Y SETTERS -----------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getCantidadDeResultados() {
        return cantidadDeResultados;
    }

    public void setCantidadDeResultados(int cantidadDeResultados) {
        this.cantidadDeResultados = cantidadDeResultados;
    }

    public long getTiempoBusqueda() {
        return tiempoBusqueda;
    }

    public void setTiempoBusqueda(long tiempoBusqueda) {
        this.tiempoBusqueda = tiempoBusqueda;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }
    
    public String getFormattedDate () {
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    	return format.format(fecha.getTime());
    }

    public Set<PuntoDeInteres> getResultado() {
        return resultado;
    }

    public void setResultado(Set<PuntoDeInteres> resultado) {
        this.resultado = resultado;
    }

    // ------------------ Metodos----------------------------------




}
