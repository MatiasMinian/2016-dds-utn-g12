package ar.edu.utn.d2s.pdis;

import jdk.nashorn.internal.ir.IfNode;

import java.util.List;

import java.util.Calendar;
import java.util.Map;
import java.util.Optional;

public class HorarioAtencion {

	private Long id;
	private List<Horario> horarios;

	public HorarioAtencion() {};

	public HorarioAtencion(List<Horario> horarios) {
		this.horarios = horarios;
	}

	/* ---- Getters & Setters ---- */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	/* --------------------------- */

	/* --------- Métodos --------- */

	public boolean esHorarioAtencion(Calendar horario) {

		return horarios.stream().anyMatch(miHorario -> (miHorario.getDay() == horario.get(Calendar.DAY_OF_WEEK)) &&
				(horario.get(Calendar.HOUR_OF_DAY) >= miHorario.getHoraComienzo().get(Calendar.HOUR_OF_DAY)) && (horario.get(Calendar.HOUR_OF_DAY) <= miHorario.getHoraFin().get(Calendar.HOUR_OF_DAY)));
	}
}
