package ar.edu.utn.d2s.pdis;
import java.util.List;

public class Rubro {

	private Long id;
	private String nombre;
	private double rangoDeCercania;
	
	public Rubro(Long id, String nombre, double rangoDeCercania) {
		this.id = id;
		this.nombre = nombre;
		this.rangoDeCercania = rangoDeCercania;
	}
	
	/* ---- Getters & Setters ---- */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getRangoDeCercania() {
		return rangoDeCercania;
	}

	public void setRangoDeCercania(double rangoDeCercania) {
		this.rangoDeCercania = rangoDeCercania;
	}
	
	/* --------------------------- */
	
	/* --------- Métodos --------- */
	
	public boolean referenciaAlRubro(String texto){
		return this.nombre.equals(texto);
	}
	/* --------------------------- */	
}

