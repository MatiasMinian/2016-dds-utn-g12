package ar.edu.utn.d2s.pdis;

import org.uqbar.geodds.Polygon;

public class Comuna {

	private Long id;
	private int numero;
	private Poligono area;

	public Comuna() {};

	public Comuna(int numero, Poligono area) {
		this.numero = numero;
		this.area = area;
	}
	
	/* ---- Getters & Setters ---- */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Poligono getArea() {
		return area;
	}

	public void setArea(Poligono area) {
		this.area = area;
	}
	
	/* --------------------------- */
	
	/* --------- Métodos --------- */
	
	
	
	/* --------------------------- */
}
