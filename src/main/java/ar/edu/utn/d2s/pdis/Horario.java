package ar.edu.utn.d2s.pdis;

import java.util.Calendar;

public class Horario {

    private Long id;
    private Calendar horaComienzo;
    private Calendar horaFin;
    private Integer day;

    public Horario() {};

    public Horario(Calendar horaComienzo, Calendar horaFin, Integer day) {
        this.horaComienzo = horaComienzo;
        this.horaFin = horaFin;
        this.day = day;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getHoraComienzo() {
        return horaComienzo;
    }

    public void setHoraComienzo(Calendar horaComienzo) {
        this.horaComienzo = horaComienzo;
    }

    public Calendar getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Calendar horaFin) {
        this.horaFin = horaFin;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}