package ar.edu.utn.d2s.pdis;

import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import org.uqbar.geodds.Point;

public class Colectivo extends PuntoDeInteres {

	private int nroLinea;
	private List<Parada> paradas;

	public Colectivo() {};

	public Colectivo(String nombre, String icono, Direccion direccion, List<Parada> paradas,int nroLinea) {
		super(nombre, icono, direccion);
		this.setRangoDeCercania(0.1);
		this.paradas = paradas != null ? paradas : new ArrayList<Parada>();
		this.nroLinea = nroLinea;
	}	
	
	/* ---- Getters & Setters ---- */

	public List<Parada> getParadas() {
		return paradas;
	}

	public int getNroLinea() {
		return nroLinea;
	}

	public void setNroLinea(int nroLinea) {
		this.nroLinea = nroLinea;
	}

	public void setParadas(List<Parada> paradas) {
		this.paradas = paradas;
	}
	
	/* --------------------------- */

	/* --------- Métodos --------- */

	public boolean estaCerca(Point punto) {
		return paradas.stream()
				.anyMatch(parada -> parada.getDireccion().getPunto().distance(punto) < this.rangoDeCercania);
	}
	
	public boolean estaDisponible(Calendar momento, String servicio) {
		return true;
	}
	
	public boolean verificaReferencia(String texto){
		
		return texto.equals(String.valueOf(this.nroLinea));
	}
	
	public void agregarParada(Parada unaParada) {
		paradas.add(unaParada);
	}
	
	/* --------------------------- */
}
