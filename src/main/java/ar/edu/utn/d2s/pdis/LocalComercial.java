package ar.edu.utn.d2s.pdis;

import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;

import org.uqbar.geodds.Point;

public class LocalComercial extends PuntoDeInteres {

	private Rubro rubro;
	private HorarioAtencion horarioAtencion;
	private List<PalabraClave> palabrasClave = new ArrayList<>();

	public LocalComercial() {};
	
	public LocalComercial(String nombre, String icono, Direccion direccion, HorarioAtencion horariosDisponibles,
						  Rubro rubro, List<PalabraClave> palabrasClave) {

		super(nombre, icono, direccion);
		this.rubro = rubro;
		this.horarioAtencion = horariosDisponibles;
		if (palabrasClave != null) {
			this.palabrasClave.addAll(palabrasClave);
		}
	}

	/* ---- Getters & Setters ---- */

	public double getRangoCercania() {
		return this.rubro.getRangoDeCercania();
	}

	public void setRangoCercania(double rangoCercania) {
		this.rubro.setRangoDeCercania(rangoCercania);
	}

	public Rubro getRubro() {
		return rubro;
	}

	public void setRubro(Rubro rubro) {
		this.rubro = rubro;
	}
	public HorarioAtencion getHorarioAtencion() {
		return horarioAtencion;
	}

	public void setHorarioAtencion(HorarioAtencion horarioAtencion) {
		this.horarioAtencion = horarioAtencion;
	}

	public List<PalabraClave> getPalabrasClave() {
		return palabrasClave;
	}

	public void setPalabrasClave(List<PalabraClave> palabrasClave) {
		this.palabrasClave = palabrasClave;
	}

	/* --------------------------- */

	/* --------- Métodos --------- */

	public boolean estaCerca(Point punto){			
		return direccion.getPunto().distance(punto) < this.rubro.getRangoDeCercania();
	}
	
	public boolean verificaReferencia(String texto) {

		return this.nombre.startsWith(texto) || this.rubro.referenciaAlRubro(texto) || this.estaDentroDePalabrasClave(texto);
	}
	
	public boolean estaDisponible(Calendar momento) {
		return horarioAtencion.esHorarioAtencion(momento);
	}
	
	public boolean estaDentroDePalabrasClave(String texto) {
		return this.getPalabrasClave().stream().anyMatch(palabraClave->palabraClave.getPalabra().equalsIgnoreCase(texto));
	}
}