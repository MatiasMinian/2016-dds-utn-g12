package ar.edu.utn.d2s.pdis;

import java.io.File;
import java.util.*;

import org.uqbar.geodds.Point;

public class SucursalBanco extends PuntoDeInteres {

	private List<Servicio> servicios;
	private HorarioAtencion HORARIO_ATENCION;

	public SucursalBanco() {
        inicializarHorarios();
	};

	public SucursalBanco(String nombre, String icono, Direccion direccion, List<Servicio> servicios) {
		super(nombre, icono, direccion);
		this.servicios = servicios;
		inicializarHorarios();
	}

	/* ---- Getters & Setters ---- */

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public HorarioAtencion getHorarioAtencion() {
		return HORARIO_ATENCION;
	}

	public void setHorarioAtencion(HorarioAtencion HORARIO_ATENCION) {
		this.HORARIO_ATENCION = HORARIO_ATENCION;
	}


	/* --------------------------- */

	/* --------- Métodos --------- */
	
	public boolean estaDisponible(Calendar momento, String servicio) {
		return HORARIO_ATENCION.esHorarioAtencion(momento);
	}

	public boolean verificaReferencia(String texto) {

		return nombre.startsWith(texto);
	}

	public void inicializarHorarios(){

		Calendar horaComienzo = Calendar.getInstance();
		horaComienzo.set(Calendar.HOUR_OF_DAY, 10);
		horaComienzo.set(Calendar.MINUTE, 0);
		horaComienzo.set(Calendar.SECOND, 0);

		Calendar horaFin = Calendar.getInstance();
		horaFin.set(Calendar.HOUR_OF_DAY, 15);
		horaFin.set(Calendar.MINUTE, 0);
		horaFin.set(Calendar.SECOND, 0);

		Horario horarioLunes = new Horario();
		horarioLunes.setDay(2);
		horarioLunes.setHoraComienzo(horaComienzo);
		horarioLunes.setHoraFin(horaFin);

		Horario horarioMartes = new Horario();
		horarioMartes.setDay(3);
		horarioMartes.setHoraComienzo(horaComienzo);
		horarioMartes.setHoraFin(horaFin);

		Horario horarioMiercoles = new Horario();
		horarioMiercoles.setDay(4);
		horarioMiercoles.setHoraComienzo(horaComienzo);
		horarioMiercoles.setHoraFin(horaFin);

		Horario horarioJueves = new Horario();
		horarioJueves.setDay(5);
		horarioJueves.setHoraComienzo(horaComienzo);
		horarioJueves.setHoraFin(horaFin);

		Horario horarioViernes = new Horario();
		horarioViernes.setDay(6);
		horarioViernes.setHoraComienzo(horaComienzo);
		horarioViernes.setHoraFin(horaFin);

		List<Horario> horarios = new ArrayList<>();
		horarios.add(horarioLunes);
		horarios.add(horarioMartes);
		horarios.add(horarioMiercoles);
		horarios.add(horarioJueves);
		horarios.add(horarioViernes);

		HORARIO_ATENCION = new HorarioAtencion(horarios);

	}

	/* --------------------------- */
}
