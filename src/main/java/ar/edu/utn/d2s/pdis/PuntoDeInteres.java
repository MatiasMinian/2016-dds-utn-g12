package ar.edu.utn.d2s.pdis;

import org.uqbar.geodds.Point;

public abstract class PuntoDeInteres {
	
	protected long Id;
	protected String nombre;
	protected String icono;
	protected Direccion direccion;
	protected double rangoDeCercania;
	protected String tipo;

	public PuntoDeInteres() {};

	public PuntoDeInteres(String nombre, String icono, Direccion direccion) {
		this.nombre = nombre;
		this.icono = icono;
		this.direccion = direccion;
		this.rangoDeCercania = 0.5;
	}
	
	/* ---- Getters & Setters ---- */
	
	public String getNombre() {
		return nombre;
	}
	
	public String getTipo(){
		return this.getClass().getSimpleName();
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public double getRangoDeCercania() {
		return rangoDeCercania;
	}

	public void setRangoDeCercania(double rangoDeCercania) {
		this.rangoDeCercania = rangoDeCercania;
	}
	
	public long getId() {
		return Id;
	}

	public void setId(long Id) {
		this.Id = Id;
	}

	/* --------------------------- */

	/* --------- Métodos --------- */
	
	public boolean estaCerca(Point punto){			
		return direccion.getPunto().distance(punto) < this.rangoDeCercania;
	}

	public abstract boolean verificaReferencia(String texto);
	
	/* --------------------------- */
}
