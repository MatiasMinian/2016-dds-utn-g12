package ar.edu.utn.d2s.pdis;

import org.uqbar.geodds.Point;

public class Parada {

	private Long id;
	private Direccion direccion;

	public Parada() {};

	public Parada(Direccion direccion) {
		this.direccion = direccion;
	}
	
	/* ---- Getters & Setters ---- */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Direccion getDireccion() {
		return direccion;
	}
	
	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}
	
	/* --------------------------- */
	
	/* --------- Métodos --------- */
	
	
	
	/* --------------------------- */
}
