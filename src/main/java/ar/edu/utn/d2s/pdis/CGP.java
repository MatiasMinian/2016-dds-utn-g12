package ar.edu.utn.d2s.pdis;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.uqbar.geodds.Point;

public class CGP extends PuntoDeInteres {
	
	private Comuna comuna;
	private List<Servicio> servicios = new ArrayList<>();
	public CGP() {};

	public CGP(String nombre, String icono, Direccion direccion, Comuna comuna, List<Servicio> servicios) {
		super(nombre, icono, direccion);
		this.comuna = comuna;
		this.servicios = servicios == null ? new ArrayList<Servicio>() : servicios;
	}

	/* ---- Getters & Setters ---- */

	public Comuna getComuna() {
		return comuna;
	}

	public void setComuna(Comuna comuna) {
		this.comuna = comuna;
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	/* --------------------------- */

	/* --------- Métodos --------- */

	public boolean estaCerca(Point punto) {
		return this.comuna.getArea().isInside(punto);
	}
	
	public boolean estaDisponible(Calendar momento, String servicio) {
		return this.servicios.stream().anyMatch(unServicio -> unServicio.getNombre().equals(servicio)
				&& unServicio.estaDisponible(momento));
	}
	
	public boolean estaDisponible(Calendar momento) {
		return this.servicios.stream().anyMatch(servicio -> servicio.estaDisponible(momento));
	}
	

	public boolean verificaReferencia(String texto) {

		return texto.equals(String.valueOf(this.getComuna().getNumero()))
				|| this.servicios.stream().anyMatch(servicio -> servicio.getNombre().startsWith(texto));
	}

	public void agregarServicio(Servicio unServicio) {
		this.servicios.add(unServicio);
	}	
	
	/* --------------------------- */
}