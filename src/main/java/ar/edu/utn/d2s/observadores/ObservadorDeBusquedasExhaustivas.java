package ar.edu.utn.d2s.observadores;

public interface ObservadorDeBusquedasExhaustivas {
	
	public void actualizar(String busqueda);

}
