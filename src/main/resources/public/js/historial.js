$(document).ready(function() {

	var queryString = location.search;

	if(queryString) {

		var rawParams = queryString.split('?')[1].split('&');
		var params = [];

		for(var i = 0; i < rawParams.length; ++i) {
			params[rawParams[i].split('=')[0]] = rawParams[i].split('=')[1] ? rawParams[i].split('=')[1] : "";
		}

		if(params.username)
			$("#usernameHistoryForm").val(params.username);
		if(params.dateFrom)
			$("#dateFromHistory").val(params.dateFrom);
		if(params.dateTo)
			$("#dateToHistory").val(params.dateTo);
	}

});