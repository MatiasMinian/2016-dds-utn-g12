

$(document).on('click', '.accionesChbox input', function(){
	var $this = $(this);

	var httpVerbs = {
		DELETE: 'DELETE',
		PUT: 'PUT'
	};

	var method = $this.prop('checked') ? httpVerbs.PUT : httpVerbs.DELETE;
	var accionId = $this.val();

	$this.prop('disabled', true);
	$this.parents('.checkbox').first().toggleClass("disabled");
	$this.parents('.checkbox').first().children(".miniLoader").show();

	$.ajax({
		url: '/accion/' + accionId + '/' ,
        method: method,
        dataType : "json",
        success: function(data) {
        	console.log(data);
        },
        error: function(error) {
        	console.log(error);
        },
        complete: function() {
        	$this.prop('disabled', false);
        	$this.parents('.checkbox').first().toggleClass("disabled");
        	$this.parents('.checkbox').first().children(".miniLoader").hide();
        }
	});

});