$(document).ready(function() {
	var movementStrength = 25;
	var height = movementStrength / $(window).height();
	var width = movementStrength / $(window).width();
	$("body").mousemove(function(e){
	          var pageX = e.pageX - ($(window).width() / 2);
	          var pageY = e.pageY - ($(window).height() / 2);
	          var newvalueX = width * pageX * -1 - 25;
	          var newvalueY = height * pageY * -1 - 50;
	          $('body').css("background-position", newvalueX+"px     "+newvalueY+"px");
	});
	
	var pdis = [];
	
	function buscar(){
		$this = $("#busq_button");
		
		var textoBusqueda = $('#busq_input').val();
		
        $('#result_container').html('<div class="loader"></div>');
		
        $.ajax({
            url: '/pdis?texto=' + textoBusqueda,
            type: 'GET',
            dataType : "json",
            success: function (data) {

                
                if(!data.length){
                	$('#result_container').html('<h3>No hay resultados con "' + textoBusqueda + '"</h3>');
                }else{
                	
                	pdis = data;

                    var contentHTML = '<table class="table"><thead><tr><th>Nombre</th><th>Dirección</th></tr></thead><tbody>';
                    
                    for(var i=0;i<data.length;i++){
                    	direccion = data[i]['direccion'] ? data[i]['direccion']['callePrincipal'] : '';
                    	
                    	contentHTML += '<tr data-index="'+i+'"><td>'+data[i]['nombre']+'</td><td>'+ direccion +'</td></tr>';
                    }
                    	
                    contentHTML += '</tbody></table>';
                    
                    $('#result_container').html(contentHTML);
                }

            },
            error: function (error) {
                console.log(error);
            },
            beforeSend: function () {
                $this.prop('disabled', true);
            },
            complete: function () {
                $this.prop('disabled', false);
            }
        });
		
	}
	
	$('#busq_input').keypress(function(e) {
	    if(e.which == 13) {
	    	buscar();
	    }
	});
	
	$('#busq_button').click(buscar);
	
	$('body').on('click', '#result_container table tr' ,function(){
		var index = $(this).data('index');
		
		pdi = pdis[index];
		
		htmlModal = obtenerHTMLdePdi(pdi);
		
		$('#pdi_id').html(pdi.id);
		$('#pdi_content').html(htmlModal);
		$('#pdi_modal').modal();
		
	});
	
	function obtenerHTMLdePdi(pdi){
		html = '';
		
		switch(pdi.tipo){
			case 'CGP':
				html = htmlCGP(pdi);
				break;
			case 'SucursalBanco':
				html = htmlSucursalBanco(pdi);
				break;
			case 'LocalComercial':
				html = htmlLocalComercial(pdi);
				break;
			case 'Colectivo':
				html = htmlColectivo(pdi);
				break;
		}
		
		
		return html;
	}
	
	function htmlCGP(pdi){
		return '<ul><li>Icono: ' + htmlIcono(pdi.icono) + '</li>' +
			   '<li>Dirección: ' + htmlDireccion(pdi.direccion) + '</li>' +
			   '<li>Zona que le corresponde: ' + htmlComuna(pdi.comuna) + '</li>' +
			   '<li>Lista de servicios que presta con su horario de atención: ' + htmlServiciosAt(pdi.servicios) + '</li></ul>';
	}
	
	function htmlSucursalBanco(pdi){
		return '<ul><li>Icono: ' + htmlIcono(pdi.icono) + '</li>' +
		   '<li>Dirección: ' + htmlDireccion(pdi.direccion) + '</li>' +
		   '<li>Lista de servicios que presta: ' + htmlServiciosAt(pdi.servicios) + '</li></ul>';
	}
	
	function htmlLocalComercial(pdi){
		return '<ul><li>Icono: ' + htmlIcono(pdi.icono) + '</li>' +
		   '<li>Dirección: ' + htmlDireccion(pdi.direccion) + '</li>' +
		   '<li>Nombre comercial o de fantasía: ' + pdi.nombre + '</li>' +
		   '<li>Rubro: ' + htmlRubro(pdi.rubro) + '</li></ul>';
	}

	function htmlColectivo(pdi){
		return '<ul><li>Icono: ' + htmlIcono(pdi.icono) + '</li>' +
		   '<li>Número de línea: ' + pdi.nroLinea + '</li></ul>';
	}
	
	function htmlIcono(icono){
		if(icono){
			return '<img src="/img' + icono + '" class="pdi_icon">';
		}else{
			return 'Sin icono';
		}
	}
	
	function htmlDireccion(direccion){
		htmlArray = [];
		
		if(direccion.callePrincipal)
			htmlArray.push(direccion.callePrincipal);
		
		if(direccion.entreCalle1)
			htmlArray.push(direccion.entreCalle1);
		
		if(direccion.entreCalle2)
			htmlArray.push(direccion.entreCalle2);
		
		if(direccion.numero)
			htmlArray.push(direccion.numero);
		
		if(direccion.piso)
			htmlArray.push(direccion.piso);
		
		if(direccion.departamento)
			htmlArray.push(direccion.departamento);
		
		if(direccion.codigoPostal)
			htmlArray.push(direccion.codigoPostal);
		
		if(direccion.localidad)
			htmlArray.push(direccion.localidad);
		
		if(direccion.barrio)
			htmlArray.push(direccion.barrio);
		
		if(direccion.provincia)
			htmlArray.push(direccion.provincia);
		
		if(direccion.pais)
			htmlArray.push(direccion.pais);
		
		return htmlArray.join(', ');
	}
	
	function htmlRubro(rubro){
		htmlArray = [];
		
		if(rubro.nombre)
			htmlArray.push(rubro.nombre);
		
		return htmlArray.join(', ');
	}
	
	function htmlServicios(servicios){
		return servicios ? servicios.join(', ') : '';
	}
	
	function htmlComuna(comuna){
		return comuna ? comuna.numero : '';
	}
	
	function htmlServiciosAt(servicios){
		if(servicios){
			html = '<ul>';
			
			for(i=0;i<servicios.length;i++){
				if(servicios[i].horarioAtencion){
					html += '<li>' + servicios[i].nombre + ' - ' + htmlHorariosAtencion(servicios[i].horarioAtencion.horarios)  + '</li>';
				}else{
					html += '<li>' + servicios[i].nombre + '</li>';
				}
			}
			
			return html + '</ul>';
		}else{
			return '';
		}
	}
	
	var dias = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábados', 'Domingos'];
	
	function htmlHorariosAtencion(horariosAtencion){
		horarios = [];
		
		for(i=0;i<horariosAtencion.length;i++){
			var fechaComienzo = new Date(horariosAtencion[i].horaComienzo);
			var fechaFin = new Date(horariosAtencion[i].horaFin);
			
			horarios.push('Días ' + dias[horariosAtencion[i].day] + ' ' + 
					twoDigits(fechaComienzo.getHours()) + ':' + twoDigits(fechaComienzo.getMinutes()) + ' a ' + 
					twoDigits(fechaFin.getHours()) + ':' + twoDigits(fechaFin.getMinutes()));
		}
		
		return horarios.join(', ');
	}
	
	function twoDigits(number){
		return ("0" + number).slice(-2);
	}
	
});